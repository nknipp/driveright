module.exports = {
  'env': {
    'browser': false,
    'commonjs': true,
    'es6': true,
    'node': true,
    'jest': true,
    'jquery': true,
    'mongo': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended'
  ],
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaFeatures': {
      'experimentalObjectRestSpread': true,
      'jsx': true
    }
  },
  'plugins': [
    'async-await',
    'flowtype',
    'graphql',
    'react'
  ],
  'rules': {
    'graphql/template-strings': ['error', {
      'env': 'relay',
      'schemaJson': require('./cache/main.json')
    }],
    // "indent": [
    //   "error",
    //   2,
    //   {"SwitchCase": 1}
    // ],
    // 'linebreak-style': [
    //   'error',
    //   'windows'
    // ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'no-console': 0,
    'no-useless-escape': 0,
    'async-await/space-after-async': 2,
    'async-await/space-after-await': 2
  },
  'settings': {
    'flowtype': {
      'onlyFilesWithFlowAnnotation': true
    }
  }
};