'use strict';

const nodemailer = jest.genMockFromModule('nodemailer');

const __sendMail = jest.fn().mockImplementation((message, cb) => cb(null, 'message'));
const transport = {
  sendMail: __sendMail
};

nodemailer.__sendMail = __sendMail;
nodemailer.createTransport = function() {
  return transport;
};

module.exports = nodemailer;