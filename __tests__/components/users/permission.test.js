'use strict';

const _ = require('underscore');
const connection = require('../../../dist/components/databases/databaseConnection');
const permissions = require('../../../dist/components/users/permission');

process.env.MONGODB_ADDON_URI = 'mongodb://localhost/testPermissions';

let db = undefined;

async function initializeMongoDB() {
  db = await connection.connectDB();
  return db.collection('roles').insertMany([{
    'role' : 'admin',
    'permissions' : [
      'system:*',
      'permissions:*',
      'roles:*',
      'drivingSchools:*',
      'drivingInstructors:*',
      'learners:*'
    ]
  },{
    'role': 'learner',
    'permissions': [
      'drivingSchools:view:$DrivingSchool',
      'drivingInstructors:view:$DrivingSchool'
    ]
  },{
    'role' : 'drivingSchoolAdmin',
    'permissions' : [
      'permissions:*:$DrivingSchool',
      'roles:*:$DrivingSchool',
      'drivingSchools:view,edit:$DrivingSchool',
      'drivingInstructors:add,view,edit,delete:$DrivingSchool',
      'learners:add,view,edit,delete:$DrivingSchool'
    ]
  }]);
}

beforeAll(() => {
  return initializeMongoDB();
});

afterAll(() => {
  return db.dropDatabase()
    .then(() => {
      db.close();
    });
});

test('read role and prepare permissions for drivingSchoolAdmin', async () => {
  const user = {
    role: 'drivingSchoolAdmin',
    drivingSchoolId: '123456789'
  };

  const receivedPermissions = await permissions.getPermissionsByUser(user);
  const userPermissions = [
    'permissions:*:123456789',
    'roles:*:123456789',
    'drivingSchools:view,edit:123456789',
    'drivingInstructors:add,view,edit,delete:123456789',
    'learners:add,view,edit,delete:123456789'
  ];
  expect(_.isEqual(receivedPermissions, userPermissions)).toBeTruthy();
});

test('read role and prepare permissions for learner', async () => {
  const user = {
    role: 'learner',
    drivingSchoolId: '987654321'
  };

  const receivedPermissions = await permissions.getPermissionsByUser(user);
  const userPermissions = [
    'drivingSchools:view:987654321',
    'drivingInstructors:view:987654321'
  ];
  expect(_.isEqual(receivedPermissions, userPermissions)).toBeTruthy();
});

test('read role and prepare permissions for admin', async () => {
  const user = {
    role: 'admin',
    drivingSchoolId: '123456789'
  };

  const receivedPermissions = await permissions.getPermissionsByUser(user);
  const userPermissions = [
    'system:*',
    'permissions:*',
    'roles:*',
    'drivingSchools:*',
    'drivingInstructors:*',
    'learners:*'
  ];
  expect(_.isEqual(receivedPermissions, userPermissions)).toBeTruthy();
});
