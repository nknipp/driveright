'use strict';

jest.mock('nodemailer');

const connection = require('../../../dist/components/databases/databaseConnection');
const cryptoUtils = require('../../../dist/utils/crypto');
const resetPassword = require('../../../dist/components/users/resetPassword');

process.env.MONGODB_ADDON_URI = 'mongodb://localhost/testResetPassword';
process.env.ENCRYPTION_PASS = 'abcdefghijklmnopqrstuvwxyz';

let db = undefined;

async function initializeMongoDB() {
  db = await connection.connectDB();
  return db.collection('users').insertMany([{
    'credentials' : {
      'emailAddress': 'test@test.local',
      'hash': ''
    }
  }]);
}

beforeAll(() => {
  return initializeMongoDB();
});

afterAll(() => {
  return db.dropDatabase()
    .then(() => {
      db.close();
    });
});

describe('resetPasswordStep1', () => {
  afterEach(() => {
    require('nodemailer').__sendMail.mockClear();
  });

  test('resetPasswordStep1 with invalid emailAddress', async () => {
    const msg = await resetPassword.resetPasswordStep1('invalid@test.local');
    expect(msg).toBe('We send an email to invalid@test.local');
    expect(require('nodemailer').__sendMail.mock.calls.length).toBe(0);
  });

  test('resetPasswordStep1 with valid emailAddress', async () => {
    const msg = await resetPassword.resetPasswordStep1('test@test.local');
    expect(msg).toBe('We send an email to test@test.local');

    const sendMailFn = require('nodemailer').__sendMail;
    expect(sendMailFn.mock.calls.length).toBe(1);

    const message = sendMailFn.mock.calls[0][0];
    expect(message.to).toBe('test@test.local');

    const resetTokenMatch = /<a href=".+\/login\?resettoken=(.+)">/.exec(message.html);
    expect(resetTokenMatch).not.toBeNull();
    expect(resetTokenMatch[1]).not.toBeNull();

    let resetData = cryptoUtils.decryptData(decodeURIComponent(resetTokenMatch[1]));
    expect(resetData).not.toBeNull();

    resetData = JSON.parse(resetData);
    expect(resetData.emailAddress).not.toBeNull();
    expect(resetData.emailAddress).toBe('test@test.local');
  });
});

describe('resetPasswordStep2', () => {
  test('resetPasswordStep2 with unequal passwords', async () => {
    expect.assertions(1);
    await expect(resetPassword.resetPasswordStep2('token', 'password1', 'password2')).rejects.toBeDefined();
  });

  test('resetPasswordStep2 with invalid emailAddress', async () => {
    const token = cryptoUtils.encryptData(JSON.stringify({ emailAddress: 'invalid@test.local', date: new Date() }));
    await resetPassword.resetPasswordStep2(token, 'password', 'password');

    //TODO what to expect???
  });

  test('resetPasswordStep2 with valid emailAddress', async () => {
    const token = cryptoUtils.encryptData(JSON.stringify({ emailAddress: 'test@test.local', date: new Date() }));
    const msg = await resetPassword.resetPasswordStep2(token, 'password', 'password');
    expect(msg).toBe('Reset password');

    const user = await db.collection('users').findOne({ 'credentials.emailAddress': 'test@test.local' });
    const pw = require('credential')();
    expect(pw.verify(user.credentials.hash, 'password')).toBeTruthy();
  });
});