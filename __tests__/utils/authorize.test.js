'use strict';

const authorizer = require('../../dist/utils/authorize');

test('undefined subjectPermission', () => {
  expect(authorizer.isAllowed('some:view:12345', undefined)).toBeFalsy();
});

test('equal permissions', () => {
  expect(authorizer.isAllowed('some:view:12345', 'some:view:12345')).toBeTruthy();
  expect(authorizer.isAllowed('some:view:12345', 'some:view,edit:12345')).toBeTruthy();
  expect(authorizer.isAllowed('some:view:12345', 'some:view,edit,delete:12345')).toBeTruthy();
  expect(authorizer.isAllowed('some:view:12345', 'some:*:12345')).toBeTruthy();
  expect(authorizer.isAllowed('some:view:12345', 'some:view:54321,12345')).toBeTruthy();

  expect(authorizer.isAllowed('some:view:12345', ['some:edit:12345', 'some:view:12345'])).toBeTruthy();
  expect(authorizer.isAllowed('some:view:12345', ['some:edit:54321', 'some:view:*'])).toBeTruthy();
  expect(authorizer.isAllowed('some:view:12345', ['some:edit:54321', 'some:*:*'])).toBeTruthy();
});

test('unequal permissions', () => {
  expect(authorizer.isAllowed('some:view:12345', 'some:view:54321')).toBeFalsy();
  expect(authorizer.isAllowed('some:edit:12345', 'some:view:12345')).toBeFalsy();
  expect(authorizer.isAllowed('some:view:12345', 'other:view:12345')).toBeFalsy();
  expect(authorizer.isAllowed('some:view:12345', 'other:view:*')).toBeFalsy();
  expect(authorizer.isAllowed('some:view:12345', 'other:*:*')).toBeFalsy();
  expect(authorizer.isAllowed('some:view:12345', 'some1:view:12345')).toBeFalsy();
  expect(authorizer.isAllowed('some1:view:12345', 'some:view:12345')).toBeFalsy();
  expect(authorizer.isAllowed('some:view:12345', 'some:view:54321,15243')).toBeFalsy();

  expect(authorizer.isAllowed('some:view:12345', ['some:edit:12345', 'some:view:54321'])).toBeFalsy();
});