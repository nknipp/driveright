'use strict';

const getBabelRelayPlugin = require('babel-relay-plugin');
const schema = require('./cache/main.json');

module.exports = getBabelRelayPlugin(schema.data, {
  debug: true,
  suppressWarnings: false,
  enforceSchema: true
});