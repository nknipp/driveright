const exec = require('child_process').exec;
const gulp = require('gulp');
const babel = require('gulp-babel');
const del = require('del');
const schema = require('gulp-graphql');
const prune = require('gulp-prune');
const sourcemaps = require('gulp-sourcemaps');

const argv = require('yargs').argv;
let env = argv.env ? argv.env : 'dev';

gulp.task('babel', () => {
  let stream = gulp.src(['src/**/*.js', '!src/public/**/*']);
  if (env === 'dev') {
    stream = stream.pipe(sourcemaps.init());
  }
  stream = stream.pipe(babel({
    presets: ['flow']
  }));
  if (env === 'dev') {
    stream = stream.pipe(sourcemaps.write());
  }
  return stream.pipe(gulp.dest('dist'));
});

gulp.task('flow', () => {
  exec('flow', (err, stdout) => {
    console.log(stdout);
  });
});

gulp.task('schema', ['babel'], () => {
  gulp.src('dist/schema/main.js')
    .pipe(schema({
      fileName: 'main',
      json: true,
      graphql: true
    }))
    .on('error', console.log)
    .pipe(gulp.dest('cache'));
});

gulp.task('static', (cb) => {
  gulp.src('package*.json')
    .pipe(gulp.dest('dist'));
  gulp.src('src/bin/*')
    .pipe(prune('dist/bin'))
    .pipe(gulp.dest('dist/bin'));
  gulp.src('src/clevercloud/*')
    .pipe(prune('dist/clevercloud'))
    .pipe(gulp.dest('dist/clevercloud'));
  gulp.src('src/mailTemplates/*')
    .pipe(prune('dist/mailTemplates'))
    .pipe(gulp.dest('dist/mailTemplates'));
  gulp.src('src/views/*')
    .pipe(prune('dist/views'))
    .pipe(gulp.dest('dist/views'));
  gulp.src('src/public/favicon.ico')
    .pipe(gulp.dest('dist/public'));
  gulp.src('src/public/robots.txt')
    .pipe(gulp.dest('dist/public'));
  gulp.src(['src/public/images/**/*'])
    .pipe(prune('dist/public/images'))
    .pipe(gulp.dest('dist/public/images'));
  gulp.src(['src/public/theme/**/*', '!src/public/theme/dashboard/sass/**/*'])
    .pipe(prune('dist/public/theme'))
    .pipe(gulp.dest('dist/public/theme'));
  cb();
});

gulp.task('webpack', () => {
  const configFile = env === 'dev' ? './webpack.config.dev.js' : './webpack.config.prd.js';
  exec(`webpack ${ env === 'prd' ? '-p' : ''} --config ${configFile}`, (err, stdout) => {
    console.log(err);
    console.log(stdout);
  });
});

gulp.task('clean', ['static'], (cb) => {
  if (env !== 'dev') {
    del(['dist/**/*.js.map']);
  }
  del('dist/public/theme/dashboard/sass');
  cb();
});

gulp.task('build', ['flow', 'schema', 'webpack', 'static', 'clean']);