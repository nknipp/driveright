Berechtigungen

drivingschool:\<component>:\<right>:\<drivingschoolid>:\<userid> \
component: [master|store|passedgallery|course] \
right: [add|delete|edit|view]

user:\<right>:\<role>:\<drivingschoolid>