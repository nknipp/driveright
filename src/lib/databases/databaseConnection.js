// @ flow
'use strict';

const MongoClient = require('mongodb').MongoClient;

let db;
exports.connectDB = function () {
  return new Promise((resolve, reject) => {
    if (db) return resolve(db);

    // Use connect method to connect to the Server
    MongoClient.connect(process.env.MONGODB_ADDON_URI, {
      authMechanism: 'SCRAM-SHA-1'
    }, (err, _client) => {
      if (err) return reject(err);
      db = _client.db();
      resolve(db);
    });
  });
};
