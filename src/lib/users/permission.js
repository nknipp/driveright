// @flow
'use strict';

const roles = require('./role');

function getPermissionsByUser(user: Object): Promise<Array<string>> {
  if (user.permissions !== undefined) return user.permissions;
  return roles.getRoleByName(user.role)
    .then((role: Object) => {
      const permissions = new Array(role.permissions.length);
      for (let i = 0; i <  role.permissions.length; i++) {
        permissions[i] = role.permissions[i];
        permissions[i] = permissions[i].replace('$DrivingSchoolId', user.drivingSchoolId);
        permissions[i] = permissions[i].replace('$UserId', user._id);
      }
      return permissions;
    });
}

module.exports = {
  getPermissionsByUser
};
