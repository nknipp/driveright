// @flow
'use strict';

const credential = require('credential');
const cryptoUtils = require('../utils/crypto');
const mailer = require('../utils/mailer');
const user = require('./user');

async function resetPasswordStep1(username: string): Promise<string> {
  const userObj: Object = await user.getUserByUsername(username);
  if (userObj) {
    const token: string = cryptoUtils.encryptData(JSON.stringify({ username, date: new Date() }));
    await mailer.send(username, 'resetPasswordStep1', { data: { user: userObj, token }});
  }
  return `Wir haben eine E-Mail an ${username} gesendet, in der Sie weitere Informationen erhalten.`;
}

async function resetPasswordStep2(token: string, password1: string, password2: string): Promise<Object> {
  if (password1 !== password2) return { success: false, msg: 'Beide Passwörter müssen gleich sein.' };
  if (password1 === '') return { success: false, msg: 'Das Passwort darf nicht leer sein.' };

  //TODO Passwort-Policy gültig

  const encryptedToken: Object = JSON.parse(cryptoUtils.decryptData(token));
  //TODO Datum/Uhrzeit ist nicht älter als 24 Stunden
  //if (encryptedToken.date < new Date())
  let userObj: Object = await user.getUserByUsername(encryptedToken.username);
  if (!userObj && encryptedToken.id && encryptedToken.role) {
    //TODO User für Fahrschule anlegen
  }
  if (userObj) {
    const pw = credential();
    userObj.credentials.hash = await pw.hash(password1);
    await user.saveUserById(userObj);
  }
  return { success: true, msg: 'Passwort wurde erfolgreich zurückgesetzt' };
}

module.exports = {
  resetPasswordStep1,
  resetPasswordStep2
};