// @flow
'use strict';

const connection = require('../databases/databaseConnection');

async function getRoleByName(role: string): Promise<Object> {
  const db = await connection.connectDB();
  return await db.collection('roles').findOne({ role: role });
}

module.exports = {
  getRoleByName
};