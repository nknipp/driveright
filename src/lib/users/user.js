// @flow
'use strict';

const connection = require('../databases/databaseConnection');

async function getUserByUsername(username: string): Promise<Object> {
  const db = await connection.connectDB();
  return db.collection('users').findOne({ 'credentials.username': username });
}

async function saveUserById(user: Object) {
  const db = await connection.connectDB();
  return db.collection('users').updateOne({ _id: user._id }, {
    $set: {
      credentials: user.credentials,
      drivingSchoolId: user.drivingSchoolId,
      firstname: user.firstname,
      lastname: user.lastname
    }
  });
}

module.exports = {
  getUserByUsername,
  saveUserById
};