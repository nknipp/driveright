// @flow
'use strict';

export function swapElement(array: Array<any>, from: number, to: number) {
  if (from < 0 || from >= array.length || to < 0 || to >= array.length) return array;

  const arrayClone: Array<any> = array.slice();
  const value = arrayClone[from];
  arrayClone.splice(from, 1);
  arrayClone.splice(to, 0, value);

  return arrayClone;
}