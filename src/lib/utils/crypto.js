// @flow
'use strict';

const crypto = require('crypto');

let encryptionPass: string = '';
if (process.env.ENCRYPTION_PASS) {
  encryptionPass = process.env.ENCRYPTION_PASS;
}

function encryptData(data: string) {
  const cipher = crypto.createCipher('aes-256-ctr', encryptionPass);
  let crypted = cipher.update(data, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

function decryptData(data: string) {
  const decipher = crypto.createDecipher('aes-256-ctr', encryptionPass);
  let decrypted = decipher.update(data, 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
}

module.exports = {
  encryptData,
  decryptData
};