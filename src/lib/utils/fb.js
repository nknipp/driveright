// @ flow
'use strict';

const fs = require('fs');
const path = require('path');
const graph = require('fbgraph');
const request = require('request-promise-native');

const connection = require('../databases/databaseConnection');

const clientId = process.env.FBCLIENTID;
const clientSecret = process.env.FBCLIENTSECRET;

const fbAccessUrl = `https://graph.facebook.com/oauth/access_token?client_id=${clientId}&client_secret=${clientSecret}&grant_type=client_credentials`;

graph.setVersion('2.10');

async function getRatingStatistics() {
  try {
    const body = await request(fbAccessUrl);

    const access_token = JSON.parse(body)['access_token'];
    graph.setAccessToken(access_token);

    const db = await connection.connectDB();
    const drivingschools = await db.collection('drivingschools').find({ facebookId: { $exists: true, $ne: '' }});
    drivingschools.forEach(drivingschool => {
      graph.get(`/${drivingschool.facebookId}?fields=overall_star_rating,rating_count`, function(err, res) {
        if (err) {
          console.log(err);
          return;
        }

        const srcSitePath = path.join(process.env.SITESBASE, drivingschool.website, 'static/rating-statistics.json');
        fs.writeFile(srcSitePath, JSON.stringify(res), (err) => {
          if (err) {
            console.log(err);
            return;
          }
          const destSitePath = path.join(process.env.SITESBASE, drivingschool.website, 'public/rating-statistics.json');
          fs.copyFile(srcSitePath, destSitePath, (err) => {
            if (err) console.log(err);
          });
        });
        console.log(res);
      });
    });
  } catch (err) {
    console.log(err);
  }
}

module.exports = {
  getRatingStatistics
};
