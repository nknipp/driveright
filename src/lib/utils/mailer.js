// @flow
'use strict';

const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');
const ejs = require('ejs');

const mailTemplatesDir = path.join(__dirname, '../../mailTemplates');

function loadTemplateByName(templateName: string): Promise<string> {
  const templateNameWithExtension: string = templateName.endsWith('.ejs') ? templateName : templateName + '.ejs';
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(mailTemplatesDir, templateNameWithExtension), 'utf8', (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
}

function getRenderedTemplateData(template: string, data: Object): Object {
  const renderedTemplate = ejs.render(template, data);
  const mailSubject: string = /<mailsubject>([\s\S]+)<\/mailsubject>/.exec(renderedTemplate)[1];
  const mailBody: string = /<mailbody>([\s\S]+)<\/mailbody>/.exec(renderedTemplate)[1];
  const mailPlain: string = /<mailplain>([\s\S]+)<\/mailplain>/.exec(renderedTemplate)[1];
  return {mailSubject, mailBody, mailPlain};
}

function getTransport() {
  let smtpHost = '{}';
  if (process.env.SMTP_HOST) smtpHost = process.env.SMTP_HOST;
  return nodemailer.createTransport(JSON.parse(smtpHost));
}

function sendMail(transport, message) {
  return new Promise((resolve, reject) => {
    transport.sendMail(message, (err, info) => {
      if (err) return reject(err);
      resolve(info);
    });
  });
}

async function send(recipient: string, templateName: string, mailData: Object, attachments: Array<Buffer> = [], replyTo: string | void = undefined) {
  try {
    const template = await loadTemplateByName(templateName);
    const {mailSubject, mailBody, mailPlain} = getRenderedTemplateData(template, mailData);

    const transport = getTransport();
    const message = {
      from: process.env.SMTP_SENDER,
      to: recipient,
      replyTo: replyTo,
      subject: mailSubject,
      text: mailPlain,
      html: mailBody,
      attachments: attachments
    };
    return sendMail(transport, message);
  } catch(e) {
    console.error(e);
  }
}

module.exports = {
  send
};