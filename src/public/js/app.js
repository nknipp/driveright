// @flow
'use strict';

const React = require('react');
const withRouter = require('react-router/lib/withRouter');

const App = (props: Object) => {
  return (
    <div>{ React.cloneElement(props.children) }</div>
  );
};

const AppIndex = function() {
  return <div>Hier ist die Anwendung.</div>;
};

module.exports = {
  App: withRouter(App),
  AppIndex
};