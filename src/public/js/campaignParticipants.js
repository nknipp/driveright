// @ flow
'use strict';

const React = require('react');
const Relay = require('react-relay');

const { confirm, Spinner } = require('./feedback');

class CampaignParticipants extends React.Component {
  constructor(props) {
    super(props);

    this.handleConfirmDelete = this.handleConfirmDelete.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleConfirmDelete(item) {
    this.deleteCampaignParticipant = item.item.node;
    confirm('Löschen bestätigen', 'Wollen Sie diesen Teilnehmer wirklich löschen?', this.handleDelete);
  }

  handleDelete() {
    Spinner.start();
    Relay.Store.commitUpdate(
      new DeleteCampaignParticipantMutation({
        campaignParticipantId: this.deleteCampaignParticipant.id,
        drivingSchoolId: this.props.id,
        campaignParticipants: null
      }), {
        onSuccess: () => {
          Spinner.success('Teilnehmer wurde gelöscht');
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Teilnehmer konnte nicht gelöscht werden');
        }
      }
    );
  }

  renderCampaignParticipant(item) {
    const node = item.node;
    return (
      <div className="row" key={node.id}>
        <div className="col-sm-1">{node.solution}</div>
        <div className="col-sm-10">
          {node.name}, {node.street}, {node.postcode} {node.city}, {node.phone}, {node.emailAddress}
        </div>
        <div className="col-sm-1">
          <button type="button" className="btn btn-primary pull-right" title="Teilnehmer löschen" onClick={(e) => this.handleConfirmDelete({item}, e)}>
            <i className="ti-trash"></i>
          </button>
        </div>
      </div>
    );
  }

  render() {
    const campaignParticipantEdges = this.props.campaignParticipants.campaignParticipants.edges;

    return (
      <div className="container-fluid">
        <div className="card">
          { campaignParticipantEdges.map(campaignParticipant => this.renderCampaignParticipant(campaignParticipant)) }
        </div>
      </div>
    );
  }
}

class DeleteCampaignParticipantMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { deleteCampaignParticipant }`;
  }

  getVariables() {
    return {
      campaignParticipantId: this.props.campaignParticipantId,
      drivingSchoolId: this.props.drivingSchoolId
    };
  }

  static get fragments() {
    return {
      campaignParticipants: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on DeleteCampaignParticipantMutationPayload {
        drivingschool
        deletedCampaignParticipantId
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'campaignParticipants',
      deletedIDFieldName: 'deletedCampaignParticipantId'
    }];
  }
}

const CampaignParticipantsPage = Relay.createContainer(
  CampaignParticipants,
  {
    fragments: {
      campaignParticipants: () => Relay.QL`
        fragment on DrivingSchool {
          id
          campaignParticipants(first: 1000) {
            edges {
              cursor
              node {
                id
                solution,
                name,
                street,
                postcode,
                city,
                phone,
                emailAddress
              }
            }
          }
          ${DeleteCampaignParticipantMutation.getFragment('campaignParticipants')}
        }`
    }
  }
);

const CampaignParticipantsQuery = {
  campaignParticipants: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  CampaignParticipants: CampaignParticipantsPage,
  CampaignParticipantsQuery
};