// @flow
'use strict';

import React from 'react';
import Relay from 'react-relay';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import Cropper from './components/cropper';
import { SortableList, SortableListItem } from './components/sortableList';

import { confirm, error, Spinner } from './feedback';

const wrapperStyle = {
  border: '1px solid #CCC5B9',
  backgroundColor: '#fffcf5',
  borderRadius: '4px',
  padding: '7px 18px',
  height: '220px'
};
const editorStyle = {
  backgroundColor: '#fff',
  height: '160px'
};

type Car = {
  filename: string,
  image: string,
  description: string
};

type Props = {
  drivingSchool: {
    id: string,
    website: string,
    cars: Array<Car>
  }
};

type State = {
  mode: 'add' | 'edit' | 'view',
  image: string,
  editorState: EditorState
};

// const constraints = {
//   image: {
//     presence: {
//       allowEmpty: false,
//       message: 'Bitte laden Sie ein Bild hoch.'
//     }
//   }
// };

class CarComponent extends React.Component<Props, State> {

  state = {
    mode: 'view',
    image: '',
    editorState: EditorState.createEmpty()
  };
  currentCar: number = 0;

  static getEditorStateFromHtml(content) {
    const contentBlock = htmlToDraft(content);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      return EditorState.createWithContent(contentState);
    }
    return EditorState.createEmpty();
  }

  static getHtmlFromEditorState(editorState: EditorState) {
    return draftToHtml(convertToRaw(editorState.getCurrentContent()));
  }

  handleChangeEditorState = (editorState) => {
    this.setState({ editorState });
  };

  handleAdd = () => {
    this.setState({
      mode: 'add',
      image: '',
      editorState: EditorState.createEmpty()
    });
  };

  handleEdit = (idx: number) => {
    this.currentCar = idx;
    const car = this.props.drivingSchool.cars[idx];
    this.setState({
      mode: 'edit',
      image: '',
      editorState: CarComponent.getEditorStateFromHtml(car.description)
    });
  };

  handleCancel = () => {
    this.currentCar = 0;
    this.setState({
      mode: 'view',
      image: ''
    });
  };

  handleImageData = (image: string) => {
    this.setState({ image });
  };

  handleConfirmDelete = (idx: number) => {
    this.currentCar = idx;
    confirm('Löschen bestätigen', 'Wollen Sie das Fahrzeug wirklich löschen?', this.handleDelete);
  };

  handleMove = (dndResult) => {
    Spinner.start();
    Relay.Store.commitUpdate(
      new SortCarMutation({
        drivingSchoolId: this.props.drivingSchool.id,
        from: dndResult.from,
        to: dndResult.to,
        drivingSchool: null
      }), {
        onSuccess: () => {
          Spinner.success('Sortierung wurde geändert');
          this.setState({ mode: 'view' });
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Sortierung konnte nicht geändert werden');
        }
      }
    );
  };

  handleDelete = () => {
    const car = this.props.drivingSchool.cars[this.currentCar];
    // $FlowIgnore
    delete car.__dataID__;

    Spinner.start();
    Relay.Store.commitUpdate(
      new DeleteCarMutation({
        drivingSchoolId: this.props.drivingSchool.id,
        index: this.currentCar,
        drivingSchool: null
      }), {
        onSuccess: () => {
          Spinner.success('Fahrzeug wurde gelöscht');
          this.setState({ mode: 'view' });
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Fahrzeug konnte nicht gelöscht werden');
        }
      }
    );
  };

  handleSave = () => {
    Spinner.start();
    if (this.state.mode === 'edit') {
      Relay.Store.commitUpdate(
        new UpdateCarMutation({
          drivingSchoolId: this.props.drivingSchool.id,
          index: this.currentCar,
          image: this.state.image,
          description: CarComponent.getHtmlFromEditorState(this.state.editorState),
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Fahrzeug wurde geändert');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Fahrzeug konnte nicht geändert werden');
          }
        }
      );
    } else if (this.state.mode === 'add') {
      Relay.Store.commitUpdate(
        new AddCarMutation({
          drivingSchoolId: this.props.drivingSchool.id,
          image: this.state.image,
          description: CarComponent.getHtmlFromEditorState(this.state.editorState),
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Fahrzeug wurde eingefügt');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Fahrzeug konnte nicht eingefügt werden');
          }
        }
      );
    }
  };

  renderEditBar() {
    if (this.state.mode === 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Neues Fahrzeug anlegen" onClick={this.handleAdd}><i className="ti-plus"></i></button>
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    }
  }

  renderAddOrEdit() {
    return (
      <div className="card">
        <form encType="multipart/form-data">
          <div className="form-group">
            <label>Beschreibung</label>
            <Editor editorState={this.state.editorState}
                    onEditorStateChange={this.handleChangeEditorState}
                    toolbar={{
                      options: ['inline', 'fontSize', 'textAlign', 'list'],
                      list: {
                        options: ['unordered', 'ordered'],
                        unordered: { className: undefined },
                        ordered: { className: undefined }
                      }
                    }}
                    wrapperStyle={wrapperStyle}
                    editorStyle={editorStyle}
            />
          </div>
          <div className="form-group">
            <label>Bild</label>
            <Cropper
              image={ this.handleImageData }
              style={{ height: '525px', width: '525px' }}
              aspectRatio={1 / 1}
            />
          </div>
        </form>
      </div>
    );
  }

  renderView() {
    const cars = this.props.drivingSchool.cars;
    return (
      <div className="card">
        <SortableList>
          {
            cars.map((car, idx) =>
            <SortableListItem key={idx} idx={idx} onDrop={this.handleMove}>
              <div className="row">
                <div style={{ float: 'left' }}>
                  <i className="ti-arrows-vertical"></i>
                </div>
                <div>
                  <div className="col-sm-1">
                    {
                      car.filename ? <img src={`${this.props.drivingSchool.website}/static/images/cars/${car.filename}`} className="img-responsive"/> : ''
                    }
                  </div>
                  <div className="col-sm-8">{car.description}</div>
                  <div className="col-sm-2">
                    <button type="button" className="btn btn-primary pull-right" title="Fahrzeug löschen" onClick={() => this.handleConfirmDelete(idx)}>
                      <i className="ti-trash"></i>
                    </button>
                    <button type="button" className="btn btn-primary pull-right" style={{ marginRight: '10px' }} title="Fahrzeug bearbeiten" onClick={() => this.handleEdit(idx)}>
                      <i className="ti-pencil"></i>
                    </button>
                  </div>
                </div>
              </div>
            </SortableListItem>
          )}
        </SortableList>
      </div>
    );
  }

  render() {
    let content;
    switch (this.state.mode) {
      case 'add':
      case 'edit':
        content = this.renderAddOrEdit();
        break;

      case 'view':
      default:
        content = this.renderView();
    }

    return (
      <div>
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class AddCarMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { addCar }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      image: this.props.image,
      description: this.props.description
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          website
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddCarMutationPayload {
        drivingschool {
          cars
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class UpdateCarMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updateCar }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      index: this.props.index,
      image: this.props.image,
      description: this.props.description
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateCarMutationPayload {
        drivingschool {
          id
          website
          cars
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class SortCarMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { sortCar }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      from: this.props.from,
      to: this.props.to
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on SortCarMutationPayload {
        drivingschool {
          id
          website
          cars
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class DeleteCarMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { deleteCar }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      index: this.props.index
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
          fragment on DrivingSchool {
              id
              website
          }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on DeleteCarMutationPayload {
        drivingschool {
          id
          cars
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

const CarPage = Relay.createContainer(
  CarComponent,
  {
    fragments: {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          website
          cars {
            filename
            description
          }
          ${AddCarMutation.getFragment('drivingSchool')}
          ${UpdateCarMutation.getFragment('drivingSchool')}
          ${SortCarMutation.getFragment('drivingSchool')}
          ${DeleteCarMutation.getFragment('drivingSchool')}
        }`
    }
  }
);

const CarQuery = {
  drivingSchool: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  Car: CarPage,
  CarQuery
};