// @ flow
'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import Router from 'react-router/lib/Router';

const createHashHistory = require('history/lib/createHashHistory');
const applyRouterMiddleware = require('react-router/lib/applyRouterMiddleware');
const useRouterHistory = require('react-router/lib/useRouterHistory');
const useRelay = require('react-router-relay');

const routes = require('./routes');

const graphQLUrl = '/q';

Relay.injectNetworkLayer(
  new Relay.DefaultNetworkLayer(graphQLUrl, {
    credentials: 'include'
  })
);

const history = useRouterHistory(createHashHistory)({ queryKey: false });

ReactDOM.render(
  <Router
    history={history}
    routes={routes}
    render={applyRouterMiddleware(useRelay.default)}
    environment={Relay.Store}
  />,
  document.getElementById('content')
);