'use strict';

const React = require('react');
const Cropper = require('react-cropper').default;

require('cropperjs/dist/cropper.css');

class CropperComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      src: null,
      cropResult: null,
    };

    this.cropImage = this.cropImage.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  cropImage() {
    if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return;
    }
    this.props.image(this.cropper.getCroppedCanvas().toDataURL());
  }

  onChange(e) {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({ src: reader.result });
    };
    reader.readAsDataURL(files[0]);
  }

  render() {
    return (
      <div>
        <input type="file" onChange={this.onChange} />
        <Cropper
          ref={ cropper => { this.cropper = cropper; }}
          src={ this.state.src }
          style={ this.props.style }
          aspectRatio={ this.props.aspectRatio }
          guides={ false }
          cropend={ this.cropImage }/>
      </div>
    );
  }

}

module.exports = CropperComponent;