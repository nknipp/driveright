import React from 'react';
import { DragDropContext, DragSource, DropTarget } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend'  ;

const sortableListItemSource = {
  beginDrag(props) {
    return { idx: props.idx };
  },

  endDrag(props, monitor, component) {
    if (!monitor.didDrop()) {
      return;
    }
    const dragItem = monitor.getItem();
    const dropItem = monitor.getDropResult();

    if (typeof props.onDrop === 'function') {
      props.onDrop({ from: dragItem.idx, to: dropItem.idx });
    }
  }
};

function collectSource(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const sortableListItemTarget = {
  drop(props) {
    return { idx: props.idx };
  }
};

function collectTarget(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class SortableListItem extends React.Component {
  render() {
    const { connectDragSource, connectDropTarget } = this.props;
    return connectDropTarget(connectDragSource(
      <div>
        {this.props.children}
      </div>
    ));
  }
}

class SortableList extends React.Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

module.exports = {
  SortableList: DragDropContext(HTML5Backend)(SortableList),
  SortableListItem: DropTarget('SortableList', sortableListItemTarget, collectTarget)(DragSource('SortableList', sortableListItemSource, collectSource)(SortableListItem))
};
