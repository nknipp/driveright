// @flow
'use strict';

import React from 'react';
import Relay from 'react-relay';
import { UploadField } from '@navjobs/upload';
import Datetime from 'react-datetime';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import moment from 'moment';

require('react-datetime/css/react-datetime.css');
require('react-draft-wysiwyg/dist/react-draft-wysiwyg.css');

const { confirm, error, Spinner } = require('./feedback');

type CourseDay = {
  date: Date,
  timeFrom: string,
  timeTo: string,
  lesson: string,
  instructor: string
};

type Props = {
  drivingSchool: {
    id: string,
    website: string,
    stores: {
      edges: [{
        id: string,
        address: {
          street: string,
          city: string
        }
      }]
    },
    courses: {
      edges: [{
        node: {
          id: string,
          storeId: string,
          tag: string,
          title: string,
          description: string,
          availableSeats: number,
          filename: string,
          courseDays: Array<CourseDay>
        }
      }]
    },
    stores: {
      edges: [{
        node: {
          id: string,
          address: {
            street: string,
            city: string
          }
        }
      }]
    }
  }
};

type State = {
  mode: 'add' | 'edit' | 'view',
  courseId?: string,
  storeId: string,
  drivingSchoolId: string,
  tag: string,
  title: string,
  availableSeats?: number,
  filename?: string,
  courseDays: Array<CourseDay>,
  editorState: EditorState
};

const constraints = {
  title: {
    presence: { message: 'Bitte geben Sie den Titel des Kurses ein.' }
  }
};

const wrapperStyle = {
  border: '1px solid #CCC5B9',
  backgroundColor: '#fffcf5',
  borderRadius: '4px',
  padding: '7px 18px'
};

const emptyCourseDay = {
  date: new Date(),
  timeFrom: '',
  timeTo: '',
  lesson: '',
  instructor: ''
};

class CoursesComponent extends React.Component<Props, State> {

  state = {
    mode: 'view',
    courseId: undefined,
    storeId: '',
    drivingSchoolId: '',
    tag: '',
    title: '',
    availableSeats: 0,
    filename: undefined,
    courseDays: [ Object.assign({}, emptyCourseDay)],
    editorState: EditorState.createEmpty()
  };
  files: Array<string> = [];
  deleteCourse: Object = {};

  static getEditorStateFromHtml(content) {
    const contentBlock = htmlToDraft(content);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      return EditorState.createWithContent(contentState);
    }
    return EditorState.createEmpty();
  }

  static getHtmlFromEditorState(editorState: EditorState) {
    return draftToHtml(convertToRaw(editorState.getCurrentContent()));
  }

  handleAdd = () => {
    const store = this.props.drivingSchool.stores.edges.slice(0, 1)[0];
    this.setState({
      mode: 'add',
      courseId: undefined,
      storeId: store.node.id,
      tag: '',
      title: '',
      availableSeats: 0,
      filename: '',
      courseDays: [ Object.assign({}, emptyCourseDay)],
      editorState: EditorState.createEmpty()
    });
  };

  handleAddCourseDay = index => {
    const courseDays = this.state.courseDays.slice();
    courseDays.splice(index + 1, 0, Object.assign({}, emptyCourseDay));
    this.setState({ courseDays });
  };

  handleDeleteCourseDay = index => {
    if (this.state.courseDays.length > 1) {
      const courseDays = this.state.courseDays.slice();
      courseDays.splice(index, 1);
      this.setState({ courseDays });
    }
  };

  handleCancel = () => {
    this.setState({ mode: 'view' });
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleChangeCourseDay = (index, key) => data => {
    const courseDays = this.state.courseDays.slice();
    courseDays[index][key] = key === 'date' ? data.toDate() : data.target.value;
    // $FlowIgnore
    this.setState({ courseDays });
  };

  handleChangeEditorState = (editorState) => {
    this.setState({ editorState });
  };

  handleEdit = course => {
    const node = course.node;
    this.setState({
      mode: 'edit',
      courseId: node.id,
      storeId: node.storeId,
      tag: node.tag,
      title: node.title,
      availableSeats: node.availableSeats,
      filename: node.filename,
      courseDays: node.courseDays,
      editorState: CoursesComponent.getEditorStateFromHtml(node.description)
    });
  };

  handleConfirmDelete = (course) => {
    this.deleteCourse = course.node;
    confirm('Löschen bestätigen', 'Wollen Sie den Kurs wirklich löschen?', this.handleDelete);
  };

  handleDelete = () => {
    Spinner.start();
    Relay.Store.commitUpdate(
      new DeleteCourseMutation({
        courseId: this.deleteCourse.id,
        drivingSchoolId: this.props.drivingSchool.id,
        drivingSchool: null
      }), {
        onSuccess: () => {
          Spinner.success('Kurs wurde gelöscht');
          this.setState({ mode: 'view' });
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Kurs konnte nicht gelöscht werden');
        }
      }
    );
  };

  handleImageSelected = (files) => {
    this.setState({ filename: files[0].name });
    this.files = files;
    // $FlowIgnore
    $('#selected-image').html(files[0].name);
  };

  handleSave = () => {
    // $FlowIgnore
    const validationResult = validate(this.state, constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      error(validationResult, 'Bitte geben Sie die Daten ein.');
      return;
    }

    const file = this.files.length > 0 ? this.files[0] : undefined;

    Spinner.start();
    if (this.state.mode === 'add') {
      Relay.Store.commitUpdate(
        new AddCourseMutation({
          drivingSchoolId: this.props.drivingSchool.id,
          storeId: this.state.storeId,
          tag: this.state.tag,
          title: this.state.title,
          description: CoursesComponent.getHtmlFromEditorState(this.state.editorState),
          availableSeats: this.state.availableSeats,
          file: file,
          courseDays: this.state.courseDays,
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Kurs wurde eingefügt');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Kurs konnte nicht eingefügt werden');
          }
        }
      );
    } else if (this.state.mode === 'edit') {
      //HACK Aus irgendeinem Grund sind bei einem Update weitere Properties in dem Kurstage-Array enthalten
      const courseDays = this.state.courseDays.map(courseDay => {
        const clone = Object.assign({}, courseDay);
        delete clone.__dataID__;
        return clone;
      });
      Relay.Store.commitUpdate(
        new UpdateCourseMutation({
          courseId: this.state.courseId,
          drivingSchoolId: this.props.drivingSchool.id,
          storeId: this.state.storeId,
          tag: this.state.tag,
          title: this.state.title,
          description: CoursesComponent.getHtmlFromEditorState(this.state.editorState),
          availableSeats: this.state.availableSeats,
          file: file,
          courseDays: courseDays,
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Kurs wurde aktualisiert');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Kurs konnte nicht aktualisiert werden');
          }
        }
      );
    }
  };

  renderEditBar() {
    if (this.state.mode === 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Neuen Kurs anlegen" onClick={this.handleAdd}><i className="ti-plus"></i></button>
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    }
  }

  renderCourse(course) {
    const node = course.node;
    return (
      <div className="row" key={node.id}>
        <div className="col-sm-2">
          {
            node.filename ? <img src={`${this.props.drivingSchool.website}/static/images/courses/${node.filename}`} style={{ width: '120px'}}/> : ''
          }
        </div>
        <div className="col-sm-1">{node.tag}</div>
        <div className="col-sm-2">{node.title}</div>
        <div className="col-sm-3">{node.description}</div>
        <div className="col-sm-1">{node.availableSeats}</div>
        <div className="col-sm-1">{new Date(node.courseDays[0].date).toLocaleDateString()}</div>
        <div className="col-sm-2">
          <button type="button" className="btn btn-primary pull-right" title="Kurs löschen" onClick={() => this.handleConfirmDelete({node})}>
            <i className="ti-trash"></i>
          </button>
          <button type="button" className="btn btn-primary pull-right" style={{ marginRight: '10px' }} title="Kurs bearbeiten" onClick={() => this.handleEdit({node})}>
            <i className="ti-pencil"></i>
          </button>
        </div>
      </div>
    );
  }

  renderStores() {
    const storeEdges = this.props.drivingSchool.stores.edges;
    return (
      <div className="form-group">
        <label>Filiale</label>
        <select name="storeId" className="form-control border-input" onChange={this.handleChange} value={this.state.storeId}>
          {
            storeEdges.map(store => {
              return <option key={store.node.id} value={store.node.id}>{store.node.address.city}, {store.node.address.street}</option>;
            })
          }
        </select>
      </div>
    );
  }

  renderCourseTag() {
    return (
      <div className="form-group">
        <label>Kursart</label>
        <select name="tag" className="form-control border-input" onChange={this.handleChange} value={this.state.tag}>
          <option></option>
          <option>Intensiv</option>
          <option>Erste Hilfe</option>
          <option>Ferien</option>
          <option>ASF</option>
          <option>B96</option>
          <option>BKF</option>
          <option>MPU</option>
        </select>
      </div>
    );
  }

  renderCourseDays() {
    return (
      <div className="form-group">
        {
          this.state.courseDays.map((courseDay, idx) => {
            return (
              <div className="row" key={idx}>
                <div className="col-md-3 col-sm-12">
                  <label>Datum</label>
                  <Datetime value={moment(courseDay.date)}
                            onChange={this.handleChangeCourseDay(idx, 'date')}
                            timeFormat={false}
                            closeOnSelect={true}
                            locale="de-DE"
                            inputProps={{
                              className: 'form-control border-input'
                            }}
                  />
                </div>
                <div className="col-md-2 col-sm-12">
                  <label>Von</label>
                  <input type="text" className="form-control border-input" onChange={this.handleChangeCourseDay(idx, 'timeFrom')} value={courseDay.timeFrom}/>
                </div>
                <div className="col-md-2 col-sm-12">
                  <label>Bis</label>
                  <input type="text" className="form-control border-input" onChange={this.handleChangeCourseDay(idx, 'timeTo')} value={courseDay.timeTo}/>
                </div>
                <div className="col-md-2 col-sm-12">
                  <label>Lektion</label>
                  <input type="text" className="form-control border-input" onChange={this.handleChangeCourseDay(idx, 'lesson')} value={courseDay.lesson}/>
                </div>
                <div className="col-md-2 col-sm-12">
                  <label>Kursleiter</label>
                  <input type="text" className="form-control border-input" onChange={this.handleChangeCourseDay(idx, 'instructor')} value={courseDay.instructor}/>
                </div>
                <div className="col-md-1 col-sm-12" style={{ paddingTop: '38px' }}>
                  <button type="button" onClick={() => this.handleAddCourseDay(idx)}><i className="ti-plus"></i></button>
                  {
                    this.state.courseDays.length > 1 ? <button type="button" style={{ marginLeft: '5px' }} onClick={() => this.handleDeleteCourseDay(idx)}><i className="ti-minus"></i></button> : ''
                  }
                </div>
              </div>
            );
          })
        }
      </div>
    );
  }

  renderAdd() {
    return (
      <div className="card">
        <form encType="multipart/form-data">
          { this.renderStores() }
          { this.renderCourseTag() }
          <div className="form-group">
            <label>Titel</label>
            <input name="title" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.title}/>
          </div>
          <div className="form-group">
            <label>Beschreibung</label>
            <Editor editorState={this.state.editorState}
                    onEditorStateChange={this.handleChangeEditorState}
                    toolbar={{
                      options: ['inline', 'list']
                    }}
                    wrapperStyle={wrapperStyle}
                    editorStyle={{ backgroundColor: '#fff' }}
            />
          </div>
          <div className="form-group">
            <label>Anzahl Plätze</label>
            <input name="availableSeats" type="number" className="form-control border-input" onChange={this.handleChange} value={this.state.availableSeats}/>
          </div>
          <div className="form-group">
            <label>Bild</label>
            <UploadField
              onFiles={ files => this.handleImageSelected(files) }
              containerProps={{
                className: 'resume_import'
              }}
              uploadProps={{
                accept: '.jpg,.jpeg,.png'
              }}>
              <div>
                Bild hochladen: <span id="selected-image">Kein Bild ausgewählt</span>
              </div>
            </UploadField>
          </div>
          {this.renderCourseDays()}
        </form>
      </div>
    );
  }

  renderEdit() {
    return (
      <div className="card">
        <form encType="multipart/form-data">
          { this.renderStores() }
          { this.renderCourseTag() }
          <div className="form-group">
            <label>Titel</label>
            <input name="title" type="text" className="form-control border-input" onChange={this.handleChange}
                   value={this.state.title}/>
          </div>
          <div className="form-group">
            <label>Beschreibung</label>
            <Editor editorState={this.state.editorState}
                    onEditorStateChange={this.handleChangeEditorState}
                    toolbar={{
                      options: ['inline', 'list']
                    }}
                    wrapperStyle={wrapperStyle}
                    editorStyle={{backgroundColor: '#fff'}}
            />
          </div>
          <div className="form-group">
            <label>Anzahl Plätze</label>
            <input name="availableSeats" type="number" className="form-control border-input"
                   onChange={this.handleChange} value={this.state.availableSeats}/>
          </div>
          <div className="form-group">
            <label>Bild</label>
            <UploadField
              onFiles={files => this.handleImageSelected(files)}
              containerProps={{
                className: 'resume_import'
              }}
              uploadProps={{
                accept: '.jpg,.jpeg,.png'
              }}>
              <div>
                Bild hochladen: <span id="selected-image">Kein Bild ausgewählt</span>
              </div>
            </UploadField>
          </div>
          {this.renderCourseDays()}
        </form>
      </div>
    );
  }

  renderView() {
    const courseEdges = this.props.drivingSchool.courses.edges;
    return (
      <div className="card">
        { courseEdges.map((course) => this.renderCourse(course)) }
      </div>
    );
  }

  render() {
    let content;
    switch (this.state.mode) {
      case 'add':
        content = this.renderAdd();
        break;

      case 'edit':
        content = this.renderEdit();
        break;

      case 'view':
      default:
        content = this.renderView();
    }

    return (
      <div>
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class AddCourseMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { addCourse }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      storeId: this.props.storeId,
      tag: this.props.tag,
      title: this.props.title,
      description: this.props.description,
      availableSeats: this.props.availableSeats,
      courseDays: this.props.courseDays
    };
  }

  getFiles() {
    return {
      file: this.props.file
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddCourseMutationPayload {
        drivingschool {
          courses
        }
        courseEdge
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'courses',
      edgeName: 'courseEdge',
      rangeBehaviors: {
        '': 'prepend'
      }
    }];
  }
}

class UpdateCourseMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updateCourse }`;
  }

  getVariables() {
    return {
      courseId: this.props.courseId,
      drivingSchoolId: this.props.drivingSchoolId,
      storeId: this.props.storeId,
      tag: this.props.tag,
      title: this.props.title,
      description: this.props.description,
      availableSeats: this.props.availableSeats,
      courseDays: this.props.courseDays
    };
  }

  getFiles() {
    return {
      file: this.props.file
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateCourseMutationPayload {
        drivingschool {
          id
          courses
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class DeleteCourseMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { deleteCourse }`;
  }

  getVariables() {
    return {
      courseId: this.props.courseId,
      drivingSchoolId: this.props.drivingSchoolId
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
          fragment on DrivingSchool {
              id
          }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on DeleteCourseMutationPayload {
        drivingschool {
          id
          courses
        }
        deletedCourseId
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'courses',
      deletedIDFieldName: 'deletedCourseId'
    }];
  }
}

const Courses = Relay.createContainer(
  CoursesComponent,
  {
    fragments: {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          website
          courses(first: 1000) {
            edges {
              cursor
              node {
                id
                storeId
                tag
                title
                description
                availableSeats
                filename
                courseDays {
                  date
                  timeFrom
                  timeTo
                  lesson
                  instructor
                }
              }
            }
          }
          stores(first: 1000) {
            edges {
              cursor
              node {
                id
                address {
                  street
                  city
                }
              }
            }
          }
          ${AddCourseMutation.getFragment('drivingSchool')}
          ${UpdateCourseMutation.getFragment('drivingSchool')}
          ${DeleteCourseMutation.getFragment('drivingSchool')}
        }`
    }
  }
);

const CoursesQuery = {
  drivingSchool: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  Courses,
  CoursesQuery
};
