// @ flow
'use strict';

const React = require('react');
const Relay = require('react-relay');
const { withRouter } = require('react-router');
const Link = require('react-router/lib/Link');

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.getRoutingFromRole = this.getRoutingFromRole.bind(this);
  }

  getRoutingFromRole(role: string) {
    switch (role.toLowerCase()) {
      case 'admin':
        return (
          <ul className="nav">
            <li>
              <Link to='/fahrschulen'>
                <i className="ti-view-list-alt"></i>
                <p>Fahrschulen</p>
              </Link>
            </li>
          </ul>
        );
      case 'drivingschooladmin':
        return (
          <ul className="nav">
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}`}>
                <i className="ti-view-list-alt"></i>
                <p>Fahrschule</p>
              </Link>
            </li>
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}/filialen`}>
                <i className="ti-direction-alt"></i>
                <p>Filialen</p>
              </Link>
            </li>
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}/team`}>
                <i className="ti-user"></i>
                <p>Team</p>
              </Link>
            </li>
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}/fahrzeuge`}>
                <i className="ti-car"></i>
                <p>Fahrzeuge</p>
              </Link>
            </li>
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}/bestanden`}>
                <i className="ti-face-smile"></i>
                <p>Bestanden</p>
              </Link>
            </li>
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}/kurse`}>
                <i className="ti-blackboard"></i>
                <p>Kurse & Seminare</p>
              </Link>
            </li>
            <li>
              <Link to={`/fahrschulen/${this.props.viewer.user.drivingSchoolId}/teilnehmer`}>
                <i className="ti-gift"></i>
                <p>Gewinnspiel-Teilnehmer</p>
              </Link>
            </li>
          </ul>
        );
      case 'drivinginstructor': return '';
      case 'learner': return '';
      default:
        return '';
    }
  }

  componentDidMount() {
    if ($(window).width() <= 991) {
      pd.initRightMenu();
    }
    $('ul.nav li a').click(function() {
      $('ul.nav li').each(function(index, element) {
        $(element).removeClass('active');
      });
      const $this = $(this);
      $this.parent().addClass('active');
      $('a.navbar-brand').html($this.text()).attr('href', $this.attr('href'));
    });

  }

  render() {
    return (
      <div className="wrapper">
        <div className="sidebar" data-background-color="white" data-active-color="danger">
          <div className="sidebar-wrapper">
            <div className="logo">
              <a href="https://www.drive-right.de" className="simple-text">
                <strong>driveright</strong>
              </a>
            </div>
            {this.getRoutingFromRole(this.props.viewer.user.role)}
          </div>
        </div>

        <div className="main-panel">
          <nav className="navbar navbar-default">
            <div className="container-fluid">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar bar1"></span>
                  <span className="icon-bar bar2"></span>
                  <span className="icon-bar bar3"></span>
                </button>
                <a className="navbar-brand" href="#"></a>
              </div>
              <div className="collapse navbar-collapse">
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <Link to="/profil">
                      <i className="ti-settings"></i>&nbsp;
                      <p>Profil</p>
                    </Link>
                  </li>
                  <li>
                    <a href="/logout">
                      <i className="ti-close"></i>&nbsp;
                      <p>Abmelden</p>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>

          <div className="content">
            { React.cloneElement(this.props.children) }
          </div>

          <footer className="footer">
            <div className="container-fluid">
              <div className="row">
                <nav className="pull-left">
                  <ul>
                    <li>
                      <a href="https://www.drive-right.de">
                        <strong>driveright</strong>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.drive-right.de/impressum" rel="noopener noreferrer" target="_blank">
                        Impressum
                      </a>
                    </li>
                    <li>
                      <a href="https://www.drive-right.de/datenschutz" rel="noopener noreferrer" target="_blank">
                        Datenschutz
                      </a>
                    </li>
                  </ul>
                </nav>
                <div className="copyright pull-right">
                  <div>
                    &copy; {new Date().getFullYear()},
                    made with <i className="fa fa-heart heart"></i> by <a href="https://www.drive-right.de" rel="noopener noreferrer" target="_blank">driveright</a>
                  </div>
                </div>
              </div>
            </div>
          </footer>

        </div>
      </div>
    );
  }
}

const DashboardIndex = () => {
  return <div>Dashboard</div>;
};

const DashboardPage = Relay.createContainer(
  Dashboard,
  {
    fragments: {
      viewer: () => Relay.QL`
        fragment on Viewer {
          user {
            id
            username
            firstname
            lastname
            drivingSchoolId
            role
            permissions
            isloggedin
          }
        }`
    }
  }
);

const DashboardQuery = {
  viewer: () => Relay.QL`
    query {
      viewer
    }`
};

module.exports = {
  Dashboard: withRouter(DashboardPage),
  DashboardIndex,
  DashboardQuery
};