// @ flow
'use strict';

const React = require('react');
const Relay = require('react-relay');
const { withRouter } = require('react-router');

const { error, Spinner } = require('./feedback');

class DrivingSchool extends React.Component {
  constructor(props) {
    super(props);

    this.handleEdit = this.handleEdit.bind(this);
    this.handleInvitation = this.handleInvitation.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      id: props.drivingschool.id,
      name: props.drivingschool.name || '',
      street: props.drivingschool.address.street || '',
      postcode: props.drivingschool.address.postcode || '',
      city: props.drivingschool.address.city || '',
      website: props.drivingschool.website || '',
      emailAddress: props.drivingschool.emailAddress || '',
      phone: props.drivingschool.phone || '',
      fax: props.drivingschool.fax || '',
      commercialRegister: props.drivingschool.commercialRegister || '',
      taxNumber: props.drivingschool.taxNumber || '',
      taxIdNumber: props.drivingschool.taxIdNumber || '',
      owner: props.drivingschool.owner || '',
      managingDirector: props.drivingschool.managingDirector || '',
      operativeManager: props.drivingschool.operativeManager || '',
      regulatingAuthority: props.drivingschool.regulatingAuthority || '',
      facebookId: props.drivingschool.facebookId || '',
      instagramId: props.drivingschool.instagramId || '',
      onlineLearning: props.drivingschool.onlineLearning || '',
      mode: 'view'
    };
    this.constraints = {
      name: { presence: { message: 'Bitte geben Sie einen Name ein.' }},
      street: { presence: { message: 'Bitte geben Sie eine Straße ein.' }},
      postcode: { presence: { message: 'Bitte geben Sie eine Postleitzahl ein.' }},
      city: { presence: { message: 'Bitte geben Sie einen Ort ein.' }}
    };
  }

  handleEdit() {
    this.setState({ mode: 'edit' });
  }

  handleInvitation() {
    this.setState({ mode: 'invitation' });
  }

  handleSave() {
    const validationResult = validate(this.state, this.constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      error(validationResult, 'Bitte geben Sie die Daten ein.');
      return;
    }

    if (this.state.mode === 'edit') {
      Spinner.start();
      Relay.Store.commitUpdate(
        new UpdateDrivingSchoolMutation({
          id: this.state.id,
          name: this.state.name,
          street: this.state.street,
          postcode: this.state.postcode,
          city: this.state.city,
          website: this.state.website,
          emailAddress: this.state.emailAddress,
          phone: this.state.phone,
          fax: this.state.fax,
          commercialRegister: this.state.commercialRegister,
          taxNumber: this.state.taxNumber,
          taxIdNumber: this.state.taxIdNumber,
          owner: this.state.owner,
          managingDirector: this.state.managingDirector,
          operativeManager: this.state.operativeManager,
          regulatingAuthority: this.state.regulatingAuthority,
          facebookId: this.state.facebookId,
          instagramId: this.state.instagramId,
          onlineLearning: this.state.onlineLearning,
          drivingschool: null
        }), {
          onSuccess: () => {
            Spinner.success('Fahrschuldaten wurde aktualisiert.');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Fahrschuldaten konnten nicht aktualisiert werden.');
          }
        }
      );
    } else if (this.state.mode === 'invitation') {
      Spinner.start();
      const serializedData = `{
        "query": "mutation { inviteUser(id: \\"${this.state.id}\\", emailAddress: \\"${this.state.emailAddress}\\", role: 'drivingschooladmin') { success errors }}",
        "variables": null
      }`;
      $.ajax({
        url: '/q',
        method: 'POST',
        contentType: 'application/json',
        data: serializedData,
        success: function(data) {
          if (!data.data.sendContactRequest.success) {
            Spinner.failure(data.data.sendContactRequest.errors, 'Einladung konnte nicht verschickt werden.');
          } else {
            $('#contactRequestForm').hide();
            Spinner.success('Einladung wurde verschickt.');
          }
        }
      });
    }
  }

  handleCancel() {
    this.setState({ mode: 'view' });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  renderEditBar() {
    if (this.state.mode === 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Bearbeiten" onClick={this.handleEdit}><i className="ti-pencil"></i></button>
          {/*<button type="button" className="btn editbar-btn" title="Einladen" onClick={this.handleInvitation}><i className="ti-plus"></i></button>*/}
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    }
  }

  renderView() {
    return (
      <div className="card">
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Name</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.name}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Inhaber</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.owner}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Geschäftsführer</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.managingDirector}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Verantwortlicher Leiter</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.operativeManager}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Adresse</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.address.street}, {this.props.drivingschool.address.postcode} {this.props.drivingschool.address.city}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Website</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.website}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>E-Mail</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.emailAddress}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Telefon</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.phone}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Fax</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.fax}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Facebook ID</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.facebookId}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Instagram ID</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.instagramId}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Online Lernen</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.onlineLearning}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Handelsregister</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.commercialRegister}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Steuernummer</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.taxNumber}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Ust-Id</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.taxIdNumber}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Aufsichtsbehörde</label></div>
          <div className="col-sm-12 col-md-10">{this.props.drivingschool.regulatingAuthority}</div>
        </div>
      </div>
    );
  }

  renderEdit() {
    return (
      <div className="card">
        <form>
          <div className="form-group">
            <label>Name</label>
            <input name="name" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.name}/>
          </div>
          <div className="form-group">
            <label>Inhaber</label>
            <input name="owner" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.owner}/>
          </div>
          <div className="form-group">
            <label>Geschäftsführer</label>
            <input name="managingDirector" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.managingDirector}/>
          </div>
          <div className="form-group">
            <label>Verantwortlicher Leiter</label>
            <input name="operativeManager" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.operativeManager}/>
          </div>
          <div className="form-group">
            <label>Strasse</label>
            <input name="street" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.street}/>
          </div>
          <div className="form-group">
            <label>Postleitzahl</label>
            <input name="postcode" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.postcode}/>
          </div>
          <div className="form-group">
            <label>Ort</label>
            <input name="city" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.city}/>
          </div>
          <div className="form-group">
            <label>Website</label>
            <input name="website" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.website}/>
          </div>
          <div className="form-group">
            <label>E-Mail</label>
            <input name="emailAddress" type="email" className="form-control border-input" onChange={this.handleChange} value={this.state.emailAddress}/>
          </div>
          <div className="form-group">
            <label>Telefon</label>
            <input name="phone" type="tel" className="form-control border-input" onChange={this.handleChange} value={this.state.phone}/>
          </div>
          <div className="form-group">
            <label>Fax</label>
            <input name="fax" type="tel" className="form-control border-input" onChange={this.handleChange} value={this.state.fax}/>
          </div>
          <div className="form-group">
            <label>Facebook ID</label>
            <input name="facebookId" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.facebookId}/>
          </div>
          <div className="form-group">
            <label>Instagram ID</label>
            <input name="instagramId" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.instagramId}/>
          </div>
          <div className="form-group">
            <label>Online Lernen</label>
            <input name="onlineLearning" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.onlineLearning}/>
          </div>
          <div className="form-group">
            <label>Handelsregister</label>
            <input name="commercialRegister" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.commercialRegister}/>
          </div>
          <div className="form-group">
            <label>Steuernummer</label>
            <input name="taxNumber" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.taxNumber}/>
          </div>
          <div className="form-group">
            <label>Ust-Id</label>
            <input name="taxIdNumber" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.taxIdNumber}/>
          </div>
          <div className="form-group">
            <label>Aufsichtsbehörde</label>
            <textarea name="regulatingAuthority" className="form-control border-input" onChange={this.handleChange} value={this.state.regulatingAuthority}/>
          </div>
        </form>
      </div>
    );
  }

  renderInvitation() {
    return (
      <div className="card">
        <form>
          <div className="form-group">
            <label>E-Mail</label>
            <input name="emailAddress" type="email" className="form-control border-input" onChange={this.handleChange} value={this.state.emailAddress}/>
          </div>
        </form>
      </div>
    );
  }

  render() {
    let content;
    if (this.state.mode === 'edit') {
      content = this.renderEdit();
    } else if (this.state.mode === 'invitation') {
      content = this.renderInvitation();
    } else {
      content = this.renderView();
    }

    return (
      <div>
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class AddDrivingSchool extends React.Component {
  constructor(props) {
    super(props);

    this.handleSave = this.handleSave.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      name: '',
      street: '',
      postcode: '',
      city: '',
      website: '',
      emailAddress: '',
      phone: '',
      fax: '',
      commercialRegister: '',
      taxNumber: '',
      taxIdNumber: '',
      owner: '',
      managingDirector: '',
      operativeManager: '',
      regulatingAuthority: '',
      facebookId: '',
      instagramId: '',
      onlineLearning: ''
    };
    this.constraints = {
      name: {
        presence: { message: 'Bitte geben Sie einen Name ein.' }
      }
    };
  }

  handleSave() {
    const validationResult = validate(this.state, this.constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      error(validationResult, 'Bitte geben Sie die Daten ein');
      return;
    }

    Spinner.start();
    Relay.Store.commitUpdate(
      new AddDrivingSchoolMutation({
        name: this.state.name,
        street: this.state.street,
        postcode: this.state.postcode,
        city: this.state.city,
        website: this.state.website,
        emailAddress: this.state.emailAddress,
        phone: this.state.phone,
        fax: this.state.fax,
        commercialRegister: this.state.commercialRegister,
        taxNumber: this.state.taxNumber,
        taxIdNumber: this.state.taxIdNumber,
        owner: this.state.owner,
        managingDirector: this.state.managingDirector,
        operativeManager: this.state.operativeManager,
        regulatingAuthority: this.state.regulatingAuthority,
        facebookId: this.state.facebookId,
        instagramId: this.state.instagramId,
        onlineLearning: this.state.onlineLearning,
        viewer: null
      }), {
        onSuccess: () => {
          Spinner.success('Fahrschule wurde angelgt');
          this.props.router.push('/fahrschulen');
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Fahrschule konnte nicht angelegt werden');
        }
      }
    );
  }

  handleCancel() {
    this.props.router.goBack();
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  renderEditBar() {
    return (
      <div className="editbar">
        <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
        <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
      </div>
    );
  }

  renderEdit() {
    return (
      <div className="card">
        <form>
          <div className="form-group">
            <label>Name</label>
            <input name="name" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.name}/>
          </div>
          <div className="form-group">
            <label>Inhaber</label>
            <input name="owner" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.owner}/>
          </div>
          <div className="form-group">
            <label>Geschäftsführer</label>
            <input name="managingDirector" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.managingDirector}/>
          </div>
          <div className="form-group">
            <label>Verantwortlicher Leiter</label>
            <input name="operativeManager" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.operativeManager}/>
          </div>
          <div className="form-group">
            <label>Strasse</label>
            <input name="street" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.street}/>
          </div>
          <div className="form-group">
            <label>Postleitzahl</label>
            <input name="postcode" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.postcode}/>
          </div>
          <div className="form-group">
            <label>Ort</label>
            <input name="city" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.city}/>
          </div>
          <div className="form-group">
            <label>Website</label>
            <input name="website" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.website}/>
          </div>
          <div className="form-group">
            <label>E-Mail</label>
            <input name="emailAddress" type="email" className="form-control border-input" onChange={this.handleChange} value={this.state.emailAddress}/>
          </div>
          <div className="form-group">
            <label>Telefon</label>
            <input name="phone" type="tel" className="form-control border-input" onChange={this.handleChange} value={this.state.phone}/>
          </div>
          <div className="form-group">
            <label>Fax</label>
            <input name="fax" type="tel" className="form-control border-input" onChange={this.handleChange} value={this.state.fax}/>
          </div>
          <div className="form-group">
            <label>Facebook ID</label>
            <input name="facebookId" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.facebookId}/>
          </div>
          <div className="form-group">
            <label>Facebook ID</label>
            <input name="instagramId" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.instagramId}/>
          </div>
          <div className="form-group">
            <label>Online Lernen</label>
            <input name="onlineLearning" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.onlineLearning}/>
          </div>
          <div className="form-group">
            <label>Handelsregister</label>
            <input name="commercialRegister" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.commercialRegister}/>
          </div>
          <div className="form-group">
            <label>Steuernummer</label>
            <input name="taxNumber" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.taxNumber}/>
          </div>
          <div className="form-group">
            <label>Ust-Id</label>
            <input name="taxIdNumber" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.taxIdNumber}/>
          </div>
          <div className="form-group">
            <label>Aufsichtsbehörde</label>
            <textarea name="regulatingAuthority" className="form-control border-input" onChange={this.handleChange} value={this.state.regulatingAuthority}/>
          </div>
        </form>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderEditBar()}
        {this.renderEdit()}
      </div>
    );
  }
}

class AddDrivingSchoolMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { addDrivingSchool }`;
  }

  getVariables() {
    return {
      name: this.props.name,
      street: this.props.street,
      postcode: this.props.postcode,
      city: this.props.city,
      website: this.props.website,
      emailAddress: this.props.emailAddress,
      phone: this.props.phone,
      fax: this.props.fax,
      commercialRegister: this.props.commercialRegister,
      taxNumber: this.props.taxNumber,
      taxIdNumber: this.props.taxIdNumber,
      owner: this.props.owner,
      managingDirector: this.props.managingDirector,
      operativeManager: this.props.operativeManager,
      regulatingAuthority: this.props.regulatingAuthority,
      facebookId: this.props.facebookId,
      instagramId: this.props.instagramId,
      onlineLearning: this.props.onlineLearning
    };
  }

  static get fragments() {
    return {
      viewer: () => Relay.QL`
        fragment on Viewer {
          user {
            id
          }
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddDrivingSchoolMutationPayload {
        viewer {
          alldrivingschools
        }
        drivingSchoolEdge
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      connectionName: 'allDrivingSchools',
      edgeName: 'drivingSchoolEdge',
      rangeBehaviors: {
        '': 'append'
      }
    }];
  }
}

class UpdateDrivingSchoolMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updateDrivingSchool }`;
  }

  getVariables() {
    return {
      id: this.props.id,
      name: this.props.name,
      street: this.props.street,
      postcode: this.props.postcode,
      city: this.props.city,
      website: this.props.website,
      emailAddress: this.props.emailAddress,
      phone: this.props.phone,
      fax: this.props.fax,
      commercialRegister: this.props.commercialRegister,
      taxNumber: this.props.taxNumber,
      taxIdNumber: this.props.taxIdNumber,
      owner: this.props.owner,
      managingDirector: this.props.managingDirector,
      operativeManager: this.props.operativeManager,
      regulatingAuthority: this.props.regulatingAuthority,
      facebookId: this.props.facebookId,
      instagramId: this.props.instagramId,
      onlineLearning: this.props.onlineLearning
    };
  }

  static get fragments() {
    return {
      drivingschool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          name
          address {
            street
            postcode
            city
          }
          website
          emailAddress
          phone
          fax
          facebookId
          instagramId
          onlineLearning
          commercialRegister
          taxNumber
          taxIdNumber
          owner
          managingDirector
          operativeManager
          regulatingAuthority
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateDrivingSchoolMutationPayload {
        changedDrivingSchool
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        changedDrivingSchool: this.props.id
      }
    }];
  }
}

const AddDrivingSchoolPage = Relay.createContainer(
  AddDrivingSchool,
  {
    fragments: {
      drivingschool: () => Relay.QL`
        fragment on DrivingSchool {
          ${AddDrivingSchoolMutation.getFragment('viewer')}
        }`
    }
  }
);

const DrivingSchoolPage = Relay.createContainer(
  DrivingSchool,
  {
    fragments: {
      drivingschool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          name
          address {
            street
            postcode
            city
          }
          website
          emailAddress
          phone
          fax
          facebookId
          instagramId
          onlineLearning
          commercialRegister
          taxNumber
          taxIdNumber
          owner
          managingDirector
          operativeManager
          regulatingAuthority
          ${UpdateDrivingSchoolMutation.getFragment('drivingschool')}
        }`
    }
  }
);

const AddDrivingSchoolQuery = {
  drivingschool: () => Relay.QL`
    query {
      viewer
    }`
};

const DrivingSchoolQuery = {
  drivingschool: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  AddDrivingSchool: withRouter(AddDrivingSchoolPage),
  AddDrivingSchoolQuery,
  DrivingSchool: DrivingSchoolPage,
  DrivingSchoolQuery
};
