// @ flow
'use strict';

const React = require('react');
const Relay = require('react-relay');
const { withRouter } = require('react-router');
const Link = require('react-router/lib/Link');

class DrivingSchools extends React.Component {
  constructor(props) {
    super(props);
    this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd(event) {
    this.props.router.push('/fahrschulen/add');
  }

  renderEditBar() {
    return (
      <div className="editbar">
        <button type="button" className="btn editbar-btn" title="Neue Fahrschule anlegen" onClick={this.handleAdd}><i className="ti-plus"></i></button>
      </div>
    );
  }

  render() {
    const drivingschoolEdges = this.props.drivingschools.alldrivingschools.edges;
    return (
      <div className="container-fluid">
        {this.renderEditBar()}
        <div className="card">
          {drivingschoolEdges.map((drivingschoolEdge) => {
            return (
              <div className="row" key={drivingschoolEdge.node.id}>
                <div className="col-sm-3">{drivingschoolEdge.node.name}</div>
                <div className="col-sm-4">{drivingschoolEdge.node.address.street}</div>
                <div className="col-sm-1">{drivingschoolEdge.node.address.postcode}</div>
                <div className="col-sm-3">{drivingschoolEdge.node.address.city}</div>
                <div className="col-sm-1">
                  <Link to={`/fahrschulen/${drivingschoolEdge.node.id}`} className="btn btn-primary pull-right" title="Fahrschule bearbeiten">
                    <i className="ti-pencil"></i>
                  </Link>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const DrivingSchoolsPage = Relay.createContainer(
  DrivingSchools,
  {
    fragments: {
      drivingschools: () => Relay.QL`
        fragment on Viewer {
          alldrivingschools(first: 10000) {
            pageInfo {
              hasPreviousPage
              hasNextPage
              startCursor
              endCursor
            }
            edges {
              cursor
              node {
                id
                name
                address {
                  street
                  postcode
                  city
                }
              }
            }
          }
        }`
    }
  }
);

const DrivingSchoolsQuery = {
  drivingschools: () => Relay.QL`
    query {
      viewer
    }`
};

module.exports = {
  DrivingSchools: withRouter(DrivingSchoolsPage),
  DrivingSchoolsQuery
};