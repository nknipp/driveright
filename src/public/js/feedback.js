// @ flow
'use strict';

function notify(type: string, message: string, title: string = '') {
  if (message instanceof Array) {
    message = message.join('<br/>');
  }
  $.notify({
    title,
    message
  },{
    type
  });
}

function confirm(title, msg, handleOk) {
  $('body').append(`
    <div id="modal-dialog-container" class="modal fade" tabIndex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">${title}</h4>
          </div>
          <div class="modal-body">
            <p>${msg}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            <button id="modal-dialog-container-OK" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
          </div>
        </div>
      </div>
      <script>
        $('#modal-dialog-container').on('hidden.bs.modal', function (e) {
           $(this).data('bs.modal', null).remove()
        });
      </script>
    </div>`
  );
  document.getElementById('modal-dialog-container-OK').addEventListener('click', function() {
    handleOk();
  }, false);
  $('#modal-dialog-container').modal();
}

function error(msg, title) {
  let err = msg;
  if (err instanceof Array) {
    err = err.join('<br/>');
  }
  $('body').append(`
    <div id="spinner-failure-container" class="modal fade" tabIndex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">${title}</h4>
          </div>
          <div class="modal-body">
            <p>${err}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
          </div>
        </div>
      </div>
      <script>
        $('#spinner-failure-container').on('hidden.bs.modal', function (e) {
           $(this).data('bs.modal', null).remove()
        })
      </script>
    </div>`
  );
  $('#spinner-failure-container').modal();
}

function start() {
  $('body').append(`
    <div id="spinner-container" class="container">
      <div class="row">
      <div id="spinner-loader">
        <ul class="spinner">
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>`
  );
}

function success(msg, title) {
  $('#spinner-container').remove();
  notify('info', msg, title);
}

function failure(msg, title) {
  $('#spinner-container').remove();
  error(msg, title);
}

module.exports = {
  confirm,
  error,
  notify,
  Spinner: {
    start,
    success,
    failure
  }
};