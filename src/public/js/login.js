// @ flow
'use strict';

const React = require('react');
const { withRouter } = require('react-router');
const Link = require('react-router/lib/Link');

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.state = {
      username: '',
      password: ''
    };
    this.constraints = {
      username: {
        presence: {
          allowEmpty: false,
          message: 'Bitte geben Sie Ihre E-Mail Adresse ein.<br/>'
        }
      },
      password: {
        presence: {
          allowEmpty: false,
          message: 'Bitte geben Sie Ihr Passwort ein.'
        }
      }
    };
  }

  handleSubmit() {
    const validationResult = validate(this.state, this.constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      $('div.container div.error').html(validationResult).show();
      return;
    }

    $.post('/login', this.state).done((data) => {
      window.location.href = data.redirect;
    }).fail(jqXHR => {
      $('div.container div.error').html(jqXHR.responseText).show();
    });
  }

  handleKeyPress (event) {
    if(event.key === 'Enter') {
      this.handleSubmit();
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
      <div className="container">

        <div className="row align-items-center justify-content-center">

          <div className="col-12 col-sm-5 col-md-5 col-lg-4">

            <form className="sky-form boxed">
              <header><i className="fa fa-users"></i> Melden Sie sich an</header>

              <div className="error text-center" style={{ display: 'none', margin: '20px 0 -20px', color: 'red' }}></div>

              <fieldset className="m-0">

                <label className="mt-20">E-Mail</label>
                <label className="input">
                  <i className="ico-append fa fa-envelope"></i>
                  <input name="username" type="email" value={this.state.username} onKeyPress={this.handleKeyPress} onChange={this.handleChange} className="form-control" placeholder="E-Mail Adresse"/>
                </label>

                <label className="mt-20">Password</label>
                <label className="input">
                  <i className="ico-append fa fa-lock"></i>
                  <input name="password" type="password" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.handleChange} className="form-control" placeholder="Passwort"/>
                </label>
              </fieldset>

              <footer className="clearfix">
                <button type="button" className="btn btn-primary float-right" onClick={this.handleSubmit}><i className="fa fa-check"></i> OK, Log mich ein</button>
                <div className="float-right">
                  <Link to="/passwort-zuruecksetzen">Passwort vergessen?</Link>
                </div>
              </footer>
            </form>

          </div>

        </div>

      </div>

      // <div className="container">
      //   <div className="row">
      //     <div className="col-md-4 col-md-offset-4 col-xs-12">
      //       <div className="vertical-center">
      //         <h3><strong>Melden Sie sich an</strong></h3>
      //         <form role="form">
      //           <div className="error" style={{display: 'none'}}></div>
      //           <div className="input-group">
      //             <input name="username" type="text" value={this.state.username} onKeyPress={this.handleKeyPress} onChange={this.handleChange} className="form-control" placeholder="E-Mail Adresse"/>
      //           </div>
      //
      //           <div className="input-group">
      //             <input name="password" type="password" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.handleChange} className="form-control" placeholder="Passwort"/>
      //           </div>
      //
      //           <div><Link to="/passwort-zuruecksetzen">Passwort vergessen?</Link></div>
      //           <button type="button" className="btn btn-success" onClick={this.handleSubmit}>Anmelden</button>
      //         </form>
      //       </div>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

module.exports = {
  Login: withRouter(Login)
};