// @ flow
'use strict';

const React = require('react');
const Relay = require('react-relay');
const { UploadField } = require('@navjobs/upload');
const { confirm, error, Spinner } = require('./feedback');

class PassedGallery extends React.Component {
  constructor(props) {
    super(props);

    this.handleAdd = this.handleAdd.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleConfirmDelete = this.handleConfirmDelete.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleImageSelected = this.handleImageSelected.bind(this);
    this.handleSave = this.handleSave.bind(this);

    this.state = {
      drivingSchoolId: props.drivingSchool.id,
      website: props.drivingSchool.website,
      gallery: props.drivingSchool.passedGallery,
      mode: 'view',
      name: '',
      date: '',
      text: '',
      file: '',
      passedGalleryItemId: null,
      deletePassedGalleryItemId: null
    };
    this.constraints = {
      name: {
        presence: { message: 'Bitte geben Sie einen Name ein.' }
      },
      file: function(value, attributes) {
        if (attributes.mode !== 'add') return null;
        return {
          presence: { message: 'Bitte wählen Sie ein Bild aus.' }
        };
      }
    };
  }

  handleAdd() {
    this.setState({ mode: 'add', name: '', date: '', text: '', file: '' });
  }

  handleCancel() {
    this.setState({ mode: 'view', name: '', date: '', text: '', file: '', passedGalleryItemId: null });
  }

  handleEdit(item) {
    const node = item.item.node;
    this.setState({ mode: 'edit', name: node.name, date: node.date, text: node.text, passedGalleryItemId: node.id });
  }

  handleConfirmDelete(item) {
    this.deletePassedGalleryItem = item.item.node;
    confirm('Löschen bestätigen', 'Wollen Sie den Bestanden-Eintrag wirklich löschen?', this.handleDelete);
  }

  handleDelete() {
    Spinner.start();
    Relay.Store.commitUpdate(
      new DeletePassedGalleryMutation({
        passedGalleryItemId: this.deletePassedGalleryItem.id,
        drivingSchoolId: this.state.drivingSchoolId,
        drivingSchool: null
      }), {
        onSuccess: () => {
          Spinner.success('Bestanden-Eintrag wurde gelöscht');
          this.setState({ mode: 'view' });
          delete this.deletePassedGalleryItem;
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Bestanden-Eintrag konnte nicht gelöscht werden');
        }
      }
    );
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleImageSelected(files) {
    this.setState({ file: files[0].name });
    this.files = files;
    $('#selected-image').html(files[0].name);
  }

  handleSave() {
    const validationResult = validate(this.state, this.constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      error(validationResult, 'Bitte geben Sie die Daten ein.');
      return;
    }

    Spinner.start();
    if (this.state.mode === 'add') {
      Relay.Store.commitUpdate(
        new AddPassedGalleryMutation({
          drivingSchoolId: this.state.drivingSchoolId,
          name: this.state.name,
          date: this.state.date,
          text: this.state.text,
          file: this.files[0],
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Bestanden-Eintrag wurde eingefügt');
            this.setState({ mode: 'view', name: '', date: '', text: '', file: '', passedGalleryItemId: null });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Bestanden-Eintrag konnte nicht eingefügt werden');
          }
        }
      );
    } else if (this.state.mode === 'edit') {
      Relay.Store.commitUpdate(
        new UpdatePassedGalleryMutation({
          passedGalleryItemId: this.state.passedGalleryItemId,
          drivingSchoolId: this.state.drivingSchoolId,
          name: this.state.name,
          date: this.state.date,
          text: this.state.text,
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Bestanden-Eintrag wurde aktualisiert');
            this.setState({ mode: 'view', name: '', date: '', text: '', file: '', passedGalleryItemId: null });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Bestanden-Eintrag konnte nicht aktualisiert werden');
          }
        }
      );
    }
  }

  renderEditBar() {
    if (this.state.mode === 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Neuen Bestanden-Eintrag anlegen" onClick={this.handleAdd}><i className="ti-plus"></i></button>
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    }
  }

  renderPassedItem(item) {
    const node = item.node;
    return (
      <div className="row" key={node.id}>
        <div className="col-sm-2"><img src={`${this.props.drivingSchool.website}/static/images/passed-gallery/${node.filename}`} style={{ width: '120px'}}/></div>
        <div className="col-sm-2">{node.name}</div>
        <div className="col-sm-2">{node.date}</div>
        <div className="col-sm-4">{node.text}</div>
        <div className="col-sm-2">
          <button type="button" className="btn btn-primary pull-right" title="Bestanden-Eintrag löschen" onClick={(e) => this.handleConfirmDelete({item}, e)}>
            <i className="ti-trash"></i>
          </button>
          <button type="button" className="btn btn-primary pull-right" style={{ marginRight: '10px' }} title="Bestanden-Eintrag bearbeiten" onClick={(e) => this.handleEdit({item}, e)}>
            <i className="ti-pencil"></i>
          </button>
        </div>
      </div>
    );
  }

  renderAdd() {
    return (
      <div className="card">
        <form encType="multipart/form-data">
          <div className="form-group">
            <label>Name</label>
            <input name="name" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.name}/>
          </div>
          <div className="form-group">
            <label>Datum</label>
            <input name="date" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.date}/>
          </div>
          <div className="form-group">
            <label>Text</label>
            <input name="text" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.text}/>
          </div>
          <div className="form-group">
            <label>Bild</label>
            <UploadField
              onFiles={ files => this.handleImageSelected(files) }
              containerProps={{
                className: 'resume_import'
              }}
              uploadProps={{
                accept: '.jpg,.jpeg,.png'
              }}>
              <div>
                Bild hochladen: <span id="selected-image">Kein Bild ausgewählt</span>
              </div>
            </UploadField>
          </div>
        </form>
      </div>
    );
  }

  renderEdit() {
    return (
      <div className="card">
        <form>
          <div className="form-group">
            <label>Name</label>
            <input name="name" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.name}/>
          </div>
          <div className="form-group">
            <label>Datum</label>
            <input name="date" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.date}/>
          </div>
          <div className="form-group">
            <label>Text</label>
            <input name="text" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.text}/>
          </div>
        </form>
      </div>
    );
  }

  renderView() {
    const passedGalleryEdges = this.props.drivingSchool.passedGallery.edges;
    return (
      <div className="card">
        { passedGalleryEdges.map((passedItem) => this.renderPassedItem(passedItem)) }
      </div>
    );
  }

  render() {
    let content;
    switch (this.state.mode) {
      case 'add':
        content = this.renderAdd();
        break;

      case 'edit':
        content = this.renderEdit();
        break;

      default:
        content = this.renderView();
    }

    return (
      <div>
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class AddPassedGalleryMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { addPassedGallery }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      name: this.props.name,
      date: this.props.date,
      text: this.props.text
    };
  }

  getFiles() {
    return {
      file: this.props.file,
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddPassedGalleryMutationPayload {
        drivingschool {
          passedGallery
        }
        passedGalleryEdge
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'passedGallery',
      edgeName: 'passedGalleryEdge',
      rangeBehaviors: {
        '': 'prepend'
      }
    }];
  }
}

class UpdatePassedGalleryMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updatePassedGallery }`;
  }

  getVariables() {
    return {
      passedGalleryItemId: this.props.passedGalleryItemId,
      drivingSchoolId: this.props.drivingSchoolId,
      name: this.props.name,
      date: this.props.date,
      text: this.props.text
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
          fragment on DrivingSchool {
              id
          }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
        fragment on UpdatePassedGalleryMutationPayload {
            drivingschool {
                id
                passedGallery
            }
        }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class DeletePassedGalleryMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { deletePassedGallery }`;
  }

  getVariables() {
    return {
      passedGalleryItemId: this.props.passedGalleryItemId,
      drivingSchoolId: this.props.drivingSchoolId
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
          fragment on DrivingSchool {
              id
          }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on DeletePassedGalleryMutationPayload {
        drivingschool
        deletedPassedGalleryItemId
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'passedGallery',
      deletedIDFieldName: 'deletedPassedGalleryItemId'
    }];
  }
}

const PassedGalleryPage = Relay.createContainer(
  PassedGallery,
  {
    fragments: {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          website  
          passedGallery(first: 1000) {
            edges {
              cursor
              node {
                id
                name
                date
                text
                filename
              }
            }
          }
          ${AddPassedGalleryMutation.getFragment('drivingSchool')}
          ${UpdatePassedGalleryMutation.getFragment('drivingSchool')}
          ${DeletePassedGalleryMutation.getFragment('drivingSchool')}
        }`
    }
  }
);

const PassedGalleryQuery = {
  drivingSchool: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  PassedGallery: PassedGalleryPage,
  PassedGalleryQuery
};