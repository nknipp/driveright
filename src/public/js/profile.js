// @flow
'use strict';

const React = require('react');
const Relay = require('react-relay');
const { Spinner } = require('./feedback');

type Props = {
  profile: {
    user: {
      id: string,
      username: string,
      firstname: string,
      lastname: string
    }
  }
};

type State = {
  id: string,
  username: string,
  firstname: string,
  lastname: string,
  password1: string,
  password2: string,
  mode: 'view' | 'editprofile' | 'changepassword'
};

class Profile extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    (this: any).handleEditProfile = this.handleEditProfile.bind(this);
    (this: any).handleChangePassword = this.handleChangePassword.bind(this);
    (this: any).handleSave = this.handleSave.bind(this);
    (this: any).handleCancel = this.handleCancel.bind(this);
    (this: any).handleChange = this.handleChange.bind(this);

    this.state = {
      id: props.profile.user.id,
      username: props.profile.user.username || '',
      firstname: props.profile.user.firstname || '',
      lastname: props.profile.user.lastname || '',
      password1: '',
      password2: '',
      mode: 'view'
    };
  }

  handleEditProfile() {
    this.setState({ mode: 'editprofile' });
  }

  handleChangePassword() {
    this.setState({ mode: 'changepassword' });
  }

  handleSave() {
    if (this.state.mode === 'editprofile') {
      Spinner.start();
      Relay.Store.commitUpdate(
        new UpdateProfileMutation({
          id: this.state.id,
          // username: this.state.username,
          firstname: this.state.firstname,
          lastname: this.state.lastname
        }), {
          onSuccess: () => {
            Spinner.success('Profil wurde aktualisiert');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Profil konnte nicht aktualisiert werden.');
          }
        }
      );
    } else if (this.state.mode === 'changepassword') {
      Spinner.start();
      Relay.Store.commitUpdate(
        new UpdatePasswordMutation({
          id: this.state.id,
          password1: this.state.password1,
          password2: this.state.password2
        }), {
          onSuccess: () => {
            Spinner.success('Passwort wurde geändert');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Passwort konnte nicht geändert werden.');
          }
        }
      );
    }
  }

  handleCancel() {
    this.setState({ mode: 'view' });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  renderEditBar() {
    if (this.state.mode !== 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" onClick={this.handleEditProfile}><i className="ti-pencil"></i></button>
        </div>
      );
    }
  }

  renderView() {
    return (
      <div className="card">
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>E-Mail Adresse</label></div>
          <div className="col-sm-12 col-md-10">{this.props.profile.user.username}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Vorname</label></div>
          <div className="col-sm-12 col-md-10">{this.props.profile.user.firstname}</div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-2"><label>Nachname</label></div>
          <div className="col-sm-12 col-md-10">{this.props.profile.user.lastname}</div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <button type="button" className="btn btn-primary" onClick={this.handleChangePassword}>Password ändern</button>
          </div>
        </div>
      </div>
    );
  }

  renderEditProfile() {
    return (
      <div className="card">
        <form>
          <div className="form-group">
            <label>E-Mail Adresse</label>
            <input name="username" type="text" readOnly className="form-control border-input" onChange={this.handleChange} value={this.state.username}/>
          </div>
          <div className="form-group">
            <label>Vorname</label>
            <input name="firstname" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.firstname}/>
          </div>
          <div className="form-group">
            <label>Nachname</label>
            <input name="lastname" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.lastname}/>
          </div>
        </form>
      </div>
    );
  }

  renderChangePassword() {
    return (
      <div className="card">
        <form>
          <div className="form-group">
            <label>Passwort</label>
            <input name="password1" type="password" className="form-control border-input" onChange={this.handleChange} value={this.state.password1}/>
          </div>
          <div className="form-group">
            <label>Passwort bestätigen</label>
            <input name="password2" type="password" className="form-control border-input" onChange={this.handleChange} value={this.state.password2}/>
          </div>
        </form>
      </div>
    );
  }

  render() {
    let content;
    if (this.state.mode === 'editprofile') {
      content = this.renderEditProfile();
    } else if (this.state.mode === 'changepassword') {
      content = this.renderChangePassword();
    } else {
      content = this.renderView();
    }

    return (
      <div>
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class UpdateProfileMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updateProfile }`;
  }

  getVariables() {
    return {
      id: this.props.id,
      username: this.props.username,
      firstname: this.props.firstname,
      lastname: this.props.lastname
    };
  }

  static get fragments() {
    return {
      profile: () => Relay.QL`
        fragment on Viewer {
          user {
            id
            username
            firstname
            lastname
          }
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateProfileMutationPayload {
        changedProfile
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        changedProfile: this.props.id
      }
    }];
  }
}

class UpdatePasswordMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updatePassword }`;
  }

  getVariables() {
    return {
      id: this.props.id,
      password1: this.props.password1,
      password2: this.props.password2
    };
  }

  static get fragments() {
    return {
      password: () => Relay.QL`
        fragment on Viewer {
          user {
            id
          }
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdatePasswordMutationPayload {
        changedPassword
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        changedPassword: this.props.id
      }
    }];
  }
}

const ProfilePage = Relay.createContainer(
  Profile,
  {
    fragments: {
      profile: () => Relay.QL`
        fragment on Viewer {
          user {
            id
            username
            firstname
            lastname
          }
          ${UpdateProfileMutation.getFragment('profile')}
          ${UpdatePasswordMutation.getFragment('password')}
        }`
    }
  }
);

const ProfileQuery = {
  profile: () => Relay.QL`
    query {
      viewer
    }`
};

module.exports = {
  Profile: ProfilePage,
  ProfileQuery
};