// @ flow
'use strict';

const React = require('react');

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      username: ''
    };
    this.constraints = {
      username: {
        presence: { message: 'Bitte geben Sie Ihre E-Mail Adresse ein.' }
      }
    };
  }

  handleSubmit() {
    const validationResult = validate(this.state, this.constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      $('div.container div.error').html(validationResult).show();
      return;
    }

    $.post('/resetpassword', this.state).done((data) => {
      $('#resetFormContainer').html(data.message);
    }).fail((errors) => {
      $('div.container div.error').html(errors.responseText).show();
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-xs-12 col-sm-5 col-md-5 col-lg-4">
            <div id="resetFormContainer">
              <form className="sky-form boxed">
                <header><i className="fa fa-users"></i> Passwort vergessen</header>

                <div className="error text-center" style={{ display: 'none', margin: '20px 0 -20px', color: 'red' }}></div>

                <fieldset className="m-0">
                  <label className="mt-20">E-Mail</label>
                  <label className="input">
                    <i className="ico-append fa fa-envelope"></i>
                    <input name="username" type="email" value={this.state.username} onChange={this.handleChange} className="form-control" placeholder="E-Mail Adresse"/>
                  </label>
                </fieldset>

                <footer className="clearfix">
                  <button type="button" className="btn btn-primary float-right" onClick={this.handleSubmit}><i className="fa fa-check"></i> Passwort zurücksetzen</button>
                </footer>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

module.exports = {
  ResetPassword
};