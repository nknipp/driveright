// @flow
'use strict';

const React = require('react');
const IndexRoute = require('react-router/lib/IndexRoute');
const Route = require('react-router/lib/Route');

const { App, AppIndex } = require('./app');
const { Dashboard, DashboardIndex, DashboardQuery } = require('./dashboard');
const { DrivingSchools, DrivingSchoolsQuery } = require('./drivingschools');
const { AddDrivingSchool, AddDrivingSchoolQuery, DrivingSchool, DrivingSchoolQuery } = require('./drivingschool');
const { Profile, ProfileQuery } = require('./profile');
const { Login } = require('./login');
const { ResetPassword } = require('./resetPassword');
const { SetPassword } = require('./setPassword');
const { Courses, CoursesQuery } = require('./courses');
const { PassedGallery, PassedGalleryQuery } = require('./passed-gallery');
const { Stores, StoreQuery } = require('./store');
const { Team, TeamQuery } = require('./team');
const { Car, CarQuery } = require('./cars');
const { CampaignParticipants, CampaignParticipantsQuery } = require('./campaignParticipants');

const Routes =
  <Route path='/' component={App}>
    <IndexRoute component={AppIndex}/>
    <Route path='/dashboard' component={Dashboard} queries={DashboardQuery}>
      <IndexRoute component={DashboardIndex}/>
      <Route path='/fahrschulen'>
        <IndexRoute component={DrivingSchools} queries={DrivingSchoolsQuery}/>
        <Route path='add' component={AddDrivingSchool} queries={AddDrivingSchoolQuery}/>
        <Route path=':id' component={DrivingSchool} queries={DrivingSchoolQuery}/>
        <Route path=":id/bestanden" component={PassedGallery} queries={PassedGalleryQuery}/>
        <Route path=":id/kurse" component={Courses} queries={CoursesQuery}/>
        <Route path=":id/filialen" component={Stores} queries={StoreQuery}/>
        <Route path=":id/team" component={Team} queries={TeamQuery}/>
        <Route path=":id/fahrzeuge" component={Car} queries={CarQuery}/>
        <Route path=":id/teilnehmer" component={CampaignParticipants} queries={CampaignParticipantsQuery}/>
      </Route>
      <Route path="/profil" component={Profile} queries={ProfileQuery}/>
    </Route>
    <Route path='/anmelden' component={Login}/>
    <Route path='/passwort-zuruecksetzen' component={ResetPassword}/>
    <Route path='/passwort-setzen/:token' component={SetPassword}/>
  </Route>;

module.exports = Routes;