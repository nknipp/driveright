// @ flow
'use strict';

const React = require('react');
const { withRouter } = require('react-router');

class SetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      token: props.params.token,
      password1: '',
      password2: ''
    };
  }

  handleSubmit() {
    const body = this.state;
    $.post('/resetpassword', body).done(data => {
      if (data.success) {
        $('#resetFormContainer').hide();
        $('#passwordResetContainer').show();
      } else {
        $('div.container div.error').html(data.msg).show();
      }
    }).fail((jqXHR) => {
      $('div.container div.error').html(jqXHR.responseText).show();
    }, 'json');

  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-xs-12 col-sm-5 col-md-5 col-lg-4">
            <div id="resetFormContainer">
              <form className="sky-form boxed">
                <header><i className="fa fa-users"></i> Passwort zurücksetzen</header>

                <div className="error text-center" style={{ display: 'none', margin: '20px 0 -20px', color: 'red' }}></div>

                <fieldset className="m-0">
                  <label className="mt-20">Password</label>
                  <label className="input">
                    <i className="ico-append fa fa-lock"></i>
                    <input name="password1" type="password" value={this.state.password1} onChange={this.handleChange} className="form-control" placeholder="Passwort"/>
                  </label>

                  <label className="mt-20">Password</label>
                  <label className="input">
                    <i className="ico-append fa fa-lock"></i>
                    <input name="password2" type="password" value={this.state.password2} onChange={this.handleChange} className="form-control" placeholder="Passwort wiederholen"/>
                  </label>
                </fieldset>

                <footer className="clearfix">
                  <button type="button" className="btn btn-primary float-right" onClick={this.handleSubmit}><i className="fa fa-check"></i> Passwort ändern</button>
                </footer>
              </form>
            </div>
            <div id="passwordResetContainer" style={{ display: 'none' }}>
              Ihr Passwort würde zurückgesetzt.<br/>
              Melden Sie sich mit Ihren Zugangsdaten <a style={{ color: 'black' }} href="/anmelden#/anmelden">hier</a> an.
            </div>
          </div>
        </div>
      </div>
    );
  }
}

module.exports = {
  SetPassword: withRouter(SetPassword)
};