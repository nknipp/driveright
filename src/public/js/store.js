// @ flow
'use strict';

const React = require('react');
const Relay = require('react-relay');

const { confirm, error, Spinner } = require('./feedback');

function timeSelector(name, handle, stateValue) {
  return (
    <select name={name} className="form-control border-input" style={{ display: 'inline-block', maxWidth: '80%', marginLeft: '10px' }} onChange={handle} value={stateValue}>
      <option></option>
      <option>00:00</option>
      <option>00:15</option>
      <option>00:30</option>
      <option>00:45</option>
      <option>01:00</option>
      <option>01:15</option>
      <option>01:30</option>
      <option>01:45</option>
      <option>02:00</option>
      <option>02:15</option>
      <option>02:30</option>
      <option>02:45</option>
      <option>03:00</option>
      <option>03:15</option>
      <option>03:30</option>
      <option>03:45</option>
      <option>04:00</option>
      <option>04:15</option>
      <option>04:30</option>
      <option>04:45</option>
      <option>05:00</option>
      <option>05:15</option>
      <option>05:30</option>
      <option>05:45</option>
      <option>06:00</option>
      <option>06:15</option>
      <option>06:30</option>
      <option>06:45</option>
      <option>07:00</option>
      <option>07:15</option>
      <option>07:30</option>
      <option>07:45</option>
      <option>08:00</option>
      <option>08:15</option>
      <option>08:30</option>
      <option>08:45</option>
      <option>09:00</option>
      <option>09:15</option>
      <option>09:30</option>
      <option>09:45</option>
      <option>10:00</option>
      <option>10:15</option>
      <option>10:30</option>
      <option>10:45</option>
      <option>11:00</option>
      <option>11:15</option>
      <option>11:30</option>
      <option>11:45</option>
      <option>12:00</option>
      <option>12:15</option>
      <option>12:30</option>
      <option>12:45</option>
      <option>13:00</option>
      <option>13:15</option>
      <option>13:30</option>
      <option>13:45</option>
      <option>14:00</option>
      <option>14:15</option>
      <option>14:30</option>
      <option>14:45</option>
      <option>15:00</option>
      <option>15:15</option>
      <option>15:30</option>
      <option>15:45</option>
      <option>16:00</option>
      <option>16:15</option>
      <option>16:30</option>
      <option>16:45</option>
      <option>17:00</option>
      <option>17:15</option>
      <option>17:30</option>
      <option>17:45</option>
      <option>18:00</option>
      <option>18:15</option>
      <option>18:30</option>
      <option>18:45</option>
      <option>19:00</option>
      <option>19:15</option>
      <option>19:30</option>
      <option>19:45</option>
      <option>20:00</option>
      <option>20:15</option>
      <option>20:30</option>
      <option>20:45</option>
      <option>21:00</option>
      <option>21:15</option>
      <option>21:30</option>
      <option>21:45</option>
      <option>22:00</option>
      <option>22:15</option>
      <option>22:30</option>
      <option>22:45</option>
      <option>23:00</option>
      <option>23:15</option>
      <option>23:30</option>
      <option>23:45</option>
    </select>
  );
}

class Store extends React.Component {
  constructor(props) {
    super(props);

    this.handleAdd = this.handleAdd.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleConfirmDelete = this.handleConfirmDelete.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleSave = this.handleSave.bind(this);

    this.state = {
      storeId: null,
      drivingSchoolId: props.stores.id,
      mode: 'view',
      street: '',
      postcode: '',
      city: '',
      phone: '',
      openingHoursMondayFrom1: '',
      openingHoursMondayTo1: '',
      openingHoursMondayFrom2: '',
      openingHoursMondayTo2: '',
      openingHoursTuesdayFrom1: '',
      openingHoursTuesdayTo1: '',
      openingHoursTuesdayFrom2: '',
      openingHoursTuesdayTo2: '',
      openingHoursWednesdayFrom1: '',
      openingHoursWednesdayTo1: '',
      openingHoursWednesdayFrom2: '',
      openingHoursWednesdayTo2: '',
      openingHoursThursdayFrom1: '',
      openingHoursThursdayTo1: '',
      openingHoursThursdayFrom2: '',
      openingHoursThursdayTo2: '',
      openingHoursFridayFrom1: '',
      openingHoursFridayTo1: '',
      openingHoursFridayFrom2: '',
      openingHoursFridayTo2: '',
      openingHoursSaturdayFrom1: '',
      openingHoursSaturdayTo1: '',
      openingHoursSaturdayFrom2: '',
      openingHoursSaturdayTo2: '',
      theoreticalLessonsMondayFrom1: '',
      theoreticalLessonsMondayTo1: '',
      theoreticalLessonsMondayFrom2: '',
      theoreticalLessonsMondayTo2: '',
      theoreticalLessonsTuesdayFrom1: '',
      theoreticalLessonsTuesdayTo1: '',
      theoreticalLessonsTuesdayFrom2: '',
      theoreticalLessonsTuesdayTo2: '',
      theoreticalLessonsWednesdayFrom1: '',
      theoreticalLessonsWednesdayTo1: '',
      theoreticalLessonsWednesdayFrom2: '',
      theoreticalLessonsWednesdayTo2: '',
      theoreticalLessonsThursdayFrom1: '',
      theoreticalLessonsThursdayTo1: '',
      theoreticalLessonsThursdayFrom2: '',
      theoreticalLessonsThursdayTo2: '',
      theoreticalLessonsFridayFrom1: '',
      theoreticalLessonsFridayTo1: '',
      theoreticalLessonsFridayFrom2: '',
      theoreticalLessonsFridayTo2: '',
      theoreticalLessonsSaturdayFrom1: '',
      theoreticalLessonsSaturdayTo1: '',
      theoreticalLessonsSaturdayFrom2: '',
      theoreticalLessonsSaturdayTo2: ''
    };
    this.constraints = {
      street: {
        presence: { message: 'Bitte geben Sie eine Strasse ein.' }
      },
      postcode: {
        presence: { message: 'Bitte geben Sie eine Postleitzahl ein.' }
      },
      city: {
        presence: { message: 'Bitte geben Sie einen Ort ein.' }
      },
      phone: {
        presence: { message: 'Bitte geben Sie eine Telefonnummer ein.' }
      }
    };
  }

  handleAdd() {
    this.setState({
      mode: 'add',
      street: '',
      postcode: '',
      city: '',
      phone: '',
      openingHoursMondayFrom1: '',
      openingHoursMondayTo1: '',
      openingHoursMondayFrom2: '',
      openingHoursMondayTo2: '',
      openingHoursTuesdayFrom1: '',
      openingHoursTuesdayTo1: '',
      openingHoursTuesdayFrom2: '',
      openingHoursTuesdayTo2: '',
      openingHoursWednesdayFrom1: '',
      openingHoursWednesdayTo1: '',
      openingHoursWednesdayFrom2: '',
      openingHoursWednesdayTo2: '',
      openingHoursThursdayFrom1: '',
      openingHoursThursdayTo1: '',
      openingHoursThursdayFrom2: '',
      openingHoursThursdayTo2: '',
      openingHoursFridayFrom1: '',
      openingHoursFridayTo1: '',
      openingHoursFridayFrom2: '',
      openingHoursFridayTo2: '',
      openingHoursSaturdayFrom1: '',
      openingHoursSaturdayTo1: '',
      openingHoursSaturdayFrom2: '',
      openingHoursSaturdayTo2: '',
      theoreticalLessonsMondayFrom1: '',
      theoreticalLessonsMondayTo1: '',
      theoreticalLessonsMondayFrom2: '',
      theoreticalLessonsMondayTo2: '',
      theoreticalLessonsTuesdayFrom1: '',
      theoreticalLessonsTuesdayTo1: '',
      theoreticalLessonsTuesdayFrom2: '',
      theoreticalLessonsTuesdayTo2: '',
      theoreticalLessonsWednesdayFrom1: '',
      theoreticalLessonsWednesdayTo1: '',
      theoreticalLessonsWednesdayFrom2: '',
      theoreticalLessonsWednesdayTo2: '',
      theoreticalLessonsThursdayFrom1: '',
      theoreticalLessonsThursdayTo1: '',
      theoreticalLessonsThursdayFrom2: '',
      theoreticalLessonsThursdayTo2: '',
      theoreticalLessonsFridayFrom1: '',
      theoreticalLessonsFridayTo1: '',
      theoreticalLessonsFridayFrom2: '',
      theoreticalLessonsFridayTo2: '',
      theoreticalLessonsSaturdayFrom1: '',
      theoreticalLessonsSaturdayTo1: '',
      theoreticalLessonsSaturdayFrom2: '',
      theoreticalLessonsSaturdayTo2: ''
    });
  }

  handleCancel() {
    this.setState({
      mode: 'view',
      street: '',
      postcode: '',
      city: '',
      phone: '',
      openingHoursMondayFrom1: '',
      openingHoursMondayTo1: '',
      openingHoursMondayFrom2: '',
      openingHoursMondayTo2: '',
      openingHoursTuesdayFrom1: '',
      openingHoursTuesdayTo1: '',
      openingHoursTuesdayFrom2: '',
      openingHoursTuesdayTo2: '',
      openingHoursWednesdayFrom1: '',
      openingHoursWednesdayTo1: '',
      openingHoursWednesdayFrom2: '',
      openingHoursWednesdayTo2: '',
      openingHoursThursdayFrom1: '',
      openingHoursThursdayTo1: '',
      openingHoursThursdayFrom2: '',
      openingHoursThursdayTo2: '',
      openingHoursFridayFrom1: '',
      openingHoursFridayTo1: '',
      openingHoursFridayFrom2: '',
      openingHoursFridayTo2: '',
      openingHoursSaturdayFrom1: '',
      openingHoursSaturdayTo1: '',
      openingHoursSaturdayFrom2: '',
      openingHoursSaturdayTo2: '',
      theoreticalLessonsMondayFrom1: '',
      theoreticalLessonsMondayTo1: '',
      theoreticalLessonsMondayFrom2: '',
      theoreticalLessonsMondayTo2: '',
      theoreticalLessonsTuesdayFrom1: '',
      theoreticalLessonsTuesdayTo1: '',
      theoreticalLessonsTuesdayFrom2: '',
      theoreticalLessonsTuesdayTo2: '',
      theoreticalLessonsWednesdayFrom1: '',
      theoreticalLessonsWednesdayTo1: '',
      theoreticalLessonsWednesdayFrom2: '',
      theoreticalLessonsWednesdayTo2: '',
      theoreticalLessonsThursdayFrom1: '',
      theoreticalLessonsThursdayTo1: '',
      theoreticalLessonsThursdayFrom2: '',
      theoreticalLessonsThursdayTo2: '',
      theoreticalLessonsFridayFrom1: '',
      theoreticalLessonsFridayTo1: '',
      theoreticalLessonsFridayFrom2: '',
      theoreticalLessonsFridayTo2: '',
      theoreticalLessonsSaturdayFrom1: '',
      theoreticalLessonsSaturdayTo1: '',
      theoreticalLessonsSaturdayFrom2: '',
      theoreticalLessonsSaturdayTo2: '',
      storeId: null
    });
  }

  handleEdit(item) {
    const node = item.item.node;
    let openingHours = node.openingHours;
    if (!openingHours) {
      openingHours = {};
      openingHours.monday = { from1: '', to1: '', from2: '', to2: ''};
      openingHours.tuesday = { from1: '', to1: '', from2: '', to2: ''};
      openingHours.wednesday = { from1: '', to1: '', from2: '', to2: ''};
      openingHours.thursday = { from1: '', to1: '', from2: '', to2: ''};
      openingHours.friday = { from1: '', to1: '', from2: '', to2: ''};
      openingHours.saturday = { from1: '', to1: '', from2: '', to2: ''};
    }
    let theoreticalLessons = node.theoreticalLessons;
    if (!theoreticalLessons) {
      theoreticalLessons = {};
      theoreticalLessons.monday = { from1: '', to1: '', from2: '', to2: ''};
      theoreticalLessons.tuesday = { from1: '', to1: '', from2: '', to2: ''};
      theoreticalLessons.wednesday = { from1: '', to1: '', from2: '', to2: ''};
      theoreticalLessons.thursday = { from1: '', to1: '', from2: '', to2: ''};
      theoreticalLessons.friday = { from1: '', to1: '', from2: '', to2: ''};
      theoreticalLessons.saturday = { from1: '', to1: '', from2: '', to2: ''};
    }
    this.setState({
      mode: 'edit',
      street: node.address.street,
      postcode: node.address.postcode,
      city: node.address.city,
      phone: node.phone,
      openingHoursMondayFrom1: openingHours.monday.from1,
      openingHoursMondayTo1: openingHours.monday.to1,
      openingHoursMondayFrom2: openingHours.monday.from2,
      openingHoursMondayTo2: openingHours.monday.to2,
      openingHoursTuesdayFrom1: openingHours.tuesday.from1,
      openingHoursTuesdayTo1: openingHours.tuesday.to1,
      openingHoursTuesdayFrom2: openingHours.tuesday.from2,
      openingHoursTuesdayTo2: openingHours.tuesday.to2,
      openingHoursWednesdayFrom1: openingHours.wednesday.from1,
      openingHoursWednesdayTo1: openingHours.wednesday.to1,
      openingHoursWednesdayFrom2: openingHours.wednesday.from2,
      openingHoursWednesdayTo2: openingHours.wednesday.to2,
      openingHoursThursdayFrom1: openingHours.thursday.from1,
      openingHoursThursdayTo1: openingHours.thursday.to1,
      openingHoursThursdayFrom2: openingHours.thursday.from2,
      openingHoursThursdayTo2: openingHours.thursday.to2,
      openingHoursFridayFrom1: openingHours.friday.from1,
      openingHoursFridayTo1: openingHours.friday.to1,
      openingHoursFridayFrom2: openingHours.friday.from2,
      openingHoursFridayTo2: openingHours.friday.to2,
      openingHoursSaturdayFrom1: openingHours.saturday.from1,
      openingHoursSaturdayTo1: openingHours.saturday.to1,
      openingHoursSaturdayFrom2: openingHours.saturday.from2,
      openingHoursSaturdayTo2: openingHours.saturday.to2,
      theoreticalLessonsMondayFrom1: theoreticalLessons.monday.from1,
      theoreticalLessonsMondayTo1: theoreticalLessons.monday.to1,
      theoreticalLessonsMondayFrom2: theoreticalLessons.monday.from2,
      theoreticalLessonsMondayTo2: theoreticalLessons.monday.to2,
      theoreticalLessonsTuesdayFrom1: theoreticalLessons.tuesday.from1,
      theoreticalLessonsTuesdayTo1: theoreticalLessons.tuesday.to1,
      theoreticalLessonsTuesdayFrom2: theoreticalLessons.tuesday.from2,
      theoreticalLessonsTuesdayTo2: theoreticalLessons.tuesday.to2,
      theoreticalLessonsWednesdayFrom1: theoreticalLessons.wednesday.from1,
      theoreticalLessonsWednesdayTo1: theoreticalLessons.wednesday.to1,
      theoreticalLessonsWednesdayFrom2: theoreticalLessons.wednesday.from2,
      theoreticalLessonsWednesdayTo2: theoreticalLessons.wednesday.to2,
      theoreticalLessonsThursdayFrom1: theoreticalLessons.thursday.from1,
      theoreticalLessonsThursdayTo1: theoreticalLessons.thursday.to1,
      theoreticalLessonsThursdayFrom2: theoreticalLessons.thursday.from2,
      theoreticalLessonsThursdayTo2: theoreticalLessons.thursday.to2,
      theoreticalLessonsFridayFrom1: theoreticalLessons.friday.from1,
      theoreticalLessonsFridayTo1: theoreticalLessons.friday.to1,
      theoreticalLessonsFridayFrom2: theoreticalLessons.friday.from2,
      theoreticalLessonsFridayTo2: theoreticalLessons.friday.to2,
      theoreticalLessonsSaturdayFrom1: theoreticalLessons.saturday.from1,
      theoreticalLessonsSaturdayTo1: theoreticalLessons.saturday.to1,
      theoreticalLessonsSaturdayFrom2: theoreticalLessons.saturday.from2,
      theoreticalLessonsSaturdayTo2: theoreticalLessons.saturday.to2,
      storeId: node.id
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleConfirmDelete(item) {
    this.deleteStore = item.item.node;
    confirm('Löschen bestätigen', 'Wollen Sie die Filiale wirklich löschen?', this.handleDelete);
  }

  handleDelete() {
    Spinner.start();
    Relay.Store.commitUpdate(
      new DeleteStoreMutation({
        storeId: this.deleteStore.id,
        drivingSchoolId: this.state.drivingSchoolId,
        stores: null
      }), {
        onSuccess: () => {
          Spinner.success('Filiale wurde gelöscht');
          this.setState({ mode: 'view' });
          delete this.deletePassedGalleryItem;
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Filiale konnte nicht gelöscht werden');
        }
      }
    );
  }

  handleSave() {
    const validationResult = validate(this.state, this.constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      error(validationResult, 'Kann Filiale nicht einfügen');
      return;
    }

    if (this.state.mode === 'add') {
      Spinner.start();
      Relay.Store.commitUpdate(
        new AddStoreMutation({
          drivingSchoolId: this.state.drivingSchoolId,
          street: this.state.street,
          postcode: this.state.postcode,
          city: this.state.city,
          phone: this.state.phone,
          openingHoursMondayFrom1: this.state.openingHoursMondayFrom1,
          openingHoursMondayTo1: this.state.openingHoursMondayTo1,
          openingHoursMondayFrom2: this.state.openingHoursMondayFrom2,
          openingHoursMondayTo2: this.state.openingHoursMondayTo2,
          openingHoursTuesdayFrom1: this.state.openingHoursTuesdayFrom1,
          openingHoursTuesdayTo1: this.state.openingHoursTuesdayTo1,
          openingHoursTuesdayFrom2: this.state.openingHoursTuesdayFrom2,
          openingHoursTuesdayTo2: this.state.openingHoursTuesdayTo2,
          openingHoursWednesdayFrom1: this.state.openingHoursWednesdayFrom1,
          openingHoursWednesdayTo1: this.state.openingHoursWednesdayTo1,
          openingHoursWednesdayFrom2: this.state.openingHoursWednesdayFrom2,
          openingHoursWednesdayTo2: this.state.openingHoursWednesdayTo2,
          openingHoursThursdayFrom1: this.state.openingHoursThursdayFrom1,
          openingHoursThursdayTo1: this.state.openingHoursThursdayTo1,
          openingHoursThursdayFrom2: this.state.openingHoursThursdayFrom2,
          openingHoursThursdayTo2: this.state.openingHoursThursdayTo2,
          openingHoursFridayFrom1: this.state.openingHoursFridayFrom1,
          openingHoursFridayTo1: this.state.openingHoursFridayTo1,
          openingHoursFridayFrom2: this.state.openingHoursFridayFrom2,
          openingHoursFridayTo2: this.state.openingHoursFridayTo2,
          openingHoursSaturdayFrom1: this.state.openingHoursSaturdayFrom1,
          openingHoursSaturdayTo1: this.state.openingHoursSaturdayTo1,
          openingHoursSaturdayFrom2: this.state.openingHoursSaturdayFrom2,
          openingHoursSaturdayTo2: this.state.openingHoursSaturdayTo2,
          theoreticalLessonsMondayFrom1: this.state.theoreticalLessonsMondayFrom1,
          theoreticalLessonsMondayTo1: this.state.theoreticalLessonsMondayTo1,
          theoreticalLessonsMondayFrom2: this.state.theoreticalLessonsMondayFrom2,
          theoreticalLessonsMondayTo2: this.state.theoreticalLessonsMondayTo2,
          theoreticalLessonsTuesdayFrom1: this.state.theoreticalLessonsTuesdayFrom1,
          theoreticalLessonsTuesdayTo1: this.state.theoreticalLessonsTuesdayTo1,
          theoreticalLessonsTuesdayFrom2: this.state.theoreticalLessonsTuesdayFrom2,
          theoreticalLessonsTuesdayTo2: this.state.theoreticalLessonsTuesdayTo2,
          theoreticalLessonsWednesdayFrom1: this.state.theoreticalLessonsWednesdayFrom1,
          theoreticalLessonsWednesdayTo1: this.state.theoreticalLessonsWednesdayTo1,
          theoreticalLessonsWednesdayFrom2: this.state.theoreticalLessonsWednesdayFrom2,
          theoreticalLessonsWednesdayTo2: this.state.theoreticalLessonsWednesdayTo2,
          theoreticalLessonsThursdayFrom1: this.state.theoreticalLessonsThursdayFrom1,
          theoreticalLessonsThursdayTo1: this.state.theoreticalLessonsThursdayTo1,
          theoreticalLessonsThursdayFrom2: this.state.theoreticalLessonsThursdayFrom2,
          theoreticalLessonsThursdayTo2: this.state.theoreticalLessonsThursdayTo2,
          theoreticalLessonsFridayFrom1: this.state.theoreticalLessonsFridayFrom1,
          theoreticalLessonsFridayTo1: this.state.theoreticalLessonsFridayTo1,
          theoreticalLessonsFridayFrom2: this.state.theoreticalLessonsFridayFrom2,
          theoreticalLessonsFridayTo2: this.state.theoreticalLessonsFridayTo2,
          theoreticalLessonsSaturdayFrom1: this.state.theoreticalLessonsSaturdayFrom1,
          theoreticalLessonsSaturdayTo1: this.state.theoreticalLessonsSaturdayTo1,
          theoreticalLessonsSaturdayFrom2: this.state.theoreticalLessonsSaturdayFrom2,
          theoreticalLessonsSaturdayTo2: this.state.theoreticalLessonsSaturdayTo2,
          stores: null
        }), {
          onSuccess: () => {
            Spinner.success('Filiale wurde eingefügt');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Fehler beim Speichern');
            Spinner.failure(error, 'Filiale konnte nicht eingefügt werden');
          }
        }
      );
    } else if (this.state.mode === 'edit') {
      Spinner.start();
      Relay.Store.commitUpdate(
        new UpdateStoreMutation({
          storeId: this.state.storeId,
          drivingSchoolId: this.state.drivingSchoolId,
          street: this.state.street,
          postcode: this.state.postcode,
          city: this.state.city,
          phone: this.state.phone,
          openingHoursMondayFrom1: this.state.openingHoursMondayFrom1,
          openingHoursMondayTo1: this.state.openingHoursMondayTo1,
          openingHoursMondayFrom2: this.state.openingHoursMondayFrom2,
          openingHoursMondayTo2: this.state.openingHoursMondayTo2,
          openingHoursTuesdayFrom1: this.state.openingHoursTuesdayFrom1,
          openingHoursTuesdayTo1: this.state.openingHoursTuesdayTo1,
          openingHoursTuesdayFrom2: this.state.openingHoursTuesdayFrom2,
          openingHoursTuesdayTo2: this.state.openingHoursTuesdayTo2,
          openingHoursWednesdayFrom1: this.state.openingHoursWednesdayFrom1,
          openingHoursWednesdayTo1: this.state.openingHoursWednesdayTo1,
          openingHoursWednesdayFrom2: this.state.openingHoursWednesdayFrom2,
          openingHoursWednesdayTo2: this.state.openingHoursWednesdayTo2,
          openingHoursThursdayFrom1: this.state.openingHoursThursdayFrom1,
          openingHoursThursdayTo1: this.state.openingHoursThursdayTo1,
          openingHoursThursdayFrom2: this.state.openingHoursThursdayFrom2,
          openingHoursThursdayTo2: this.state.openingHoursThursdayTo2,
          openingHoursFridayFrom1: this.state.openingHoursFridayFrom1,
          openingHoursFridayTo1: this.state.openingHoursFridayTo1,
          openingHoursFridayFrom2: this.state.openingHoursFridayFrom2,
          openingHoursFridayTo2: this.state.openingHoursFridayTo2,
          openingHoursSaturdayFrom1: this.state.openingHoursSaturdayFrom1,
          openingHoursSaturdayTo1: this.state.openingHoursSaturdayTo1,
          openingHoursSaturdayFrom2: this.state.openingHoursSaturdayFrom2,
          openingHoursSaturdayTo2: this.state.openingHoursSaturdayTo2,
          theoreticalLessonsMondayFrom1: this.state.theoreticalLessonsMondayFrom1,
          theoreticalLessonsMondayTo1: this.state.theoreticalLessonsMondayTo1,
          theoreticalLessonsMondayFrom2: this.state.theoreticalLessonsMondayFrom2,
          theoreticalLessonsMondayTo2: this.state.theoreticalLessonsMondayTo2,
          theoreticalLessonsTuesdayFrom1: this.state.theoreticalLessonsTuesdayFrom1,
          theoreticalLessonsTuesdayTo1: this.state.theoreticalLessonsTuesdayTo1,
          theoreticalLessonsTuesdayFrom2: this.state.theoreticalLessonsTuesdayFrom2,
          theoreticalLessonsTuesdayTo2: this.state.theoreticalLessonsTuesdayTo2,
          theoreticalLessonsWednesdayFrom1: this.state.theoreticalLessonsWednesdayFrom1,
          theoreticalLessonsWednesdayTo1: this.state.theoreticalLessonsWednesdayTo1,
          theoreticalLessonsWednesdayFrom2: this.state.theoreticalLessonsWednesdayFrom2,
          theoreticalLessonsWednesdayTo2: this.state.theoreticalLessonsWednesdayTo2,
          theoreticalLessonsThursdayFrom1: this.state.theoreticalLessonsThursdayFrom1,
          theoreticalLessonsThursdayTo1: this.state.theoreticalLessonsThursdayTo1,
          theoreticalLessonsThursdayFrom2: this.state.theoreticalLessonsThursdayFrom2,
          theoreticalLessonsThursdayTo2: this.state.theoreticalLessonsThursdayTo2,
          theoreticalLessonsFridayFrom1: this.state.theoreticalLessonsFridayFrom1,
          theoreticalLessonsFridayTo1: this.state.theoreticalLessonsFridayTo1,
          theoreticalLessonsFridayFrom2: this.state.theoreticalLessonsFridayFrom2,
          theoreticalLessonsFridayTo2: this.state.theoreticalLessonsFridayTo2,
          theoreticalLessonsSaturdayFrom1: this.state.theoreticalLessonsSaturdayFrom1,
          theoreticalLessonsSaturdayTo1: this.state.theoreticalLessonsSaturdayTo1,
          theoreticalLessonsSaturdayFrom2: this.state.theoreticalLessonsSaturdayFrom2,
          theoreticalLessonsSaturdayTo2: this.state.theoreticalLessonsSaturdayTo2,
          stores: null
        }), {
          onSuccess: () => {
            Spinner.success('Filiale wurde aktualisiert');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Filiale konnte nicht aktualisiert werden');
          }
        }
      );
    }  }

  renderEditBar() {
    if (this.state.mode === 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Neue Filiale anlegen" onClick={this.handleAdd}><i className="ti-plus"></i></button>
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    }
  }

  renderStore(item) {
    const node = item.node;
    return (
      <div className="row" key={node.id}>
        <div className="col-sm-3">{node.address.street}</div>
        <div className="col-sm-2">{node.address.postcode}</div>
        <div className="col-sm-3">{node.address.city}</div>
        <div className="col-sm-2">{node.phone}</div>
        <div className="col-sm-2">
          <button type="button" className="btn btn-primary pull-right" title="Filiale löschen" onClick={(e) => this.handleConfirmDelete({item}, e)}>
            <i className="ti-trash"></i>
          </button>
          <button type="button" className="btn btn-primary pull-right" style={{ marginRight: '10px' }} title="Filiale bearbeiten" onClick={(e) => this.handleEdit({item}, e)}>
            <i className="ti-pencil"></i>
          </button>
        </div>
      </div>
    );
  }

  renderAddAndEdit() {
    return (
      <div className="card row">
        <form>
          <div className="form-group">
            <label>Strasse</label>
            <input name="street" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.street}/>
          </div>
          <div className="form-group">
            <label>Postleitzahl</label>
            <input name="postcode" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.postcode}/>
          </div>
          <div className="form-group">
            <label>Ort</label>
            <input name="city" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.city}/>
          </div>
          <div className="form-group">
            <label>Telefon</label>
            <input name="phone" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.phone}/>
          </div>
          <div className="form-group">
            <label><strong>Öffnungszeiten</strong></label><br/>
            <label>Montag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursMondayFrom1', this.handleChange, this.state.openingHoursMondayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursMondayTo1', this.handleChange, this.state.openingHoursMondayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursMondayFrom2', this.handleChange, this.state.openingHoursMondayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursMondayTo2', this.handleChange, this.state.openingHoursMondayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Dienstag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursTuesdayFrom1', this.handleChange, this.state.openingHoursTuesdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursTuesdayTo1', this.handleChange, this.state.openingHoursTuesdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursTuesdayFrom2', this.handleChange, this.state.openingHoursTuesdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursTuesdayTo2', this.handleChange, this.state.openingHoursTuesdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Mittwoch</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursWednesdayFrom1', this.handleChange, this.state.openingHoursWednesdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursWednesdayTo1', this.handleChange, this.state.openingHoursWednesdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursWednesdayFrom2', this.handleChange, this.state.openingHoursWednesdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursWednesdayTo2', this.handleChange, this.state.openingHoursWednesdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Donnerstag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursThursdayFrom1', this.handleChange, this.state.openingHoursThursdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursThursdayTo1', this.handleChange, this.state.openingHoursThursdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursThursdayFrom2', this.handleChange, this.state.openingHoursThursdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursThursdayTo2', this.handleChange, this.state.openingHoursThursdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Freitag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursFridayFrom1', this.handleChange, this.state.openingHoursFridayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursFridayTo1', this.handleChange, this.state.openingHoursFridayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursFridayFrom2', this.handleChange, this.state.openingHoursFridayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursFridayTo2', this.handleChange, this.state.openingHoursFridayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Samstag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursSaturdayFrom1', this.handleChange, this.state.openingHoursSaturdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursSaturdayTo1', this.handleChange, this.state.openingHoursSaturdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('openingHoursSaturdayFrom2', this.handleChange, this.state.openingHoursSaturdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('openingHoursSaturdayTo2', this.handleChange, this.state.openingHoursSaturdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label><strong>Theoretischer Unterricht</strong></label><br/>
            <label>Montag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsMondayFrom1', this.handleChange, this.state.theoreticalLessonsMondayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsMondayTo1', this.handleChange, this.state.theoreticalLessonsMondayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsMondayFrom2', this.handleChange, this.state.theoreticalLessonsMondayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsMondayTo2', this.handleChange, this.state.theoreticalLessonsMondayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Dienstag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsTuesdayFrom1', this.handleChange, this.state.theoreticalLessonsTuesdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsTuesdayTo1', this.handleChange, this.state.theoreticalLessonsTuesdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsTuesdayFrom2', this.handleChange, this.state.theoreticalLessonsTuesdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsTuesdayTo2', this.handleChange, this.state.theoreticalLessonsTuesdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Mittwoch</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsWednesdayFrom1', this.handleChange, this.state.theoreticalLessonsWednesdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsWednesdayTo1', this.handleChange, this.state.theoreticalLessonsWednesdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsWednesdayFrom2', this.handleChange, this.state.theoreticalLessonsWednesdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsWednesdayTo2', this.handleChange, this.state.theoreticalLessonsWednesdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Donnerstag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsThursdayFrom1', this.handleChange, this.state.theoreticalLessonsThursdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsThursdayTo1', this.handleChange, this.state.theoreticalLessonsThursdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsThursdayFrom2', this.handleChange, this.state.theoreticalLessonsThursdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsThursdayTo2', this.handleChange, this.state.theoreticalLessonsThursdayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Freitag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsFridayFrom1', this.handleChange, this.state.theoreticalLessonsFridayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsFridayTo1', this.handleChange, this.state.theoreticalLessonsFridayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsFridayFrom2', this.handleChange, this.state.theoreticalLessonsFridayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsFridayTo2', this.handleChange, this.state.theoreticalLessonsFridayTo2)}
            </div>
          </div>
          <div className="form-group">
            <label>Samstag</label><br/>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsSaturdayFrom1', this.handleChange, this.state.theoreticalLessonsSaturdayFrom1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsSaturdayTo1', this.handleChange, this.state.theoreticalLessonsSaturdayTo1)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Von</label>
              {timeSelector('theoreticalLessonsSaturdayFrom2', this.handleChange, this.state.theoreticalLessonsSaturdayFrom2)}
            </div>
            <div className="col-md-3 col-xs-12">
              <label>Bis</label>
              {timeSelector('theoreticalLessonsSaturdayTo2', this.handleChange, this.state.theoreticalLessonsSaturdayTo2)}
            </div>
          </div>
        </form>
      </div>
    );
  }

  renderView() {
    const storeEdges = this.props.stores.stores.edges;
    return (
      <div className="card">
        { storeEdges.map((store) => this.renderStore(store)) }
      </div>
    );
  }

  render() {
    let content;
    switch (this.state.mode) {
      case 'add':
      case 'edit':
        content = this.renderAddAndEdit();
        break;

      default:
        content = this.renderView();
    }

    return (
      <div className="container-fluid">
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class AddStoreMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { addStore }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      street: this.props.street,
      postcode: this.props.postcode,
      city: this.props.city,
      phone: this.props.phone,
      openingHours: {
        monday: {
          from1: this.props.openingHoursMondayFrom1,
          to1: this.props.openingHoursMondayTo1,
          from2: this.props.openingHoursMondayFrom2,
          to2: this.props.openingHoursMondayTo2
        },
        tuesday: {
          from1: this.props.openingHoursTuesdayFrom1,
          to1: this.props.openingHoursTuesdayTo1,
          from2: this.props.openingHoursTuesdayFrom2,
          to2: this.props.openingHoursTuesdayTo2
        },
        wednesday: {
          from1: this.props.openingHoursWednesdayFrom1,
          to1: this.props.openingHoursWednesdayTo1,
          from2: this.props.openingHoursWednesdayFrom2,
          to2: this.props.openingHoursWednesdayTo2
        },
        thursday: {
          from1: this.props.openingHoursThursdayFrom1,
          to1: this.props.openingHoursThursdayTo1,
          from2: this.props.openingHoursThursdayFrom2,
          to2: this.props.openingHoursThursdayTo2
        },
        friday: {
          from1: this.props.openingHoursFridayFrom1,
          to1: this.props.openingHoursFridayTo1,
          from2: this.props.openingHoursFridayFrom2,
          to2: this.props.openingHoursFridayTo2
        },
        saturday: {
          from1: this.props.openingHoursSaturdayFrom1,
          to1: this.props.openingHoursSaturdayTo1,
          from2: this.props.openingHoursSaturdayFrom2,
          to2: this.props.openingHoursSaturdayTo2
        }
      },
      theoreticalLessons: {
        monday: {
          from1: this.props.theoreticalLessonsMondayFrom1,
          to1: this.props.theoreticalLessonsMondayTo1,
          from2: this.props.theoreticalLessonsMondayFrom2,
          to2: this.props.theoreticalLessonsMondayTo2
        },
        tuesday: {
          from1: this.props.theoreticalLessonsTuesdayFrom1,
          to1: this.props.theoreticalLessonsTuesdayTo1,
          from2: this.props.theoreticalLessonsTuesdayFrom2,
          to2: this.props.theoreticalLessonsTuesdayTo2
        },
        wednesday: {
          from1: this.props.theoreticalLessonsWednesdayFrom1,
          to1: this.props.theoreticalLessonsWednesdayTo1,
          from2: this.props.theoreticalLessonsWednesdayFrom2,
          to2: this.props.theoreticalLessonsWednesdayTo2
        },
        thursday: {
          from1: this.props.theoreticalLessonsThursdayFrom1,
          to1: this.props.theoreticalLessonsThursdayTo1,
          from2: this.props.theoreticalLessonsThursdayFrom2,
          to2: this.props.theoreticalLessonsThursdayTo2
        },
        friday: {
          from1: this.props.theoreticalLessonsFridayFrom1,
          to1: this.props.theoreticalLessonsFridayTo1,
          from2: this.props.theoreticalLessonsFridayFrom2,
          to2: this.props.theoreticalLessonsFridayTo2
        },
        saturday: {
          from1: this.props.theoreticalLessonsSaturdayFrom1,
          to1: this.props.theoreticalLessonsSaturdayTo1,
          from2: this.props.theoreticalLessonsSaturdayFrom2,
          to2: this.props.theoreticalLessonsSaturdayTo2
        }
      }
    };
  }

  static get fragments() {
    return {
      stores: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddStoreMutationPayload {
        drivingschool {
          stores
        }
        storeEdge
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'stores',
      edgeName: 'storeEdge',
      rangeBehaviors: {
        '': 'append'
      }
    }];
  }
}

class UpdateStoreMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updateStore }`;
  }

  getVariables() {
    return {
      id: this.props.storeId,
      drivingSchoolId: this.props.drivingSchoolId,
      street: this.props.street,
      postcode: this.props.postcode,
      city: this.props.city,
      phone: this.props.phone,
      openingHours: {
        monday: {
          from1: this.props.openingHoursMondayFrom1,
          to1: this.props.openingHoursMondayTo1,
          from2: this.props.openingHoursMondayFrom2,
          to2: this.props.openingHoursMondayTo2
        },
        tuesday: {
          from1: this.props.openingHoursTuesdayFrom1,
          to1: this.props.openingHoursTuesdayTo1,
          from2: this.props.openingHoursTuesdayFrom2,
          to2: this.props.openingHoursTuesdayTo2
        },
        wednesday: {
          from1: this.props.openingHoursWednesdayFrom1,
          to1: this.props.openingHoursWednesdayTo1,
          from2: this.props.openingHoursWednesdayFrom2,
          to2: this.props.openingHoursWednesdayTo2
        },
        thursday: {
          from1: this.props.openingHoursThursdayFrom1,
          to1: this.props.openingHoursThursdayTo1,
          from2: this.props.openingHoursThursdayFrom2,
          to2: this.props.openingHoursThursdayTo2
        },
        friday: {
          from1: this.props.openingHoursFridayFrom1,
          to1: this.props.openingHoursFridayTo1,
          from2: this.props.openingHoursFridayFrom2,
          to2: this.props.openingHoursFridayTo2
        },
        saturday: {
          from1: this.props.openingHoursSaturdayFrom1,
          to1: this.props.openingHoursSaturdayTo1,
          from2: this.props.openingHoursSaturdayFrom2,
          to2: this.props.openingHoursSaturdayTo2
        }
      },
      theoreticalLessons: {
        monday: {
          from1: this.props.theoreticalLessonsMondayFrom1,
          to1: this.props.theoreticalLessonsMondayTo1,
          from2: this.props.theoreticalLessonsMondayFrom2,
          to2: this.props.theoreticalLessonsMondayTo2
        },
        tuesday: {
          from1: this.props.theoreticalLessonsTuesdayFrom1,
          to1: this.props.theoreticalLessonsTuesdayTo1,
          from2: this.props.theoreticalLessonsTuesdayFrom2,
          to2: this.props.theoreticalLessonsTuesdayTo2
        },
        wednesday: {
          from1: this.props.theoreticalLessonsWednesdayFrom1,
          to1: this.props.theoreticalLessonsWednesdayTo1,
          from2: this.props.theoreticalLessonsWednesdayFrom2,
          to2: this.props.theoreticalLessonsWednesdayTo2
        },
        thursday: {
          from1: this.props.theoreticalLessonsThursdayFrom1,
          to1: this.props.theoreticalLessonsThursdayTo1,
          from2: this.props.theoreticalLessonsThursdayFrom2,
          to2: this.props.theoreticalLessonsThursdayTo2
        },
        friday: {
          from1: this.props.theoreticalLessonsFridayFrom1,
          to1: this.props.theoreticalLessonsFridayTo1,
          from2: this.props.theoreticalLessonsFridayFrom2,
          to2: this.props.theoreticalLessonsFridayTo2
        },
        saturday: {
          from1: this.props.theoreticalLessonsSaturdayFrom1,
          to1: this.props.theoreticalLessonsSaturdayTo1,
          from2: this.props.theoreticalLessonsSaturdayFrom2,
          to2: this.props.theoreticalLessonsSaturdayTo2
        }
      }
    };
  }

  static get fragments() {
    return {
      stores: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateStoreMutationPayload {
        drivingschool {
          id
          stores
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}
class DeleteStoreMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { deleteStore }`;
  }

  getVariables() {
    return {
      storeId: this.props.storeId,
      drivingSchoolId: this.props.drivingSchoolId
    };
  }

  static get fragments() {
    return {
      stores: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on DeleteStoreMutationPayload {
        drivingschool
        deletedStoreId
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'drivingschool',
      parentID: this.props.drivingSchoolId,
      connectionName: 'stores',
      deletedIDFieldName: 'deletedStoreId'
    }];
  }
}

const StorePage = Relay.createContainer(
  Store,
  {
    fragments: {
      stores: () => Relay.QL`
        fragment on DrivingSchool {
          id
          stores(first: 1000) {
            edges {
              cursor
              node {
                id
                address {
                  street
                  postcode
                  city
                }
                phone
                openingHours {
                  monday {
                    from1
                    to1
                    from2
                    to2
                  }
                  tuesday {
                    from1
                    to1
                    from2
                    to2
                  }
                  wednesday {
                    from1
                    to1
                    from2
                    to2
                  }
                  thursday {
                    from1
                    to1
                    from2
                    to2
                  }
                  friday {
                    from1
                    to1
                    from2
                    to2
                  }
                  saturday {
                    from1
                    to1
                    from2
                    to2
                  }
                }
                theoreticalLessons {
                  monday {
                    from1
                    to1
                    from2
                    to2
                  }
                  tuesday {
                    from1
                    to1
                    from2
                    to2
                  }
                  wednesday {
                    from1
                    to1
                    from2
                    to2
                  }
                  thursday {
                    from1
                    to1
                    from2
                    to2
                  }
                  friday {
                    from1
                    to1
                    from2
                    to2
                  }
                  saturday {
                    from1
                    to1
                    from2
                    to2
                  }
                }
              }
            }
          }
          ${AddStoreMutation.getFragment('stores')}
          ${UpdateStoreMutation.getFragment('stores')}
          ${DeleteStoreMutation.getFragment('stores')}
        }`
    }
  }
);

const StoreQuery = {
  stores: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  Stores: StorePage,
  StoreQuery
};