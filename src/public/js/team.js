// @flow
'use strict';

import React from 'react';
import Relay from 'react-relay';
import Cropper from './components/cropper';
import { SortableList, SortableListItem } from './components/sortableList';

import { confirm, error, Spinner } from './feedback';

const DefinedRoles = [
  { label: 'Inhaber/in', value: 'owner' },
  { label: 'Geschäftsführer/in', value: 'manager' },
  { label: 'Fahrlehrer/in', value: 'teacher' },
  { label: 'Büro', value: 'office' }];
const DefinedDrivingLicences = ['A', 'A1', 'A2', 'AM', 'B', 'BE', 'B96', 'C', 'C1E', 'C1', 'CE', 'D', 'D1', 'D1E', 'DE', 'L', 'T', 'Mofa'];

type Member = {
  name: string,
  emailAddress: string,
  phone: string,
  roles: Array<string>,
  drivingLicences: Array<string>,
  filename: string,
  image: string
};

type Props = {
  drivingSchool: {
    id: string,
    website: string,
    team: Array<Member>
  }
};

type State = {
  mode: 'add' | 'edit' | 'view',
  name: string,
  emailAddress: string,
  phone: string,
  roles: Array<string>,
  drivingLicences: Array<string>,
  image: string
};

const constraints = {
  name: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie einen Namen ein.'
    }
  }
};

class TeamComponent extends React.Component<Props, State> {

  state = {
    mode: 'view',
    name: '',
    emailAddress: '',
    phone: '',
    roles: [],
    drivingLicences: [],
    image: ''
  };
  currentMember: number = 0;

  handleAdd = () => {
    this.setState({
      mode: 'add',
      name: '',
      emailAddress: '',
      phone: '',
      roles: [],
      drivingLicences: [],
      image: ''
    });
  };

  handleEdit = (idx: number) => {
    this.currentMember = idx;
    const member = this.props.drivingSchool.team[idx];
    this.setState({
      mode: 'edit',
      name: member.name,
      emailAddress: member.emailAddress,
      phone: member.phone,
      roles: member.roles,
      drivingLicences: member.drivingLicences,
      image: ''
    });
  };

  handleCancel = () => {
    this.currentMember = 0;
    this.setState({
      mode: 'view',
      name: '',
      emailAddress: '',
      phone: '',
      roles: [],
      drivingLicences: [],
      image: ''
    });
  };

  handleChange = (event: Object) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleCheckboxChange = (event: Object) => {
    const arr = this.state[event.target.name].slice();
    const idx = arr.indexOf(event.target.value);
    if (idx > -1) {
      arr.splice(idx, 1);
    } else {
      arr.push(event.target.value);
    }
    this.setState({ [event.target.name]: arr });
  };

  handleImageData = (image: string) => {
    this.setState({ image });
  };

  handleConfirmDelete = (idx: number) => {
    this.currentMember = idx;
    confirm('Löschen bestätigen', 'Wollen Sie das Team-Mitglied wirklich löschen?', this.handleDelete);
  };

  handleMove = (dndResult) => {
    Spinner.start();
    Relay.Store.commitUpdate(
      new SortTeamMutation({
        drivingSchoolId: this.props.drivingSchool.id,
        from: dndResult.from,
        to: dndResult.to,
        drivingSchool: null
      }), {
        onSuccess: () => {
          Spinner.success('Sortierung wurde geändert');
          this.setState({ mode: 'view' });
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Sortierung konnte nicht geändert werden');
        }
      }
    );
  };

  handleDelete = () => {
    const member = this.props.drivingSchool.team[this.currentMember];
    // $FlowIgnore
    delete member.__dataID__;

    Spinner.start();
    Relay.Store.commitUpdate(
      new DeleteTeamMutation({
        drivingSchoolId: this.props.drivingSchool.id,
        index: this.currentMember,
        drivingSchool: null
      }), {
        onSuccess: () => {
          Spinner.success('Team-Mitglied wurde gelöscht');
          this.setState({ mode: 'view' });
        },
        onFailure: (transaction) => {
          const error = transaction.getError() || new Error('Mutation failed.');
          Spinner.failure(error, 'Team-Mitglied konnte nicht gelöscht werden');
        }
      }
    );
  };

  handleSave = () => {
    // $FlowIgnore
    const validationResult = validate(this.state, constraints, { format: 'flat', fullMessages: false });
    if (validationResult) {
      error(validationResult, 'Bitte geben Sie die Daten ein.');
      return;
    }

    const member = {
      name: this.state.name,
      emailAddress: this.state.emailAddress,
      phone: this.state.phone,
      roles: this.state.roles,
      drivingLicences: this.state.drivingLicences,
      image: this.state.image
    };

    Spinner.start();
    if (this.state.mode === 'edit') {
      Relay.Store.commitUpdate(
        new UpdateTeamMutation({
          drivingSchoolId: this.props.drivingSchool.id,
          index: this.currentMember,
          member: member,
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Team-Mitglied wurde geändert');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Team-Mitglied konnte nicht geändert werden');
          }
        }
      );
    } else if (this.state.mode === 'add') {
      Relay.Store.commitUpdate(
        new AddTeamMutation({
          drivingSchoolId: this.props.drivingSchool.id,
          member: member,
          drivingSchool: null
        }), {
          onSuccess: () => {
            Spinner.success('Team-Mitglied wurde eingefügt');
            this.setState({ mode: 'view' });
          },
          onFailure: (transaction) => {
            const error = transaction.getError() || new Error('Mutation failed.');
            Spinner.failure(error, 'Team-Mitglied konnte nicht eingefügt werden');
          }
        }
      );
    }
  };

  renderEditBar() {
    if (this.state.mode === 'view') {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Neues Team-Mitglied anlegen" onClick={this.handleAdd}><i className="ti-plus"></i></button>
        </div>
      );
    } else {
      return (
        <div className="editbar">
          <button type="button" className="btn editbar-btn" title="Speichern" onClick={this.handleSave}><i className="ti-save"></i></button>
          <button type="button" className="btn editbar-btn" title="Abbrechen" onClick={this.handleCancel}><i className="ti-close"></i></button>
        </div>
      );
    }
  }

  renderAddOrEdit() {
    return (
      <div className="card">
        <form encType="multipart/form-data">
          <div className="form-group">
            <label>Name</label>
            <input name="name" type="text" className="form-control border-input" onChange={this.handleChange} value={this.state.name}/>
          </div>
          <div className="form-group">
            <label>E-Mail</label>
            <input name="emailAddress" type="email" className="form-control border-input" onChange={this.handleChange} value={this.state.emailAddress}/>
          </div>
          <div className="form-group">
            <label>Telefon</label>
            <input name="phone" type="phone" className="form-control border-input" onChange={this.handleChange} value={this.state.phone}/>
          </div>
          <div className="form-group">
            <label>Funktion</label>
            <div className="form-control">
            {
              DefinedRoles.map((role, idx) => {
                return <span key={idx} style={{ paddingRight: '16px' }}><input name="roles" type="checkbox" className="border-input" onChange={this.handleCheckboxChange} value={role.value} checked={this.state.roles.indexOf(role.value) > -1}/> {role.label}</span>;
              })
            }
            </div>
          </div>
          <div className="form-group">
            <label>Ausbildungsklassen</label>
            <div className="form-control">
            {
              DefinedDrivingLicences.map((drivingLicence, idx) => {
                return <span key={idx} style={{ paddingRight: '16px' }}><input name="drivingLicences" type="checkbox" className="border-input" onChange={this.handleCheckboxChange} value={drivingLicence} checked={this.state.drivingLicences.indexOf(drivingLicence) > -1}/> {drivingLicence}</span>;
              })
            }
            </div>
          </div>
          <div className="form-group">
            <label>Bild</label>
            <Cropper
              image={ this.handleImageData }
              style={{ height: '525px', width: '525px' }}
              aspectRatio={1 / 1}
            />
          </div>
        </form>
      </div>
    );
  }

  renderView() {
    const team = this.props.drivingSchool.team;
    return (
      <div className="card">
        <SortableList>
          {
            team.map((member, idx) =>
            <SortableListItem key={idx} idx={idx} onDrop={this.handleMove}>
              <div className="row">
                <div style={{ float: 'left' }}>
                  <i className="ti-arrows-vertical"></i>
                </div>
                <div>
                  <div className="col-sm-1">
                    {
                      member.filename ? <img src={`${this.props.drivingSchool.website}/static/images/team/${member.filename}`} className="img-responsive"/> : ''
                    }
                  </div>
                  <div className="col-sm-2">{member.name}</div>
                  <div className="col-sm-3">{member.emailAddress}</div>
                  <div className="col-sm-3">{member.phone}</div>
                  <div className="col-sm-2">
                    <button type="button" className="btn btn-primary pull-right" title="Team-Mitglied löschen" onClick={() => this.handleConfirmDelete(idx)}>
                      <i className="ti-trash"></i>
                    </button>
                    <button type="button" className="btn btn-primary pull-right" style={{ marginRight: '10px' }} title="Team-Mitglied bearbeiten" onClick={() => this.handleEdit(idx)}>
                      <i className="ti-pencil"></i>
                    </button>
                  </div>
                </div>
              </div>
            </SortableListItem>
          )}
        </SortableList>
      </div>
    );
  }

  render() {
    let content;
    switch (this.state.mode) {
      case 'add':
      case 'edit':
        content = this.renderAddOrEdit();
        break;

      case 'view':
      default:
        content = this.renderView();
    }

    return (
      <div>
        {this.renderEditBar()}
        {content}
      </div>
    );
  }
}

class AddTeamMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { addTeam }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      member: this.props.member
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          website
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddTeamMutationPayload {
        drivingschool {
          team
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class UpdateTeamMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { updateTeam }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      index: this.props.index,
      member: this.props.member
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateTeamMutationPayload {
        drivingschool {
          id
          website
          team
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class SortTeamMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { sortTeam }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      from: this.props.from,
      to: this.props.to
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
        }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on SortTeamMutationPayload {
        drivingschool {
          id
          website
          team
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

class DeleteTeamMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation { deleteTeam }`;
  }

  getVariables() {
    return {
      drivingSchoolId: this.props.drivingSchoolId,
      index: this.props.index
    };
  }

  static get fragments() {
    return {
      drivingSchool: () => Relay.QL`
          fragment on DrivingSchool {
              id
              website
          }
      `
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on DeleteTeamMutationPayload {
        drivingschool {
          id
          team
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        drivingschool: this.props.drivingSchoolId
      }
    }];
  }
}

const TeamPage = Relay.createContainer(
  TeamComponent,
  {
    fragments: {
      drivingSchool: () => Relay.QL`
        fragment on DrivingSchool {
          id
          website
          team {
            name
            emailAddress
            phone
            roles
            drivingLicences
            filename
          }
          ${AddTeamMutation.getFragment('drivingSchool')}
          ${UpdateTeamMutation.getFragment('drivingSchool')}
          ${SortTeamMutation.getFragment('drivingSchool')}
          ${DeleteTeamMutation.getFragment('drivingSchool')}
        }`
    }
  }
);

const TeamQuery = {
  drivingSchool: () => Relay.QL`
    query {
      drivingschool(id: $id)
    }`
};

module.exports = {
  Team: TeamPage,
  TeamQuery
};