// @flow
'use strict';

const fs = require('fs');
const gm = require('gm');
const path = require('path');
const uuid = require('uuid');
const util = require('util');

const { fromGlobalId } = require('graphql-relay');
const { ObjectId } = require('mongodb');
const credential = require('credential');
const url = require('url');
const validate = require('validate.js');

import * as arrayUtils from '../lib/utils/array';
const cryptoUtils = require('../lib/utils/crypto');
const mailer = require('../lib/utils/mailer');

// $FlowIgnore
const copyFileAsync = util.promisify(fs.copyFile);
const unlinkAsync = util.promisify(fs.unlink);
const writeFileAsync = util.promisify(fs.writeFile);

validate.validators.boolean = function(value, options, /*key, attributes*/) {
  if (!value) return options;
};

function _handleImage(image: Buffer, fileName: string, filePath: string, width: number, height: number) {
  return new Promise((resolve, reject) => {
    gm(image, fileName).resize(width, height).write(filePath, (err) => {
      if (err) return reject(err);
      resolve();
    });
  });
}

function _getSitePath(website: string): string {
  const sitesBase: string = process.env.SITESBASE ? process.env.SITESBASE.toString() : '';
  return path.join(sitesBase, website);
}

async function _saveImage(req: Object, website: string, folder: string, width: number, height: number): Promise<string> {
  const sitePath = _getSitePath(website);
  const image = req.file.buffer;
  const imageFileName = uuid.v4() + path.extname(req.file.originalname);

  const imageFilePathSrc = path.join(sitePath, `static/images/${folder}`, imageFileName);
  await _handleImage(image, imageFileName, imageFilePathSrc, width, height);

  const imageFilePathPublic = path.join(sitePath, `public/images/${folder}`, imageFileName);
  await copyFileAsync(imageFilePathSrc, imageFilePathPublic);

  return imageFileName;
}

async function _saveImageUrl(imageUrl: string, width: number, height: number, website: string, folder: string): Promise<string> {
  const sitePath = _getSitePath(website);

  const regex = /^data:.+\/(.+);base64,.*$/;

  const matches = imageUrl.substr(0, 200).match(regex);
  if (matches && matches.length > 1) {
    const ext: string = matches[1];

    const idx = imageUrl.indexOf('base64,');
    const data = imageUrl.substr(idx + 'base64,'.length);
    const image = new Buffer(data, 'base64');
    const imageFileName = uuid.v4() + '.' + ext;

    const imageFilePathSrc = path.join(sitePath, `static/images/${folder}`, imageFileName);
    await _handleImage(image, imageFileName, imageFilePathSrc, width, height);

    const imageFilePathPublic = path.join(sitePath, `public/images/${folder}`, imageFileName);
    await copyFileAsync(imageFilePathSrc, imageFilePathPublic);

    return imageFileName;
  }

  return '';
}

async function _deleteImage(website: string, folder: string, filename: string) {
  const sitePath = _getSitePath(website);
  const filePathSrc = path.join(sitePath, `static/images/${folder}`, filename);
  await unlinkAsync(filePathSrc);
  const filePathPublic = path.join(sitePath, `public/images/${folder}`, filename);
  await unlinkAsync(filePathPublic);
}

async function _writeComponentConfig(componentData, componentConfigName, sitePath) {
  const configFilePathSrc = path.join(sitePath, `static/${componentConfigName}.json`);
  const configData = JSON.stringify(componentData);
  await writeFileAsync(configFilePathSrc, configData);

  const configFilePathPublic = path.join(sitePath, `public/${componentConfigName}.json`);
  await copyFileAsync(configFilePathSrc, configFilePathPublic);
}

async function getDrivingSchools(context: Object, orderField: string, order: number) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:master:view:*:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('drivingschools').find().sort({[orderField]: order}).toArray();
  }
  return [];
}

async function getDrivingSchoolById(globalId: string, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(globalId);
  if (authorizer.isAllowed(`drivingschool:master:view:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('drivingschools').findOne({_id: ObjectId(id)});
  }
  return null;
}

async function _writeDrivingSchoolConfig(db, id) {
  const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) });
  delete drivingSchool._id;
  const sitePath = _getSitePath(drivingSchool.website.toString());
  await _writeComponentConfig(drivingSchool, 'drivingSchool', sitePath);
}

const drivingschoolOrderConstraints = {
  drivingschool: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie den Namen Ihrer Fahrschule ein.'
    }
  },
  website: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie die Domain Ihrer Fahrschule ein.'
    }
  },
  officerName: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie den Namen des Inhabers oder Geschäftsführers ein.'
    }
  },
  street: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie die Strasse ein.'
    }
  },
  postcode: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie die Postleitzahl ein.'
    }
  },
  city: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie den Ort ein.'
    }
  },
  emailAddress:{
    email: {
      allowEmpty: false,
      message: 'Bitte geben Sie die E-Mail Adresse ein.'
    }
  },
  phone:{
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie die Telefonnummer ein.'
    }
  },
  agb: {
    presence: {
      allowEmpty: false,
      message: 'Sie müssen für die Bestellung die AGB akzeptieren.'
    }
  }
};

async function orderWebsite(args: Object, context: Object) {
  const result = { success: true, errors: [] };
  const validationResult = validate(args, drivingschoolOrderConstraints, { format: 'flat', fullMessages: false });
  if (validationResult === undefined) {
    const { db, req } = context;
    //TODO Daten in der Datenbank als "pending" anlegen
    //TODO IP-Adresse und Uhrzeit speichern
    const data = {
      state: 'pending',
      orderIp: req.ip,
      orderDate: new Date(),
      name: args.drivingschool,
      address: {
        street: args.street,
        postcode: args.postcode,
        city: args.city
      },
      website: args.website,
      emailAddress: args.emailAddress,
      phone: args.phone,
      owner: '',
      managingDirector: ''
    };

    if (args.officerType === 'owner') {
      data.owner = args.officerName;
    } else {
      data.managingDirector = args.officerName;
    }

    try {
      const drivingSchool = await db.collection('drivingschools').findOne({ website: args.website });
      if (!drivingSchool) {
        await db.collection('drivingschools').insertOne(data);

        const token: string = cryptoUtils.encryptData(JSON.stringify({ website: data.website, date: new Date() }));
        const mailData = Object.assign({}, data, { token });
        await mailer.send(args.emailAddress, 'orderWebsiteVerification', { data: mailData });
      } else {
        result.success = false;
        result.errors = ['Die Domain ist bereits registriert.'];
      }
    } catch (err) {
      console.log(err);
      result.success = false;
      result.errors = ['Das hat leider nicht funktioniert. Wenden Sie sich bitte an den Support, wenn das Problem weiterhin besteht.'];
    }
  } else {
    result.success = false;
    result.errors = validationResult;
  }
  return result;
}

async function orderWebsiteVerification(args: Object, context: Object) {
  const result = { success: true, errors: [] };

  const { db, req } = context;
  const data: Object = JSON.parse(cryptoUtils.decryptData(args.token));
  const drivingSchool = await db.collection('drivingschools').findOne({ website: data.website });
  if (drivingSchool) {
    // Fahrschule auf verifiziert setzen
    await db.collection('drivingschools').updateOne({ _id: drivingSchool._id }, {
      $set: {
        state: 'verified',
        verificationIp: req.ip,
        verificationDate: new Date()
      }
    });

    const userObj = await db.collection('users').findOne({ 'credentials.username': drivingSchool.emailAddress });
    if (!userObj) {
      const pw = credential();
      const hash = await pw.hash(uuid.v4());

      const user = {
        credentials: {
          username: drivingSchool.emailAddress,
          hash
        },
        drivingSchoolId: drivingSchool._id,
        role: 'drivingschooladmin'
      };
      await db.collection('users').insertOne(user);
    }
  } else {
    result.success = false;
    result.errors = ['Die Domain ist bereits registriert.'];
  }
  return result;
}

async function updateOrCreateDrivingSchool(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const data = {
    name: args.name,
    address: {
      street: args.street,
      postcode: args.postcode,
      city: args.city
    },
    website: args.website,
    emailAddress: args.emailAddress,
    phone: args.phone,
    fax: args.fax,
    facebookId: args.facebookId,
    instagramId: args.instagramId,
    onlineLearning: args.onlineLearning,
    commercialRegister: args.commercialRegister,
    taxNumber: args.taxNumber,
    taxIdNumber: args.taxIdNumber,
    owner: args.owner,
    managingDirector: args.managingDirector,
    operativeManager: args.operativeManager,
    regulatingAuthority: args.regulatingAuthority
  };

  if (args.id) {
    const { id } = fromGlobalId(args.id);
    if (authorizer.isAllowed(`drivingschool:master:edit:${id}:${session.req.user._id}`, session.req.user.permissions)) {
      const result = await db.collection('drivingschools').updateOne({ _id: ObjectId(id) }, {
        $set: {
          name: data.name,
          address: {
            street: data.address.street,
            postcode: data.address.postcode,
            city: data.address.city
          },
          website: data.website,
          emailAddress: data.emailAddress,
          phone: data.phone,
          fax: data.fax,
          facebookId: data.facebookId,
          instagramId: data.instagramId,
          onlineLearning: data.onlineLearning,
          commercialRegister: data.commercialRegister,
          taxNumber: data.taxNumber,
          taxIdNumber: data.taxIdNumber,
          owner: data.owner,
          managingDirector: data.managingDirector,
          operativeManager: data.operativeManager,
          regulatingAuthority: data.regulatingAuthority
        }
      });
      await _writeDrivingSchoolConfig(db, id);
      return result;
    }
  } else {
    if (authorizer.isAllowed(`drivingschool:master:add:*:${session.req.user._id}`, session.req.user.permissions)) {
      const result = await db.collection('drivingschools').insertOne(data);
      await _writeDrivingSchoolConfig(db, result.insertedId);
      return result;
    }
  }

  return {};
}

async function getCourses(id: string, orderField: string, order: number, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:course:view:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('courses').find({ drivingSchoolId: ObjectId(id) }).sort({[orderField]: order}).toArray();
  }
  return [];
}

async function getCourseById(courseId: string, drivingSchoolId: string, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:course:view:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('courses').findOne({ _id: ObjectId(courseId) });
  }
  return {};
}

async function _writeCourseConfig(db, id, sitePath) {
  const courses = await db.collection('courses').find({ drivingSchoolId: ObjectId(id) })
  .project({ storeId: 1, tag: 1, title: 1, description: 1, availableSeats: 1, filename: 1, courseDays: 1, _id: 0 }).toArray();
  for (let course of courses) {
    const store = await db.collection('stores').findOne({ _id: ObjectId(course.storeId) });
    course.store = { address: store.address, phone: store.phone };
    delete course.storeId;
  }
  await _writeComponentConfig(courses, 'courses', sitePath);
}

async function addCourse(args: Object, context: Object): Promise<Object> {
  const { authorizer, db, session, req } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);

  if (authorizer.isAllowed(`drivingschool:course:add:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const course = {
      drivingSchoolId: ObjectId(id),
      storeId: ObjectId(fromGlobalId(args.storeId).id),
      createdAt: new Date(),
      createdBy: ObjectId(session.req.user._id),
      tag: args.tag,
      title: args.title,
      description: args.description,
      availableSeats: args.availableSeats,
      filename: undefined,
      courseDays: []
    };
    args.courseDays.map(courseDay => course.courseDays.push(courseDay));

    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });
    const sitePath = _getSitePath(drivingSchool.website.toString());

    if (req.file) {
      const file = req.file.buffer;
      const filename = uuid.v4() + path.extname(req.file.originalname);

      try {
        const filePathSrc = path.join(sitePath, 'static/images/courses', filename);
        await _handleImage(file, filename, filePathSrc, 300, 300);

        const filePathPublic = path.join(sitePath, 'public/images/courses', filename);
        await copyFileAsync(filePathSrc, filePathPublic);

        course.filename = filename;
      } catch (e) {
        console.log(e);
      }
    }

    const res = await db.collection('courses').insertOne(course);
    await _writeCourseConfig(db, id, sitePath);
    return res;
  }
  return {};
}

async function updateCourse(args: Object, context: Object) {
  const { authorizer, db, session, req } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:course:edit:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const courseId = fromGlobalId(args.courseId).id;

    const course = {
      storeId: ObjectId(fromGlobalId(args.storeId).id),
      tag: args.tag,
      title: args.title,
      description: args.description,
      availableSeats: args.availableSeats,
      courseDays: []
    };
    args.courseDays.map(courseDay => course.courseDays.push(courseDay));

    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });
    const sitePath = _getSitePath(drivingSchool.website.toString());

    if (req.file) {
      const file = req.file.buffer;
      const filename = uuid.v4() + path.extname(req.file.originalname);

      try {
        const filePathSrc = path.join(sitePath, 'static/images/courses', filename);
        await _handleImage(file, filename, filePathSrc, 300, 300);

        const filePathPublic = path.join(sitePath, 'public/images/courses', filename);
        await copyFileAsync(filePathSrc, filePathPublic);

        // $FlowIgnore
        course.filename = filename;

        const oldCourse = await getCourseById(courseId, id, context);
        if (oldCourse.filename !== null) {
          const oldfilePathSrc = path.join(sitePath, 'static/images/courses', oldCourse.filename);
          await unlinkAsync(oldfilePathSrc);
          const oldfilePathPublic = path.join(sitePath, 'public/images/courses', oldCourse.filename);
          await unlinkAsync(oldfilePathPublic);
        }
      } catch (e) {
        console.log(e);
      }
    }

    await db.collection('courses').updateOne({ _id: ObjectId(courseId)}, { $set: course });
    await _writeCourseConfig(db, id, sitePath);
  }
}

async function deleteCourse(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:course:delete:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });
    const sitePath = _getSitePath(drivingSchool.website.toString());

    const courseId = fromGlobalId(args.courseId).id;
    const course = await db.collection('courses').findOneAndDelete({ _id: ObjectId(courseId) });
    const filename = course.value.filename;

    if (filename) {
      try {
        const filePathSrc = path.join(sitePath, 'static/images/courses', filename);
        await unlinkAsync(filePathSrc);

        const filePathPublic = path.join(sitePath, 'public/images/courses', filename);
        await unlinkAsync(filePathPublic);
      } catch (e) {
        console.log(e);
      }
    }

    await _writeCourseConfig(db, id, sitePath);
  }
}

async function deleteCoursesByStore(storeId: string, args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:course:delete:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });
    const sitePath = _getSitePath(drivingSchool.website.toString());

    const courses = await db.collection('courses').find({ storeId: ObjectId(storeId) }).project({ filename: 1, _id: 0 }).toArray();
    for (const course of courses) {
      if (course.filename) {
        try {
          const filePathSrc = path.join(sitePath, 'static/images/courses', course.filename);
          await unlinkAsync(filePathSrc);

          const filePathPublic = path.join(sitePath, 'public/images/courses', course.filename);
          await unlinkAsync(filePathPublic);
        } catch (e) {
          console.log(e);
        }
      }
    }

    await db.collection('courses').deleteMany({ storeId: ObjectId(storeId) });
    await _writeCourseConfig(db, id, sitePath);
  }
}

async function getPassedGalleries(id: string, orderField: string, order: number, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:passedgallery:view:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('passedgalleries').find({ drivingSchoolId: ObjectId(id) }).sort({[orderField]: order}).toArray();
  }
  return [];
}

async function getPassedGalleryItemById(passedGalleryItemId: string, drivingSchoolId: string, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:passedgallery:view:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('passedgalleries').findOne({ _id: ObjectId(passedGalleryItemId) });
  }
  return {};
}

async function addPassedGalleryItem(args: Object, context: Object): Promise<Object> {
  const { authorizer, db, session, req } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:passedgallery:add:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });

    const imageFileName = await _saveImage(req, drivingSchool.website.toString(), 'passed-gallery', 1000, 1000);

    const passedGalleryItem = {
      drivingSchoolId: ObjectId(id),
      createdAt: new Date(),
      createdBy: ObjectId(session.req.user._id),
      name: args.name,
      date: args.date,
      text: args.text,
      filename: imageFileName
    };

    const res = await db.collection('passedgalleries').insertOne(passedGalleryItem);

    const componentData = await db.collection('passedgalleries')
      .find({ drivingSchoolId: ObjectId(id) })
      .sort({ createdAt: -1 })
      .project({ name: 1, date: 1, text: 1, filename: 1, _id: 0 })
      .toArray();
    await _writeComponentConfig(
      componentData,
      'passed-gallery',
      _getSitePath(drivingSchool.website.toString())
    );

    return res;
  }
  return {};
}

async function updatePassedGalleryItem(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:passedgallery:edit:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });
    const sitePath = _getSitePath(drivingSchool.website.toString());

    const passedGalleryItem = {
      name: args.name,
      date: args.date,
      text: args.text,
    };

    const passedGalleryItemId = fromGlobalId(args.passedGalleryItemId).id;
    await db.collection('passedgalleries').updateOne({ _id: ObjectId(passedGalleryItemId)}, {
      $set: {
        name: passedGalleryItem.name,
        date: passedGalleryItem.date,
        text: passedGalleryItem.text,
      }
    });

    const componentData = await db.collection('passedgalleries')
      .find({ drivingSchoolId: ObjectId(id) })
      .sort({ createdAt: -1 })
      .project({ name: 1, date: 1, text: 1, filename: 1, _id: 0 })
      .toArray();
    await _writeComponentConfig(
      componentData,
      'passed-gallery',
      sitePath
    );
  }
}

async function deletePassedGalleryItem(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:passedgallery:delete:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) }, { website: 1 });

    const passedGalleryItemId = fromGlobalId(args.passedGalleryItemId).id;
    const passedGalleryItem = await db.collection('passedgalleries').findOneAndDelete({ _id: ObjectId(passedGalleryItemId) });

    await _deleteImage(drivingSchool.website.toString(), 'passed-gallery', passedGalleryItem.value.filename);

    const componentData = await db.collection('passedgalleries')
      .find({ drivingSchoolId: ObjectId(id) })
      .sort({ createdAt: -1 })
      .project({ name: 1, date: 1, text: 1, filename: 1, _id: 0 })
      .toArray();
    await _writeComponentConfig(
      componentData,
      'passed-gallery',
      _getSitePath(drivingSchool.website.toString())
    );
  }
}

async function getStores(drivingSchoolId: string, orderField: string, order: number, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:store:view:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('stores').find({ drivingSchoolId: ObjectId(drivingSchoolId) }).sort({[orderField]: order}).toArray();
  }
  return [];
}

async function getStoreById(storeId: string, drivingSchoolId: string, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:store:view:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('stores').findOne({ _id: ObjectId(storeId) });
  }
  return {};
}

async function _writeStoreConfig(db, id) {
  const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(id) });
  const sitePath = _getSitePath(drivingSchool.website);
  const stores = await db.collection('stores').find({ drivingSchoolId: ObjectId(id)} )
    .project({ address: 1, phone: 1, openingHours: 1, theoreticalLessons: 1, _id: 0}).toArray();
  for (const store of stores) {
    store.name = drivingSchool.name;
  }
  await _writeComponentConfig(stores, 'stores', sitePath);
}

async function addStore(args: Object, context: Object): Object {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:store:add:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const store = {
      drivingSchoolId: ObjectId(id),
      createdAt: new Date(),
      createdBy: ObjectId(session.req.user._id),
      address: {
        street: args.street,
        postcode: args.postcode,
        city: args.city
      },
      phone: args.phone,
      openingHours: args.openingHours,
      theoreticalLessons: args.theoreticalLessons
    };

    const res = await db.collection('stores').insertOne(store);
    await _writeStoreConfig(db, id);
    return res;
  }
  return {};
}

async function updateStore(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:store:edit:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const { id } = fromGlobalId(args.id);
    const res = await db.collection('stores').updateOne({ _id: ObjectId(id)}, {
      $set: {
        address: {
          street: args.street,
          postcode: args.postcode,
          city: args.city
        },
        phone: args.phone,
        openingHours: args.openingHours,
        theoreticalLessons: args.theoreticalLessons
      }
    });

    await _writeStoreConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function deleteStore(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:store:delete:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const storeId = fromGlobalId(args.storeId).id;
    await db.collection('stores').deleteOne({ _id: ObjectId(storeId) });
    await deleteCoursesByStore(storeId, args, context);
  }
}

async function addTeam(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:team:add:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });
    if (args.member.image) {
      args.member.filename = await _saveImageUrl(args.member.image, 525, 525, drivingSchool.website.toString(), 'team');
    }
    delete args.member.image;

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $push: {
        team: args.member
      }
    });

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function updateTeam(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:team:edit:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });

    const oldMember = drivingSchool.team[args.index];
    if (args.member.image) {
      args.member.filename = await _saveImageUrl(args.member.image, 525, 525, drivingSchool.website.toString(), 'team');

      if (oldMember.filename) {
        await _deleteImage(drivingSchool.website.toString(), 'team', oldMember.filename);
      }
    } else {
      args.member.filename = oldMember.filename;
    }
    delete args.member.image;

    drivingSchool.team[args.index] = args.member;

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $set: {
        team: drivingSchool.team
      }
    });

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function sortTeam(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:team:edit:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });

    const newTeam = arrayUtils.swapElement(drivingSchool.team, args.from, args.to);

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $set: {
        team: newTeam
      }
    });

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function deleteTeam(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:team:delete:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });
    const removedMember = drivingSchool.team.splice(args.index, 1)[0];

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $set: {
        team: drivingSchool.team
      }
    });

    if (removedMember.filename) {
      await _deleteImage(drivingSchool.website.toString(), 'team', removedMember.filename);
    }

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function addCar(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:car:add:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });
    if (args.image) {
      args.filename = await _saveImageUrl(args.image, 600, 600, drivingSchool.website.toString(), 'cars');
    }
    delete args.image;

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $push: {
        cars: {
          filename: args.filename,
          description: args.description
        }
      }
    });

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function updateCar(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:car:edit:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });

    const oldCar = drivingSchool.cars[args.index];
    if (args.image) {
      args.filename = await _saveImageUrl(args.image, 600, 600, drivingSchool.website.toString(), 'cars');

      if (oldCar.filename) {
        await _deleteImage(drivingSchool.website.toString(), 'cars', oldCar.filename);
      }
    } else {
      args.filename = oldCar.filename;
    }
    delete args.image;

    drivingSchool.cars[args.index] = { filename: args.filename, description: args.description };

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $set: {
        cars: drivingSchool.cars
      }
    });

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function sortCar(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:car:edit:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });

    const newCars = arrayUtils.swapElement(drivingSchool.cars, args.from, args.to);

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $set: {
        cars: newCars
      }
    });

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function deleteCar(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const drivingSchoolId = fromGlobalId(args.drivingSchoolId).id;
  if (authorizer.isAllowed(`drivingschool:car:delete:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    const drivingSchool = await db.collection('drivingschools').findOne({ _id: ObjectId(drivingSchoolId) });
    const removedCar = drivingSchool.cars.splice(args.index, 1)[0];

    const res = await db.collection('drivingschools').updateOne({ _id: ObjectId(drivingSchoolId)}, {
      $set: {
        cars: drivingSchool.cars
      }
    });

    if (removedCar.filename) {
      await _deleteImage(drivingSchool.website.toString(), 'cars', removedCar.filename);
    }

    await _writeDrivingSchoolConfig(db, drivingSchoolId);
    return res;
  }
  return {};
}

async function getUserByUsername(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`user:view:${session.req.user.drivingSchoolId}`, session.req.user.permissions)) {
    return db.collection('users').findOne({ 'credentials.username': args.username });
  }
  return null;
}

async function getUserById(globalId: string, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(globalId);
  if (authorizer.isAllowed(`user:view:${session.req.user.drivingSchoolId}`, session.req.user.permissions)) {
    return db.collection('users').findOne({ _id: id });
  }
  return null;
}

async function updateUser(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`user:edit:${session.req.user.role}:${session.req.user.drivingSchoolId}`, session.req.user.permissions)) {
    //TODO E-Mail versenden, um die neue E-Mail Adresse zu bestätigen
    session.req.user.firstname = args.firstname;
    session.req.user.lastname = args.lastname;
    const { id } = fromGlobalId(args.id);
    return db.collection('users').updateOne({ _id: ObjectId(id) }, { $set: { firstname: args.firstname, lastname: args.lastname }});
  }
  return null;
}

async function updatePassword(args: Object, context: Object) {
  const { password1, password2 } = args;
  if (password1 !== password2) throw new Error('Passwörter stimmen nicht überein.');

  const { db, session } = context;

  //TODO Passwort-Policy gültig

  const user: Object = session.req.user;
  const pw = credential();
  const hash = await pw.hash(password1);
  await db.collection('users').updateOne({ _id: ObjectId(user._id) }, { $set: { 'credentials.hash': hash }});
  return 'Passwort erfolgreich geändert.';
}

const contactRequestConstraints = {
  name: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihren Namen ein.'
    }
  },
  emailAddress: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  },
  mobile: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihre Mobilfunknummer ein.'
    }
  },
  message: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie eine Nachricht ein.'
    }
  }
};

const onlineRegistrationConstraints = {
  lastname: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Deinen Nachnamen ein.'
    }
  },
  firstname: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Deinen Vornamen ein.'
    }
  },
  street: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Deine Strasse ein.'
    }
  },
  zipcode: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Deine Postleitzahl ein.'
    }
  },
  city: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Deinen Wohnort ein.'
    }
  },
  birthdate: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Dein Geburtsdatum ein.'
    }
  },
  phone: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib Deine Telefonnummer ein.'
    }
  },
  emailAddress: {
    email: {
      message: 'Bitte gib eine gültige E-Mail Adresse ein.'
    }
  },
  licence: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib die gewünschte Führerscheinklasse ein.'
    }
  },
  startDate: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib das Datum ein, an dem Du beginnen möchtest.'
    }
  },
  endDate: {
    presence: {
      allowEmpty: false,
      message: 'Bitte gib das Datum ein, an dem Du fertig sein möchtest.'
    }
  }
};

async function sendContactRequest(args: Object, context: Object) {
  const result = { success: true, errors: [] };
  try {
    const origin = context.req.headers['origin'];
    if (origin) {
      const contactRequestData = {
        name: args.name,
        mobile: args.mobile,
        emailAddress: args.emailAddress,
        subject: args.subject,
        message: args.message
      };
      const validationResult = validate(contactRequestData, contactRequestConstraints, { format: 'flat', fullMessages: false });
      if (validationResult === undefined) {
        const hostname = url.parse(origin).hostname;
        const {db} = context;
        const drivingschool = await db.collection('drivingschools').findOne({website: hostname});
        if (drivingschool) {
          await mailer.send(drivingschool.emailAddress, 'contactrequest', { data: contactRequestData }, [], contactRequestData.emailAddress);
        }
      } else {
        result.success = false;
        result.errors = validationResult;
      }
    } else {
      console.log(`no origin given: ${context.req.url}`);
    }
  } catch (err) {
    console.log(err);
    result.success = false;
    result.errors = [ err.toString() ];
    mailer.send('support@drive-right.de', 'severe-error', { data: { error: err }})
      .catch((err) => console.log(err));
  }
  return result;
}

async function sendOnlineRegistration(args: Object, context: Object) {
  const result = { success: true, errors: [] };
  try {
    const origin = context.req.headers['origin'];
    if (origin) {
      const onlineRegistrationData = {
        lastname: args.lastname,
        firstname: args.firstname,
        street: args.street,
        zipcode: args.zipcode,
        city: args.city,
        birthdate: args.birthdate,
        phone: args.phone,
        emailAddress: args.emailAddress,
        whereToFindUs: args.whereToFindUs,
        licence: args.licence,
        owningLicences: args.owningLicences,
        startDate: args.startDate,
        endDate: args.endDate
      };
      const validationResult = validate(onlineRegistrationData, onlineRegistrationConstraints, { format: 'flat', fullMessages: false });
      if (validationResult === undefined) {
        const hostname = url.parse(origin).hostname;
        const {db} = context;
        const drivingschool = await db.collection('drivingschools').findOne({website: hostname});
        if (drivingschool) {
          await mailer.send(drivingschool.emailAddress, 'onlineregistration', { data: onlineRegistrationData }, [], onlineRegistrationData.emailAddress);
        }
      } else {
        result.success = false;
        result.errors = validationResult;
      }
    } else {
      console.log(`no origin given: ${context.req.url}`);
    }
  } catch (err) {
    console.log(err);
    result.success = false;
    result.errors = [ err.toString() ];
    mailer.send('support@drive-right.de', 'severe-error', { data: { error: err }})
      .catch((err) => console.log(err));
  }
  return result;
}

const campaignParticipantConstraints = {
  name: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihren Namen ein.'
    }
  },
  street: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihre Strasse ein.'
    }
  },
  postcode: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihre Postleitzahl ein.'
    }
  },
  city: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihren Wohnort ein.'
    }
  },
  phone: {
    presence: {
      allowEmpty: false,
      message: 'Bitte geben Sie Ihre Telefonnummer ein.'
    }
  },
  emailAddress: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  },
  acceptRequirements: {
    boolean: 'Sie müssen den Teilnahmebedingungen zustimmen.'
  }
};

async function getCampaignParticipants(drivingSchoolId: string, orderField: string, order: number, context: Object) {
  const { authorizer, db, session } = context;
  if (authorizer.isAllowed(`drivingschool:campaignparticipant:view:${drivingSchoolId}:${session.req.user._id}`, session.req.user.permissions)) {
    return await db.collection('campaignparticipants').find({ drivingSchoolId: ObjectId(drivingSchoolId) }).sort({[orderField]: order}).toArray();
  }
  return [];
}

async function addCampaignParticipant(args: Object, context: Object) {
  const result = { success: true, errors: [] };
  try {
    const origin = context.req.headers['origin'];
    if (origin) {
      const validationResult = validate(args, campaignParticipantConstraints, { format: 'flat', fullMessages: false });
      if (validationResult === undefined) {
        const hostname = url.parse(origin).hostname;
        const {db} = context;
        const drivingschool = await db.collection('drivingschools').findOne({ website: hostname });
        if (drivingschool) {
          const campaignParticipantData = {
            drivingSchoolId: drivingschool._id,
            createdAt: new Date(),
            solution: args.solution,
            name: args.name,
            street: args.street,
            postcode: args.postcode,
            city: args.city,
            phone: args.phone,
            emailAddress: args.emailAddress
          };
          await db.collection('campaignparticipants').insertOne(campaignParticipantData);
        }
      } else {
        result.success = false;
        result.errors = validationResult;
      }
    } else {
      console.log(`no origin given: ${context.req.url}`);
    }
  } catch (err) {
    console.log(err);
    result.success = false;
    result.errors = [ err.toString() ];
  }
  return result;
}

async function deleteCampaignParticipant(args: Object, context: Object) {
  const { authorizer, db, session } = context;
  const { id } = fromGlobalId(args.drivingSchoolId);
  if (authorizer.isAllowed(`drivingschool:campaignparticipant:delete:${id}:${session.req.user._id}`, session.req.user.permissions)) {
    const campaignParticipantId = fromGlobalId(args.campaignParticipantId).id;
    await db.collection('campaignparticipants').deleteOne({ _id: ObjectId(campaignParticipantId) });
  }
}

// const inviteUserConstraints = {
//   emailAddress: {
//     email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
//   }
// };

// async function inviteUser(args: Object, context: Object) {
//   const result = { success: true, errors: [] };
//   try {
//     const {authorizer, db, session} = context;
//     const drivingschool = await db.collection('drivingschools').findOne({_id: args.id});
//     if (authorizer.isAllowed(`user:add:${args.role}:${args.id}`, session.req.user.permissions)) {
//       if (drivingschool) {
//         const inviteUserData = {
//           drivingschool: drivingschool.name,
//           emailAddress: args.emailAddress,
//           token: ''
//         };
//         const validationResult = validate(inviteUserData, inviteUserConstraints, {format: 'flat', fullMessages: false});
//         if (validationResult === undefined) {
//           const encryptedData = cryptoUtils.encryptData(`{id:'${args.id}',username:'${args.emailAddress}',role:'${args.role}'}`);
//           Object.assign(inviteUserData, { token: encryptedData });
//           await mailer.send(inviteUserData.emailAddress, 'inviteuser', {data: inviteUserData});
//         } else {
//           result.success = false;
//           result.errors = validationResult;
//         }
//       } else {
//         result.success = false;
//         result.errors = ['Fahrschule nicht gefunden.'];
//       }
//     }
//   } catch (err) {
//     console.log(err);
//     result.success = false;
//     result.errors = [err.toString()];
//   }
//   return result;
// }

module.exports = {
  orderWebsite,
  orderWebsiteVerification,

  getDrivingSchools,
  getDrivingSchoolById,
  updateOrCreateDrivingSchool,

  getCourses,
  getCourseById,
  addCourse,
  updateCourse,
  deleteCourse,

  getPassedGalleries,
  getPassedGalleryItemById,
  addPassedGalleryItem,
  updatePassedGalleryItem,
  deletePassedGalleryItem,

  getStores,
  getStoreById,
  addStore,
  updateStore,
  deleteStore,

  addTeam,
  updateTeam,
  sortTeam,
  deleteTeam,

  addCar,
  updateCar,
  sortCar,
  deleteCar,

  getUserById,
  getUserByUsername,
  // inviteUser,
  updatePassword,
  updateUser,

  sendContactRequest,
  sendOnlineRegistration,

  getCampaignParticipants,
  addCampaignParticipant,
  deleteCampaignParticipant
};
