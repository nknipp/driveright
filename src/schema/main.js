// @flow
'use strict';

const validate = require('validate.js');
const _ = require('underscore');

const {
  GraphQLBoolean,
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString
} = require('graphql');

const {
  connectionArgs,
  connectionDefinitions,
  connectionFromPromisedArray,
  cursorForObjectInConnection,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
  offsetToCursor
} = require('graphql-relay');

const { ObjectId } = require('mongodb');

const {
  orderWebsite,
  orderWebsiteVerification,

  getDrivingSchools,
  getDrivingSchoolById,
  updateOrCreateDrivingSchool,

  getCourses,
  getCourseById,
  addCourse,
  updateCourse,
  deleteCourse,

  getPassedGalleries,
  getPassedGalleryItemById,
  addPassedGalleryItem,
  updatePassedGalleryItem,
  deletePassedGalleryItem,

  getStores,
  getStoreById,
  addStore,
  updateStore,
  deleteStore,

  addTeam,
  updateTeam,
  sortTeam,
  deleteTeam,

  addCar,
  updateCar,
  sortCar,
  deleteCar,

  getUserById,
  // inviteUser,
  updateUser,
  updatePassword,

  sendContactRequest,
  sendOnlineRegistration,

  getCampaignParticipants,
  addCampaignParticipant,
  deleteCampaignParticipant
} = require('./loader');

const { nodeInterface, nodeField } = nodeDefinitions(
  async (globalId, context) => {
    const { type, id } = fromGlobalId(globalId);
    switch (type) {
      case 'drivingschool': {
        const res = await getDrivingSchoolById(globalId, context);
        // $FlowIgnore
        res.type = DrivingSchoolType;
        return res;
      }

      case 'passedgallery': {
        const passedGalleryItem = await context.db.collection('passedgalleries').findOne({ _id: ObjectId(id) });
        const res = await getPassedGalleryItemById(globalId, passedGalleryItem.drivingSchoolId, context);
        // $FlowIgnore
        res.type = PassedGalleryType;
        return res;
      }

      case 'course': {
        const course = await context.db.collection('courses').findOne({ _id: ObjectId(id) });
        const res = await getCourseById(id, course.drivingSchoolId, context);
        // $FlowIgnore
        res.type = CourseType;
        return res;
      }

      case 'store': {
        const store = await context.db.collection('stores').findOne({ _id: ObjectId(id) });
        const res = await getStoreById(id, store.drivingSchoolId, context);
        // $FlowIgnore
        res.type = StoreType;
        return res;
      }

      case 'campaignparticipant': {
        const campaignParticipant = await context.db.collection('campaignparticipants').findOne({ _id: ObjectId(id) });
        const res = await getStoreById(id, campaignParticipant.drivingSchoolId, context);
        // $FlowIgnore
        res.type = CampaignParticipantType;
        return res;
      }

      case 'user':
        return getUserById(globalId, context);

      default:
        return null;
    }
  },
  (obj) => obj.type
);

const AddressType = new GraphQLObjectType({
  name: 'Address',
  fields: {
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) }
  }
});

const PassedGalleryType =  new GraphQLObjectType({
  name: 'PassedGallery',
  interfaces: [nodeInterface],
  fields: {
    id: globalIdField('passedgallery', obj => obj._id),
    createdAt: { type: new GraphQLNonNull(GraphQLString) },
    createdBy: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    date: { type: GraphQLString },
    text: { type: GraphQLString },
    filename: { type: new GraphQLNonNull(GraphQLString) }
  }
});

const {
  connectionType: PassedGalleryConnection,
  edgeType: PassedGalleryEdge
} = connectionDefinitions({ nodeType: PassedGalleryType });

const CourseDayType = new GraphQLObjectType({
  name: 'CourseDay',
  fields: {
    date: { type: new GraphQLNonNull(GraphQLString) },
    timeFrom: { type: GraphQLString },
    timeTo: { type: GraphQLString },
    lesson: { type: GraphQLString },
    instructor: { type: GraphQLString }
  }
});

const CourseType = new GraphQLObjectType({
  name: 'Course',
  interfaces: [nodeInterface],
  fields: {
    id: globalIdField('course', obj => obj._id),
    createdAt: { type: new GraphQLNonNull(GraphQLString) },
    createdBy: { type: new GraphQLNonNull(GraphQLString) },
    storeId: globalIdField('store', obj => obj.storeId),
    tag: { type: GraphQLString },
    title: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString },
    availableSeats: { type: GraphQLInt },
    filename: { type: GraphQLString },
    courseDays: { type: new GraphQLList(CourseDayType) }
  }
});

const {
  connectionType: CourseConnection,
  edgeType: CourseEdge
} = connectionDefinitions({ nodeType: CourseType });

const OpeningHoursDayType = new GraphQLObjectType({
  name: 'OpeningHoursDay',
  fields: {
    from1: { type: GraphQLString },
    to1: { type: GraphQLString },
    from2: { type: GraphQLString },
    to2: { type: GraphQLString }
  }
});

const OpeningHoursWeekType = new GraphQLObjectType({
  name: 'OpeningHoursWeek',
  fields: {
    monday: { type: OpeningHoursDayType },
    tuesday: { type: OpeningHoursDayType },
    wednesday: { type: OpeningHoursDayType },
    thursday: { type: OpeningHoursDayType },
    friday: { type: OpeningHoursDayType },
    saturday: { type: OpeningHoursDayType }
  }
});

const StoreType = new GraphQLObjectType({
  name: 'Store',
  interfaces: [nodeInterface],
  fields: {
    id: globalIdField('store', obj => obj._id),
    createdAt: { type: new GraphQLNonNull(GraphQLString) },
    createdBy: { type: new GraphQLNonNull(GraphQLString) },
    address: { type: AddressType },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    openingHours: { type: OpeningHoursWeekType },
    theoreticalLessons: { type: OpeningHoursWeekType }
  }
});

const {
  connectionType: StoreConnection,
  edgeType: StoreEdge
} = connectionDefinitions({ nodeType: StoreType });

const CampaignParticipantType = new GraphQLObjectType({
  name: 'CampaignParticipant',
  interfaces: [nodeInterface],
  fields: {
    id: globalIdField('campaignParticipant', obj => obj._id),
    solution: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    acceptRequirements: { type: new GraphQLNonNull(GraphQLBoolean) }
  }
});

const {
  connectionType: CampaignParticipantConnection,
  edgeType: CampaignParticipantEdge
} = connectionDefinitions({ nodeType: CampaignParticipantType });

const TeamMemberType = new GraphQLObjectType({
  name: 'TeamMember',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: GraphQLString },
    phone: { type: GraphQLString },
    roles: { type: new GraphQLList(GraphQLString) },
    drivingLicences: { type: new GraphQLList(GraphQLString) },
    filename: { type: GraphQLString }
  }
});

const CarType = new GraphQLObjectType({
  name: 'Car',
  fields: {
    filename: { type: GraphQLString },
    description: { type: GraphQLString }
  }
});

const DrivingSchoolType = new GraphQLObjectType({
  name: 'DrivingSchool',
  interfaces: [nodeInterface],
  fields: {
    id: globalIdField('drivingschool', obj => obj._id),
    name: { type: GraphQLString },
    address: { type: AddressType },
    website: { type: GraphQLString },
    emailAddress: { type: GraphQLString },
    phone: { type: GraphQLString },
    fax: { type: GraphQLString },
    facebookId: { type: GraphQLString },
    instagramId: { type: GraphQLString },
    onlineLearning: { type: GraphQLString },
    commercialRegister: { type: GraphQLString },
    taxNumber: { type: GraphQLString },
    taxIdNumber: { type: GraphQLString },
    owner: { type: GraphQLString },
    managingDirector: { type: GraphQLString },
    operativeManager: { type: GraphQLString },
    regulatingAuthority: { type: GraphQLString },
    team: {
      type: new GraphQLList(TeamMemberType),
      resolve: (obj) => {
        if (obj.team) return obj.team;
        else return [];
      }
    },
    cars: {
      type: new GraphQLList(CarType),
      resolve: obj => {
        if (obj.cars) return obj.cars;
        else return [];
      }
    },
    courses: {
      type: CourseConnection,
      args: connectionArgs,
      resolve: (obj, args, context) => connectionFromPromisedArray(getCourses(obj._id.toString(), 'createdAt', -1, context), args)
    },
    passedGallery: {
      type: PassedGalleryConnection,
      args: connectionArgs,
      resolve: (obj, args, context) => connectionFromPromisedArray(getPassedGalleries(obj._id.toString(), 'createdAt', -1, context), args)
    },
    stores: {
      type: StoreConnection,
      args: connectionArgs,
      resolve: (obj, args, context) => connectionFromPromisedArray(getStores(obj._id, 'createdAt', 1, context), args)
    },
    campaignParticipants: {
      type: CampaignParticipantConnection,
      args: connectionArgs,
      resolve: (obj, args, context) => connectionFromPromisedArray(getCampaignParticipants(obj._id, 'createdAt', 1, context), args)
    }
  }
});

const {
  connectionType: DrivingSchoolConnection,
  edgeType: DrivingSchoolEdge
} = connectionDefinitions({ nodeType: DrivingSchoolType });

const UserType = new GraphQLObjectType({
  name: 'User',
  fields: {
    id: globalIdField('user', obj => obj._id),
    username: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: obj => obj.credentials.username
    },
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString },
    drivingSchoolId: globalIdField('drivingschool', obj => obj.drivingSchoolId),
    role: { type: new GraphQLNonNull(GraphQLString) },
    permissions: { type: new GraphQLList(GraphQLString) },
    isloggedin: {
      type: new GraphQLNonNull(GraphQLBoolean),
      resolve: (obj) => obj.role !== 'guest'
    }
  }
});

const Viewer = new GraphQLObjectType({
  name: 'Viewer',
  fields: () => ({
    user: {
      type: UserType,
      resolve: (obj, args, context) => context.session.req.user
    },
    alldrivingschools: {
      type: DrivingSchoolConnection,
      args: connectionArgs,
      resolve: (obj, args, context) => connectionFromPromisedArray(getDrivingSchools(context, 'name', 1), args)
    }
  })
});

const queryType = new GraphQLObjectType({
  name: 'RootQuery',
  fields: {
    node: nodeField,
    viewer: {
      type: Viewer,
      resolve: () => { return {}; }
    },
    drivingschool: {
      type: DrivingSchoolType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: async (obj, args, context) => await getDrivingSchoolById(args.id, context)
    }
  }
});

const ResultStateType = new GraphQLObjectType({
  name: 'ResultState',
  fields: {
    success: { type: new GraphQLNonNull(GraphQLBoolean) },
    errors: { type: new GraphQLList(GraphQLString) }
  }
});

const orderWebsiteMutation = {
  type: ResultStateType,
  args: {
    drivingschool: { type: new GraphQLNonNull(GraphQLString) },
    website: { type: new GraphQLNonNull(GraphQLString) },
    officerType: { type: new GraphQLNonNull(GraphQLString) },
    officerName: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    agb: { type: GraphQLString}
  },
  resolve: async (obj, args, context) => {
    return orderWebsite(args, context);
  }
};

const orderWebsiteVerificationMutation = {
  type: ResultStateType,
  args: {
    token: { type : new GraphQLNonNull(GraphQLString) }
  },
  resolve: async (obj, args, context) => {
    return orderWebsiteVerification(args, context);
  }
};

const addDrivingSchoolMutation = mutationWithClientMutationId({
  name: 'AddDrivingSchoolMutation',
  inputFields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    website: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    fax: { type: GraphQLString },
    facebookId: { type: GraphQLString },
    instagramId: { type: GraphQLString },
    onlineLearning: { type: GraphQLString },
    commercialRegister: { type: GraphQLString },
    taxNumber: { type: GraphQLString },
    taxIdNumber: { type: GraphQLString },
    owner: { type: GraphQLString },
    managingDirector: { type: GraphQLString },
    operativeManager: { type: GraphQLString },
    regulatingAuthority: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    viewer: {
      type: Viewer,
      resolve: () => { return {}; }
    },
    drivingSchoolEdge: {
      type: DrivingSchoolEdge,
      resolve: async (payload, args, context) => {
        const drivingschool = await getDrivingSchoolById(payload.insertedId, context);
        return {
          cursor: cursorForObjectInConnection(await getDrivingSchools(context, 'name', 1), drivingschool),
          node: drivingschool
        };
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    const result = await updateOrCreateDrivingSchool(args, context);
    if (result && result.insertedId) {
      return { drivingschoolId: result.insertedId };
    }
    throw new Error('Speichern fehlgeschlagen');
  }
});

const updateDrivingSchoolMutation = mutationWithClientMutationId({
  name: 'UpdateDrivingSchoolMutation',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    website: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    fax: { type: GraphQLString },
    facebookId: { type: GraphQLString },
    instagramId: { type: GraphQLString },
    onlineLearning: { type: GraphQLString },
    commercialRegister: { type: GraphQLString },
    taxNumber: { type: GraphQLString },
    taxIdNumber: { type: GraphQLString },
    owner: { type: GraphQLString },
    managingDirector: { type: GraphQLString },
    operativeManager: { type: GraphQLString },
    regulatingAuthority: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    changedDrivingSchool: {
      type: DrivingSchoolType,
      resolve: obj => obj
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updateOrCreateDrivingSchool(args, context);
    return await getDrivingSchoolById(args.id, context);
  }
});

const updateProfileMutation = mutationWithClientMutationId({
  name: 'UpdateProfileMutation',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLString) },
    // username: { type: new GraphQLNonNull(GraphQLString) },
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString }
  },
  outputFields: {
    changedProfile: {
      type: UserType,
      resolve: obj => obj
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updateUser(args, context);
    return context.session.req.user;
  }
});

const updatePasswordMutation = mutationWithClientMutationId({
  name: 'UpdatePasswordMutation',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLString) },
    password1: { type: new GraphQLNonNull(GraphQLString) },
    password2: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    changedPassword: {
      type: UserType,
      resolve: obj => obj
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updatePassword(args, context);
    return context.session.req.user;
  }
});

const CourseDayInputType = new GraphQLInputObjectType({
  name: 'CourseDayInput',
  fields: {
    date: { type: new GraphQLNonNull(GraphQLString) },
    timeFrom: { type: GraphQLString },
    timeTo: { type: GraphQLString },
    lesson: { type: GraphQLString },
    instructor: { type: GraphQLString }
  }
});

const addCourseMutation = mutationWithClientMutationId({
  name: 'AddCourseMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    storeId: { type: new GraphQLNonNull(GraphQLString) },
    tag: { type: GraphQLString },
    title: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString },
    availableSeats: { type: GraphQLInt },
    courseDays: { type: new GraphQLList(CourseDayInputType) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType
    },
    courseEdge: {
      type: CourseEdge,
      resolve: async (payload, args, context) => {
        const course = await getCourseById(payload.courseId, payload.drivingSchoolId, context);
        const courses = await getCourses(payload.drivingSchoolId, 'createdAt', -1, context);
        const index = _.indexOf(courses, course);
        return {
          cursor: offsetToCursor(index),
          node: course
        };
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    const result = await addCourse(args, context);
    return {
      drivingSchoolId: fromGlobalId(args.drivingSchoolId).id,
      courseId: result.insertedId.toString()
    };
  }
});

const updateCourseMutation = mutationWithClientMutationId({
  name: 'UpdateCourseMutation',
  inputFields: {
    courseId: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    storeId: { type: new GraphQLNonNull(GraphQLString) },
    tag: { type: GraphQLString },
    title: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString },
    availableSeats: { type: GraphQLInt },
    courseDays: { type: new GraphQLList(CourseDayInputType) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updateCourse(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const deleteCourseMutation = mutationWithClientMutationId({
  name: 'DeleteCourseMutation',
  inputFields: {
    courseId: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    },
    deletedCourseId: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: async (payload) => {
        return payload.courseId;
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await deleteCourse(args, context);
    return {
      courseId: args.courseId,
      drivingSchoolId: args.drivingSchoolId
    };
  }
});

const addPassedGalleryMutation = mutationWithClientMutationId({
  name: 'AddPassedGalleryMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    date: { type: GraphQLString },
    text: { type: GraphQLString }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType
    },
    passedGalleryEdge: {
      type: PassedGalleryEdge,
      resolve: async (payload, args, context) => {
        const passedGalleryItem = await getPassedGalleryItemById(payload.passedGalleryItemId, payload.drivingSchoolId, context);
        return {
          cursor: offsetToCursor(0),
          node: passedGalleryItem
        };
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    const result = await addPassedGalleryItem(args, context);
    return {
      drivingSchoolId: fromGlobalId(args.drivingSchoolId).id,
      passedGalleryItemId: result.insertedId.toString()
    };
  }
});

const updatePassedGalleryMutation = mutationWithClientMutationId({
  name: 'UpdatePassedGalleryMutation',
  inputFields: {
    passedGalleryItemId: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    date: { type: GraphQLString },
    text: { type: GraphQLString }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updatePassedGalleryItem(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const deletePassedGalleryMutation = mutationWithClientMutationId({
  name: 'DeletePassedGalleryMutation',
  inputFields: {
    passedGalleryItemId: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    },
    deletedPassedGalleryItemId: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: async (payload) => {
        return payload.passedGalleryItemId;
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await deletePassedGalleryItem(args, context);
    return {
      passedGalleryItemId: args.passedGalleryItemId,
      drivingSchoolId: args.drivingSchoolId
    };
  }
});

const OpeningHoursDayInputType = new GraphQLInputObjectType({
  name: 'OpeningHoursDayInput',
  fields: {
    from1: { type: GraphQLString },
    to1: { type: GraphQLString },
    from2: { type: GraphQLString },
    to2: { type: GraphQLString },
  }
});

const OpeningHoursWeekInputType = new GraphQLInputObjectType({
  name: 'OpeningHoursInput',
  fields: {
    monday: { type: OpeningHoursDayInputType },
    tuesday: { type: OpeningHoursDayInputType },
    wednesday: { type: OpeningHoursDayInputType },
    thursday: { type: OpeningHoursDayInputType },
    friday: { type: OpeningHoursDayInputType },
    saturday: { type: OpeningHoursDayInputType }
  }
});

const addStoreMutation = mutationWithClientMutationId({
  name: 'AddStoreMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    openingHours: { type: OpeningHoursWeekInputType },
    theoreticalLessons: { type: OpeningHoursWeekInputType }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType
    },
    storeEdge: {
      type: StoreEdge,
      resolve: async (payload, args, context) => {
        const store = await getStoreById(payload.storeId, payload.drivingSchoolId, context);
        const stores = await getStores(payload.drivingSchoolId, 'createdAt', -1, context);
        const index = _.indexOf(stores, store);
        return {
          cursor: offsetToCursor(index),
          node: store
        };
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    const result = await addStore(args, context);
    return {
      drivingSchoolId: fromGlobalId(args.drivingSchoolId).id,
      storeId: result.insertedId.toString()
    };
  }
});

const updateStoreMutation = mutationWithClientMutationId({
  name: 'UpdateStoreMutation',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    openingHours: { type: OpeningHoursWeekInputType },
    theoreticalLessons: { type: OpeningHoursWeekInputType }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updateStore(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const deleteStoreMutation = mutationWithClientMutationId({
  name: 'DeleteStoreMutation',
  inputFields: {
    storeId: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    },
    deletedStoreId: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: async (payload) => {
        return payload.storeId;
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await deleteStore(args, context);
    return {
      storeId: args.storeId,
      drivingSchoolId: args.drivingSchoolId
    };
  }
});

const TeamInputType = new GraphQLInputObjectType({
  name: 'TeamInput',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: GraphQLString },
    phone: { type: GraphQLString },
    roles: { type: new GraphQLList(GraphQLString) },
    drivingLicences: { type: new GraphQLList(GraphQLString) },
    image: { type: GraphQLString }
  }
});

// const teamConstraints = {
//   name: {
//     presence: {
//       allowEmpty: false,
//       message: 'Bitte geben Sie einen Namen ein.'
//     }
//   }
// };

const addTeamMutation = mutationWithClientMutationId({
  name: 'AddTeamMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    member: { type: new GraphQLNonNull(TeamInputType) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    // const validationResult = validate(args, teamConstraints, { format: 'flat', fullMessages: false });
    // if (validationResult) {
    //
    // }
    await addTeam(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const updateTeamMutation = mutationWithClientMutationId({
  name: 'UpdateTeamMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    index: { type: new GraphQLNonNull(GraphQLInt) },
    member: { type: new GraphQLNonNull(TeamInputType) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updateTeam(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const sortTeamMutation = mutationWithClientMutationId({
  name: 'SortTeamMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    from: { type: new GraphQLNonNull(GraphQLInt) },
    to: { type: new GraphQLNonNull(GraphQLInt) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await sortTeam(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const deleteTeamMutation = mutationWithClientMutationId({
  name: 'DeleteTeamMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    index: { type: new GraphQLNonNull(GraphQLInt) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await deleteTeam(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const addCarMutation = mutationWithClientMutationId({
  name: 'AddCarMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    image: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await addCar(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const updateCarMutation = mutationWithClientMutationId({
  name: 'UpdateCarMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    index: { type: new GraphQLNonNull(GraphQLInt) },
    image: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await updateCar(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const sortCarMutation = mutationWithClientMutationId({
  name: 'SortCarMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    from: { type: new GraphQLNonNull(GraphQLInt) },
    to: { type: new GraphQLNonNull(GraphQLInt) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await sortCar(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

const deleteCarMutation = mutationWithClientMutationId({
  name: 'DeleteCarMutation',
  inputFields: {
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) },
    index: { type: new GraphQLNonNull(GraphQLInt) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await deleteCar(args, context);
    return { drivingSchoolId: args.drivingSchoolId };
  }
});

// const inviteUserMutation = {
//   type: ResultStateType,
//   args: {
//     id: { type: new GraphQLNonNull(GraphQLString) },
//     emailAddress: { type: new GraphQLNonNull(GraphQLString) },
//     role: { type: new GraphQLNonNull(GraphQLString) }
//   },
//   resolve: async (obj, args, context) => {
//     return inviteUser(args, context);
//   }
// };

const contactRequestMutation = {
  type: ResultStateType,
  args: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    mobile: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    subject: { type: new GraphQLNonNull(GraphQLString) },
    message: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve: async (obj, args, context) => {
    return sendContactRequest(args, context);
  }
};

const onlineRegistrationMutation = {
  type: ResultStateType,
  args: {
    lastname: { type: new GraphQLNonNull(GraphQLString) },
    firstname: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    zipcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    birthdate: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    whereToFindUs: { type: GraphQLString },
    licence: { type: new GraphQLNonNull(GraphQLString) },
    owningLicences: { type: GraphQLString },
    startDate: { type: new GraphQLNonNull(GraphQLString) },
    endDate: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: async (obj, args, context) => {
    return sendOnlineRegistration(args, context);
  }
};

const addCampaignParticipantMutation = {
  type: ResultStateType,
  args: {
    solution: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    postcode: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    phone: { type: new GraphQLNonNull(GraphQLString) },
    emailAddress: { type: new GraphQLNonNull(GraphQLString) },
    acceptRequirements: { type: new GraphQLNonNull(GraphQLBoolean) }
  },
  resolve: async (obj, args, context) => {
    return addCampaignParticipant(args, context);
  }
};

const deleteCampaignParticipantMutation = mutationWithClientMutationId({
  name: 'DeleteCampaignParticipantMutation',
  inputFields: {
    campaignParticipantId: { type: new GraphQLNonNull(GraphQLString) },
    drivingSchoolId: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    drivingschool: {
      type: DrivingSchoolType,
      resolve: async (payload, args, context) => {
        return await getDrivingSchoolById(payload.drivingSchoolId, context);
      }
    },
    deletedCampaignParticipantId: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: async (payload) => {
        return payload.campaignParticipantId;
      }
    }
  },
  mutateAndGetPayload: async (args, context) => {
    await deleteCampaignParticipant(args, context);
    return {
      campaignParticipantId: args.campaignParticipantId,
      drivingSchoolId: args.drivingSchoolId
    };
  }
});

const mutationType = new GraphQLObjectType({
  name: 'RootMutation',
  fields: {
    orderWebsite: orderWebsiteMutation,
    orderWebsiteVerification: orderWebsiteVerificationMutation,

    addDrivingSchool: addDrivingSchoolMutation,
    updateDrivingSchool: updateDrivingSchoolMutation,

    addCourse: addCourseMutation,
    updateCourse: updateCourseMutation,
    deleteCourse: deleteCourseMutation,

    addPassedGallery: addPassedGalleryMutation,
    updatePassedGallery: updatePassedGalleryMutation,
    deletePassedGallery: deletePassedGalleryMutation,

    addStore: addStoreMutation,
    updateStore: updateStoreMutation,
    deleteStore: deleteStoreMutation,

    addTeam: addTeamMutation,
    updateTeam: updateTeamMutation,
    sortTeam: sortTeamMutation,
    deleteTeam: deleteTeamMutation,

    addCar: addCarMutation,
    updateCar: updateCarMutation,
    sortCar: sortCarMutation,
    deleteCar: deleteCarMutation,

    // inviteUser: inviteUserMutation,
    sendContactRequest: contactRequestMutation,
    sendOnlineRegistration: onlineRegistrationMutation,
    updateProfile: updateProfileMutation,
    updatePassword: updatePasswordMutation,

    addCampaignParticipant: addCampaignParticipantMutation,
    deleteCampaignParticipant: deleteCampaignParticipantMutation
  }
});

const schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType
});

module.exports = schema;
