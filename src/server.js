// @ flow
'use strict';

const express = require('express');
const session = require('express-session');
// const RedisStore = require('connect-redis')(session);
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const credential = require('credential');
const { graphqlExpress, graphiqlExpress } = require('graphql-server-express');
const multer = require('multer');

const connection = require('./lib/databases/databaseConnection');
// const roles = require('./lib/users/role');
const permissions = require('./lib/users/permission');
const authorizer = require('./lib/utils/authorize');
const fbUtils = require('./lib/utils/fb');
const resetPassword = require('./lib/users/resetPassword');
const graphqlSchema = require('./schema/main');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// session config
const sessionConfig = {
  secret: process.env.ENCRYPTION_PASS,
  resave: false,
  saveUninitialized: false,
  cookie: {}
};

if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  sessionConfig.cookie.secure = true; // serve secure cookies
  // sessionConfig.store = new RedisStore(options);

  app.use((req, res, next) => {
    if (!req.headers.host.startsWith('api.')) {
      const startsWithWww = req.headers.host.startsWith('www.');
      if (!req.secure || !startsWithWww) {
        return res.redirect(`https://${startsWithWww ? '': 'www.'}${req.headers.host}${req.url}`);
      }
    }

    next();
  });
}

app.use(logger('dev'));
app.use(bodyParser.json({ limit: '25mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session(sessionConfig));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(process.env.SITESBASE));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Length, Content-Type, X-Requested-With, X-HTTP-Method-Override');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.options('/q', (req, res) => res.sendStatus(200));

// initialize passport, session and serialize/deserialize
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(user, done) {
  done(null, user);
});

// initialize an anonymous session, if necessary
app.use((req, res, next) => {
  if (!req.session.req.user) {
    Object.assign(req.session.req, { user: { firstname: 'guest', role: 'guest', permissions: [] }});
  }
  next();
});

app.use('/__update-fb-ratingstatistics__', async (req, res) => {
  fbUtils.getRatingStatistics();
  res.sendStatus(200);
});

const storage = multer.memoryStorage();
app.use('/q', multer({ storage }).single('file'));

app.get('/', (req, res) => res.render('index'));
app.get('/anmelden', (req, res) => res.render('anmelden'));
app.get('/bestellung', (req, res) => res.render('bestellung'));
app.get('/bestellung-bestaetigen', (req, res) => res.render('bestellung-bestaetigen'));
app.get('/dashboard', (req, res) => res.render('dashboard'));
app.get('/agb', (req, res) => res.render('agb'));
app.get('/impressum', (req, res) => res.render('impressum'));
app.get('/datenschutz', (req, res) => res.render('datenschutz'));

connection.connectDB()
  .then(db => {
    app.use('/q', graphqlExpress(req => ({
      schema: graphqlSchema,
      context: { db, authorizer, session: req.session, req }
    })));

    if (app.get('env') === 'development') {
      app.use('/graphiql', graphiqlExpress({
        endpointURL: '/q',
      }));
    }

    passport.use(new LocalStrategy(
      function(username, password, done) {
        db.collection('users').findOne({ 'credentials.username': username }, function(err, user) {
          if (err) {
            return done(err);
          }
          if (!user) {
            return done(null, false, { message: 'Incorrect credentials.' });
          }
          const pw = credential();
          pw.verify(user.credentials.hash, password, (err, isValid) => {
            if (err) { throw err; }
            if (!isValid) return done(null, false, { message: 'Incorrect credentials.' });
            permissions.getPermissionsByUser(user)
              .then((permissions: Array<string>) => {
                user.permissions = permissions;
                return done(null, user);
              });
          });
        });
      }
    ));
  })
  .catch(err => {
    console.log(err);
  })
  .then(() => {
    app.post('/login',
      passport.authenticate('local'),
      (req, res) => res.json({ redirect: '/dashboard#/dashboard' })
    );

    app.get('/logout', function(req, res){
      req.logout();
      res.redirect('/');
    });

    app.post('/resetpassword', async (req, res) => {
      if (!req.body.token) {
        const msg = await resetPassword.resetPasswordStep1(req.body.username);
        res.json({ message: msg });
      } else {
        const msg = await resetPassword.resetPasswordStep2(req.body.token, req.body.password1, req.body.password2);
        res.json(msg);
      }
    });

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      const err = new Error('Not Found');
      // $FlowIgnore: suppressing this error
      err.status = 404;
      next(err);
    });

    // error handler
    app.use(function(err, req, res) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.send('error');
    });
  });

module.exports = app;
