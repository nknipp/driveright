const path = require('path');

module.exports = {
  entry: './src/public/js/client.js',
  output: {
    path: path.resolve(__dirname, './dist/public/js/'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },{
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'babel-loader',
          options: {
            passPerPreset: false,
            presets: [
              'env',
              'stage-1',
              'flow',
              'react'
            ],
            plugins: [
              require('./babelRelayPlugin')
            ]
          }
        }]
      },{
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
        ]
      },{
        test: /\.(woff2?|eot|ttf)$/,
        use: [
          'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        ]
      }
    ]
  }
};